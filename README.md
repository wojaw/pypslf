# Running the simulations

TODO

# DB event types:

 - 0: OOSLEN,
 - 1: ZPOTT,
 - 2: LSDT1,
 - 3: LOCTI
 - 4: OOSMHO

# Input data format
Input data sent to Celery workers:

	javascript
	{
	    "case" : "20160915-1200", //symulowana godzina
	    "time" : 900, // symulowany czas [s]
	    "id": "SAFAWEFAWEF", //unikalne ID symulacji
	    "faults" : [
	        {
	            "num": "3", // id lini lub bus  modelu
	            "id": "wwewewe", // unikalne id faultu
	            "from": "32", // bus początkowy linii
	            "to": "20", //bus końcowy linii
	            "pos": 0.13, // miejsce zwarcia, dana częśc lini obliczona przez instancję wyżej
	            "t": 0, // czas zwarcia od począdku symulacji [s]
	            "next": { //jeśli wiecej niż jedno zwarcie w ranach faultu
	                "num": "4",
	                "from": "31", 
	                "to": "22", 
	                "pos": 0.60, 
	                "t": 100,
	            }
	        },
	        {
	            num": "1",
	            "from": "38", 
	            "to": "2", 
	            "pos": 0.23, 
	            "t": 0,
	        },
	        ...
	    ],
	    "gen": { // dane dla generatorów - obowiązkowe dal wszystkich
	        "33":{
	            "stat" : 1,//status mówiący o tym czy 1-włączony,0-wyączony
	            "val": 200 // wartość generacji [MW]
	        },
	        ...
	    }, 
	    "load":{ // dane dla obciążwnia - obowiązkowe dal wszystkich nodów
	        "20": {
	            "stat" : 1, // status noda - aktualnie nie używamy,
	            "val_p": 200, //obciążenie [MW]
	             "val_q": 100 //obciążanie [MVa]
	        }
	    },
	    outages: ["8"] //lista wyłączonych linii
	}

# Output data
## Output of relays' and protection systems' models

TODO

## Output of the post-simulation *EPCL-files.p*
The data are obtained at the end of each simulation and may found at the end of *file.log* corresponding to that simulation run. The *file.log* should be located in the *chans* directory along with *file.chf* and *file_events.csv*).

The example is given below:  

	### POSTprocessing:
	### numBusIsl: 4
	### vecBusIsl:   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   2,   2,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   1,   3,   4,   1,   1,   1,   1,   1, 
	### misBusP: -494.310,   -0.000,   -0.001,    0.004,   -0.003,   -0.001,    0.000,    0.002,    0.002,    0.002,   -0.001,    0.001,    0.000,   -0.000,   -0.000,   -0.001,    0.001,   -0.000,    0.000,    0.000,    0.000,   -0.001,   -0.001,   -0.000,    0.000,   -0.000,   -0.000,    0.000,    0.000,   -0.000,    0.000,    0.000,    0.000,    0.000,    0.000,   -0.000,    0.000,    0.000,    0.000, 
	### misSumP: -494.305
	### genSumP: 4003.201
	### demSumP: 4470.232
	### lossSumP:   27.274
	### costSumP:    4.943

Description of the values printed after '### ':

* numBusIsl - number of islands (integer)
* vecBusIsl - island id for each bus (integers)
* misBusP - power mismatch values for each bus = how much load is not supplied..? (floats [MW])
* misSumP - sum of misBusp values (float [MW])
* genSumP - sum of generation power (float [MW])
* demSumP - sum of demand power (float [MW])
* lossSumP - sum of power losses (float [MW])
* costSumP - supposed to be a cost, but is irrelevant (float [unknown])

Values to consider:
* freqAv - average frequency [Hz]
* ...

