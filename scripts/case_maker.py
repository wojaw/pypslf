import os
from oct2py import octave as oc
from oct2py import Struct
from jinja2 import Environment, FileSystemLoader

from py4pslf.pslfRunner import pslfRunner
from py4pslf.mcCase import UnitGeneration

THIS_DIR = os.path.dirname(os.path.abspath(__file__))
PROJ_DIR = os.path.abspath(os.path.join(THIS_DIR, '..'))

paths = {
    'matpower': r'C:\Octave\matpower6.0',
    'opf2sav': 'opf2sav.p',
    'tmpl_dir': os.path.join(PROJ_DIR, 'case_tmpl', 'base'),
    'cases_dir': r'C:\Users\admin\Desktop\AXA\optim_paper\cases',
    'base_case': 'new39_opf',
}

oc.addpath(paths['matpower'])
oc.addpath(paths['cases_dir'])

j2_env = Environment(loader=FileSystemLoader(paths['tmpl_dir']), trim_blocks=True)

def make_case(scenario, level, GB=0, PG=1, VG=5, PD=2, PMAX=8, PMIN=9, genIC_GB=38, genIC_ID=9,
              max_power=1100):
    mpopt = oc.mpoption('verbose', 0)
    base_mpc = oc.loadcase(os.path.join(paths['cases_dir'], paths['base_case'] + '.m'))
    base_mpc.bus[genIC_GB, PD] = max_power if scenario == 'exp' else 0
    base_mpc.gen[genIC_ID, PMAX] = max_power * level / 100.0
    base_mpc.gen[genIC_ID, PMIN] = max_power * level / 100.0

    mpc = oc.runopf(base_mpc, mpopt)
    mpc_struct = Struct()
    mpc_struct['bus'] = mpc.bus
    mpc_struct['branch'] = mpc.branch
    mpc_struct['gen'] = mpc.gen
    mpc_struct['gencost'] = mpc.gencost
    mpc_struct['version'] = mpc.version
    mpc_struct['baseMVA'] = mpc.baseMVA
    oc.savecase(os.path.join(paths['cases_dir'], '_'.join([paths['base_case'], scenario, '%03d.m' % level])), mpc_struct)

    gen_keys = ['id', 'pgen', 'pmax', 'bus', 'vsched']
    gen_vals = [-1 for _ in gen_keys]
    gen_num = len(mpc.gen)
    gen_id = range(gen_num)
    gen_list = [ dict(zip(gen_keys, gen_vals)) for _ in gen_id ]
    gen_bus = list(mpc.gen[:, GB])
    gen_pgen = list(mpc.gen[:, PG])
    gen_pmax = list(mpc.gen[:, PMAX])
    gen_pmin = list(mpc.gen[:, PMIN])
    gen_vsched = list(mpc.gen[:, VG])
    gen_costs = mpc.gencost[:, 5]

    for gid in gen_id:
        gen_list[gid]['id'] = int(gid)
        gen_list[gid]['bus'] = int(gen_bus[gid])
        gen_list[gid]['pgen'] = gen_pgen[gid]
        gen_list[gid]['pmax'] = gen_pmax[gid]
        gen_list[gid]['pmin'] = gen_pmin[gid]
        gen_list[gid]['vsched'] = gen_vsched[gid]
        gen_list[gid]['g_gencost'] = gen_costs[gid]

    gen_list = sorted(gen_list, key=lambda g: g['id'])
    gen_units = [ UnitGeneration(gen) for gen in gen_list ]

    sav_case = paths['base_case'] + '.sav'
    opf_sav_case = '_'.join([paths['base_case'], scenario, '%03d.sav' % level])

    p_name = 'opf2sav.p'
    p_file = os.path.join(paths['cases_dir'], p_name)
    p_out = open(p_file, 'w')
    p_out.write(j2_env.get_template(paths['opf2sav']).
                render(sav_case=sav_case, opf_sav_case=opf_sav_case, generation=gen_units, scenario=scenario))
    p_out.close()

    pslf = pslfRunner(workspace=paths['cases_dir'], silent=True)
    pslf.runScript(p_file)

    return mpc

res = open(os.path.join(paths['cases_dir'], 'case_maker.txt'), 'w')
for scenario in ['exp', 'imp']:
    for level in range(0, 101, 20):
        mpc = make_case(scenario, level)
        res.write('%s%03d :: %d\n' % (scenario, level, mpc.success))
res.close()