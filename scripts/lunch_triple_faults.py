import copy
import json
from datetime import datetime, timedelta
from functools import partial
from itertools import permutations

from celery.result import ResultSet
from tqdm import tqdm
from hashlib import sha1

from scripts.utils import generate_line_faults
from scripts.launch_celery_tasks import submit_computations, result_callback

datetime_format = '%Y%m%d-%H%M'


def generate_triple_cases(base_hours, horizon_hours, load_factors, lines, line_outages):
    """
    Generates json files with case configuration for chosen parameters
    :param line_outages:
    :param list[int] base_hours: stating hours
    :param list[int] horizon_hours: hour that computations are going to be done
    :param dict load_factors: matrixes of loaf factors
    :param list[pypslf.Line] lines: list of lines in the model
    :return:
    """
    line_faults = generate_line_faults(lines, {int(l): True for l in line_outages})
    # s = 0
    # for i,l in line_faults.items():
    #     s+=len(l)
    # print s
    # print "\n".join([ '{0}:  {1}'.format(i+1,len(l)) for i,l in line_faults.items()])
    for key, val in line_faults.items():
        line_faults[key] = [val[0]]
        line_faults[key][0]['pos'] = 0.5
    # line_faults = {num: [f for f in faults if f['pos'] == 0.5] for num, faults in line_faults.items()}
    for base_hour in base_hours:
        base_datetime = datetime.strptime("20140129-{:02d}00".format(base_hour), datetime_format)
        for load_factor in load_factors.values():
            for h in horizon_hours:
                horizon = base_datetime + timedelta(hours=h)
                load = load_factor[h - 1][base_hour]
                if h != 1:
                    load_increase = (load_factor[h - 1][base_hour] - load_factor[h - 2][base_hour]) / \
                                    load_factor[h - 2][base_hour]
                else:
                    load_increase = load_factor[0][base_hour] - 1
                    # load = 1
                count = 0
                faults_global = []
                for l1, l2, l3 in permutations(line_faults.values(), 3):
                    count += 1
                    for l2_f in l2:
                        for l3_f in l3:
                            faults = copy.deepcopy(l1)
                            for l1_f in faults:
                                l1_f['next'] = copy.deepcopy(l2_f)
                                l1_f['next']['t'] = 0.1
                                l1_f['next']['next'] = copy.deepcopy(l3_f)
                                l1_f['next']['next']['t'] = 0.2
                        faults_global += faults
                    if count % 6 == 0:
                        yield json.dumps(dict(
                                        id=sha1(repr(faults_global)).hexdigest(),
                                        case="20160912-{:02d}00".format(base_hour),
                                        horizon=horizon.strftime(datetime_format),
                                        time=1800,
                                        faults=faults_global,
                                        gen={},
                                        load=load,
                                        load_increase=load_increase,
                                        outages=[],
                                        sim_id=None
                                    ))
                        faults_global = []
                print count
            break
# ============================ MAIN ============================

if __name__ == '__main__':
    result_list = submit_computations([17], [1], [], 'bez_wiatru', case_generator=generate_triple_cases)

    progress_bar = tqdm(total=len(result_list))
    result_set = ResultSet(result_list)

    callback = partial(result_callback, progress_bar)

    result_set.join_native(callback=callback, propagate=True)

    print("DONE")
