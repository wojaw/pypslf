import argparse

import config
from py4pslf.io.fromXLSX import FromXLSX
from py4pslf.postprocessing.loader import load_pickled_results

parser = argparse.ArgumentParser()
parser.add_argument('-i', action="store",
                    help='EXCEL input file',
                    required=True)
# parser.add_argument('-o',
#                     help='Output file')
parser.add_argument('-p',
                    help='Pickled results')

args = parser.parse_args()

if __name__ == '__main__':

    x_reader = FromXLSX(args.i, line_sheet='lines', position_sheet='positions',
                        load_sheet='loads', zone_sheet='zones')

    print "Reading data file..."
    faults = load_pickled_results(args.p)
    "...done"

    for fault in faults:
        print fault.lines[1].dt