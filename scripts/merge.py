from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from models import Base, GridModel, Fault, Event, Simulation, Case, FaultedLine, Bus, Line, Load

engine = create_engine('sqlite:///../axa_merged.db')
Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
session = DBSession()

def merge(session_local,results,simulation_counter=0,case_counter=0,fault_counter=0,faulted_lines_counter=0,event_counter=0):
	max_simulation_id = 0
	for r in results:
		inputdata = Simulation()
		inputdata.__dict__.update(object_as_dict(r))
		inputdata.__dict__.update({'id':r.id+simulation_counter})
		session.add(inputdata)
		max_simulation_id = r.id
		# case
		
	session.commit()
		
	case = session_local.query(Case).filter(Case.simulation_id <= max_simulation_id).all();
	for r in case:
		inputdata = Case()
		inputdata.__dict__.update(object_as_dict(r))
		inputdata.__dict__.update({'id':r.id+case_counter,'simulation_id':r.simulation_id+simulation_counter})
		session.add(inputdata)
		
	session.commit()
	
	max_fault_id = 0;
	fault = session_local.query(Fault).filter(Fault.simulation_id <= max_simulation_id).all();
	for r in fault:
		inputdata = Fault()
		inputdata.__dict__.update(object_as_dict(r))
		inputdata.__dict__.update({'id':r.id+fault_counter,'simulation_id':r.simulation_id+simulation_counter,'case_id':r.id+case_counter})
		session.add(inputdata)
		max_fault_id = r.id;

	session.commit()

	faulted = session_local.query(FaultedLine).filter(FaultedLine.fault_id <= max_fault_id).all();
	for r in faulted:
		inputdata = FaultedLine()
		inputdata.__dict__.update(object_as_dict(r))
		inputdata.__dict__.update({'id':r.id+faulted_lines_counter,'fault_id':r.fault_id+fault_counter})
		session.add(inputdata)

	session.commit()
	
	event = session_local.query(Event).filter(Event.fault_id <=max_fault_id).all();
	for r in event:
		inputdata = Event()
		inputdata.__dict__.update(object_as_dict(r))
		inputdata.__dict__.update({'id':r.id+event_counter,'fault_id':r.fault_id+fault_counter})
		session.add(inputdata)

	session.commit()
	return

def object_as_dict(obj):
	return {c.key: getattr(obj, c.key)
		for c in inspect(obj).mapper.column_attrs}

if __name__ == "__main__":
	engineaxa = create_engine('sqlite:///../axa.db', echo=True)
	engine1213 = create_engine('sqlite:///../axa_12_13.db', echo=True)
	engine1423 = create_engine('sqlite:///../axa_14_23.db', echo=True)
	DBSessionaxa= sessionmaker(bind=engineaxa)
	DBSession1213 = sessionmaker(bind=engine1213)
	DBSession1423 = sessionmaker(bind=engine1423)
	sessionall = DBSessionaxa()
	session1213 = DBSession1213()
	session1423 = DBSession1423()

	#simulation
	session.query(Simulation).delete()
	session.query(Case).delete()
	session.query(Fault).delete()
	session.query(FaultedLine).delete()
	session.query(Event).delete()
	session.commit()

	res1 = sessionall.query(Simulation).filter(Simulation.base_datetime < '2014-01-29 12:00:00.000000')
	res2 = session1213.query(Simulation).all()
	res3 = session1423.query(Simulation).all()

	simulation_counter = 1
	case_counter = 1
	fault_counter = 1
	faulted_lines_counter = 1
	event_counter = 1

	merge(sessionall,res1)

	simulation_counter = session.query(Simulation).order_by(Simulation.id.desc()).first().id;
	case_counter = session.query(Case).order_by(Case.id.desc()).first().id;
	fault_counter = session.query(Fault).order_by(Fault.id.desc()).first().id;
	faulted_lines_counter = session.query(FaultedLine).order_by(FaultedLine.id.desc()).first().id;
	event_counter = session.query(Event).order_by(Event.id.desc()).first().id;
	
	merge(session1213,res2,simulation_counter,case_counter,fault_counter,faulted_lines_counter,event_counter)
	
	simulation_counter = session.query(Simulation).order_by(Simulation.id.desc()).first().id;
	case_counter = session.query(Case).order_by(Case.id.desc()).first().id;
	fault_counter = session.query(Fault).order_by(Fault.id.desc()).first().id;
	faulted_lines_counter = session.query(FaultedLine).order_by(FaultedLine.id.desc()).first().id;
	event_counter = session.query(Event).order_by(Event.id.desc()).first().id;

	merge(session1423,res3,simulation_counter,case_counter,fault_counter,faulted_lines_counter,event_counter)

	#Bus
	session.query(Bus).delete()
	session.commit()
	res = sessionall.query(Bus).all();
	for r in res:
		inputdata = Bus()
		inputdata.__dict__.update(object_as_dict(r))
		session.add(inputdata)

	#GridModel
	session.query(GridModel).delete()
	session.commit()
	res = sessionall.query(GridModel).all();
	for r in res:
		inputdata = GridModel()
		inputdata.__dict__.update(object_as_dict(r))
		session.add(inputdata)

	#Line
	session.query(Line).delete()
	session.commit()
	res = sessionall.query(Line).all();
	for r in res:
		inputdata = Line()
		inputdata.__dict__.update(object_as_dict(r))
		session.add(inputdata)

	#Load
	session.query(Load).delete()
	session.commit()
	res = sessionall.query(Load).all();
	for r in res:
		inputdata = Load()
		inputdata.__dict__.update(object_as_dict(r))
		session.add(inputdata)

	Base.metadata.create_all(bind=engine)
	session.commit()
