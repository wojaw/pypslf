import json
import logging
# from datetime import datetime, timedelta
#
# import pandas as pd
from hashlib import sha1
from random import randint

from celery.result import ResultSet

from py4pslf.io.fromXLSX import FromXLSX
from runner.tasks import run_case
import math
import config
import os
from functools import partial
from tqdm import tqdm

logger = logging.getLogger('LaunchWorkers')
logging.basicConfig(level=logging.DEBUG)

datetime_format = '%Y%m%d-%H%M'
BASE_GEN = {30: 235.0,
            31: 533.57,
            32: 611.0,
            33: 594.0,
            34: 477.0,
            35: 611.0,
            36: 526.0,
            37: 507.0,
            38: 780.0}
MAX_GEN = {30: 1040.0,
           31: 646.0,
           32: 725.0,
           33: 917.0,
           34: 508.0,
           35: 800.0,
           36: 816.0,
           37: 564.0,
           38: 865.0}
RESERVE = {gen: (MAX_GEN[gen] - BASE_GEN[gen]) for gen in MAX_GEN.iterkeys()}


def line_to_dic(_line, _pos, t=0):
    """

    :param t:
    :param _pos:
    :param py4pslf.Line _line:
    :return:
    """
    return {
        "num": _line.num,
        "from": _line.fr,
        "to": _line.to,
        "pos": _pos,
        "t": t
    }


def generate_line_faults(lines, line_outages):
    """

    :param dict line_outages:
    :param list[pypslf.Line] lines:
    :return:
    """
    shortest_line = min([line.length for line in lines]) / 2.
    line_faults = {}
    for line in lines:
        if line.num in line_outages:
            continue
        data = [
            line_to_dic(line, 1. / line.length),
            line_to_dic(line, 1 - 1. / line.length),
        ]
        parts = math.floor(line.length / shortest_line)
        if parts > 4:
            parts = 4.
        for p in range(1, int(parts)):
            p += 0.0
            data.append(line_to_dic(line, p / parts))
        line_faults[line.num] = data
    return line_faults


def result_callback(bar, task_id, val):
    """
    Function that is called every time when worker finishes his job
    :param tqdm.tqdm bar: progress bar instance
    :param str task_id: Task id
    :param runner.tasks.TaskResult val: Task result
    :return:
    """
    # prefix = '\t[{0}/{1}]'.format(counter.val, total_task_num)
    prefix = '\n'

    if val.status == 'DONE':
        logger.info('{2} Done {0} in {1:.4f} seconds'.format(task_id, val.runtime, prefix))
    else:
        logger.info('{3} Errors in task {0} for case {1} : {2}'.format(task_id, val.data['case_id'], val.comment,
                                                                       prefix))
    bar.update(1)


# ========================== THE MOST IMPORTANT PART ============================
def create_shift_schemes():
    result = [BASE_GEN]
    n = 200
    covered = {}
    for num in range(0, n):
        pwr2dist = 800
        step = 10
        new_gen = {key: val for key, val in BASE_GEN.items()}
        while pwr2dist > 0:
            index = randint(0, 8) + 30
            if index == 33 or index == 34:
                continue
            if new_gen[index] + step > MAX_GEN[index]:
                continue
            new_gen[index] += step
            pwr2dist -= step
        new_gen[33] -= 450
        new_gen[34] -= 350
        hash_ = sha1(repr(new_gen)).hexdigest()
        if hash_ in covered:
            print 'REP!'
            covered[hash_] = True
        result.append(new_gen)
    return result


# ===============================================================================


def generate_cases(lines, line_outages):
    outages = {int(l): True for l in line_outages}
    line_faults = generate_line_faults(lines, outages)
    shifts = create_shift_schemes()
    for i, shift in enumerate(shifts):
        for line in lines:
            yield json.dumps(dict(
                id=sha1(repr(line_faults[line.num])).hexdigest(),
                case="20140129-1400",
                horizon="20140129-1500",
                time=1800,
                faults=line_faults[line.num],
                gen=shift,
                load=1,
                load_increase=0.2,
                outages={},
                sim_id=i
            ))
    pass


def submit_computations(line_outages=[], case_variant='bez_wiatru', case_generator=generate_cases):
    input_xls = os.path.join(config.WORK_DIR, '..', 'input.xlsx')
    workspace = config.WORK_DIR
    case_dir = os.path.join(config.WORK_DIR, '..', case_variant)

    model = FromXLSX(input_xls, line_sheet='lines', zone_sheet='zones')

    results = []

    for json_case in case_generator(model.lines, line_outages):
        # print(json.loads(json_case))
        results.append(run_case.apply_async((json_case, workspace, case_dir)))

    logger.debug('Number of cases {0}'.format(len(results)))

    return results


if __name__ == '__main__':
    result_list = submit_computations(case_variant='bez_wiatru')

    progress_bar = tqdm(total=len(result_list))
    result_set = ResultSet(result_list)

    callback = partial(result_callback, progress_bar)

    result_set.join_native(callback=callback, propagate=True)

    print("DONE")
