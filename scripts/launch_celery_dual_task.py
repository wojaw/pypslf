import copy
import json
from functools import partial

from celery.result import ResultSet
from tqdm import tqdm
from hashlib import sha1

from scripts.utils import generate_line_faults
from scripts.launch_celery_tasks import submit_computations, result_callback


def generate_double_cases(base_hours, horizon_hours, load_factors, lines):
    """
    Generates json files with case configuration for chosen parameters
    :param list[int] base_hours: stating hours
    :param list[int] horizon_hours: hour that computations are going to be done
    :param dict load_factors: matrixes of loaf factors
    :param list[pypslf.Line] lines: list of lines in the model
    :return:
    """
    line_faults = generate_line_faults(lines)
    # s = 0
    # for i,l in line_faults.items():
    #     s+=len(l)
    # print s
    # print "\n".join([ '{0}:  {1}'.format(i+1,len(l)) for i,l in line_faults.items()])

    for base_hour in base_hours:
        for load_factor in load_factors.values():
            for h in horizon_hours:
                horizon = base_hour + h
                load = load_factor[h - 1][base_hour]
                next_hour = h if h < 8 else 0
                load_increase = (load_factor[next_hour][base_hour] - load_factor[h - 1][base_hour]) / \
                                load_factor[h - 1][base_hour]
                for line1 in lines:
                    for line2 in lines:
                        if line2.num == line1.num:
                            continue
                        for l2_F in line_faults[line2.num]:
                            # l2_F['t'] = 0.1
                            faults = copy.deepcopy(line_faults[line1.num])
                            # faults = line_faults[line1.num]
                            for i in range(0, len(faults)):
                                faults[i]['next'] = copy.deepcopy(l2_F)
                                faults[i]['next']['t'] = 0.1
                            # print faults
                            yield json.dumps(dict(
                                id=sha1(repr(faults)).hexdigest(),
                                case="20160912-{:02d}00".format(base_hour),
                                horizon="20160912-{:02d}00".format(horizon),
                                time=1800,
                                faults=faults,
                                gen={},
                                load=load,
                                load_increase=load_increase,
                                outages=[]
                            ))
            break


# ============================ MAIN ============================

if __name__ == '__main__':
    result_list = submit_computations([17], [1], 'bez_wiatru', case_generator=generate_double_cases)

    progress_bar = tqdm(total=len(result_list))
    result_set = ResultSet(result_list)

    callback = partial(result_callback, progress_bar)

    result_set.join_native(callback=callback, propagate=True)

    print("DONE")
