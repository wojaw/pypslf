import os
import pandas as pd
from openpyxl import load_workbook

from runner.case_runner import case_runner
from py4pslf.postprocessing.risk_reader import risk_reader


def get_nearest_faults(bus, pairs, time=0.0, time_delta=0.1, pos_ratio=0.01, bus_num_factor=100):
    bus_lines = filter(lambda pair: bus in pair, pairs)
    fault_lines = list()
    for n, pair in enumerate(bus_lines):
        fault_line = {
            'from': pair[0],
            'to': pair[1],
            'pos': pos_ratio * int(bus == pair[0]) + (1 - pos_ratio) * int(bus == pair[1]),
            'num': str(bus * bus_num_factor + n),
            't': time + n * time_delta,
        }
        fault_lines.append(fault_line)

    def reduce_faults(f1, f2):
        f = f1.copy()
        if f.has_key('next'):
            # print 'has:', (f['from'], f['to']), 'next:', (f['next']['from'], f['next']['to'])
            f['next'] = reduce_faults(f['next'], f2)
        else:
            # print 'not:', (f['from'], f['to']), 'add:', (f2['from'], f2['to'])
            f['next'] = f2
        return f

    # print bus
    fault = reduce(reduce_faults, fault_lines) if fault_lines else None
    return fault


def run_N1(work_dir, case_name, test_name,
           buses=None, id_step=1,
           faults_per_core=2,
           load_factor=1, load_increase=0,
           timestep=0.005, endtime=5.0, chan2csv=False,
           gen_model='genrou', oosmho_notrip=0, zpott_tcb=0.15, agc_on=True, ametr_refa=0,
           do_run=True, do_read=True,
           gen_neighbourhood=0, gen_ids=range(31,40), gen_bus=[20]):
    # set the case id, paths, etc.
    result_dir = os.path.join(work_dir, 'results')
    case_id = '_'.join([case_name, test_name])
    case_dir = os.path.join(result_dir, case_id)
    event_dir = os.path.join(case_dir, 'results')
    info_xl = os.path.join(work_dir, 'input_new.xlsx')

    # set the buses
    buses = buses if buses else range(1, 31)
    bus_pairs = [ (bus, bus) for bus in buses ]

    # set the case simulation info
    load_df = pd.read_excel(info_xl, sheetname='loads')
    bus_loads = [ load_factor * load for load in load_df.load.values ]

    # prepare the input
    def get_bus_fault(bus, position=0, time=0):
        fault = {
            "from": bus,
            "to": bus,
            "num": str(bus),
            "pos": position,
            "t": time,
        }
        return fault

    faults = [ get_bus_fault(bus) for bus in buses ]

    # add multiple faults of lines in gens neighbourhood
    if gen_neighbourhood > 0:
        line_df = pd.read_excel(info_xl, sheetname='lines')
        line_pairs = [tuple(map(int, line)) for line in line_df[['from', 'to']].values]
        tran_df = pd.read_excel(info_xl, sheetname='trans')
        tran_pairs = [ tuple(map(int, tran)) for tran in tran_df[['from', 'to']].values ]
        gen_buses = filter(bool, map(lambda pair: pair[0] if pair[1] in gen_ids else pair[1] if pair[0] in gen_ids else None, tran_pairs))
        gen_faults = filter(bool, map(lambda bus:
                                      get_nearest_faults(bus, line_pairs, pos_ratio=gen_neighbourhood),
                                      gen_buses))
        faults += gen_faults

    INPUT = {
        'fault_per_core': faults_per_core,
        'files': {
            'pre': 'dyPre.p',
            'post': 'dyCost.p',
            'inrun': 'dyInRun.p',
        },
        'cases': {
            case_name: {
                'id': case_id,
                'time': endtime,
                'load': load_factor,
                'load_increase': load_increase,
                'pre_sav_name': None,
                'outages': [],
                'gen': [],
                'faults': faults,
            }
        }
    }

    # run the simulations
    if do_run:
        runner = case_runner(work_dir, result_dir=result_dir,
                             how_to_run='from_dict', dict_to_run=INPUT,
                             timestep=timestep,
                             chan2csv=chan2csv)
        runner.get_dyd_files(INPUT=INPUT, gen_model=gen_model, oosmho_notrip=oosmho_notrip,
                             agc_on=agc_on, zpott_tcb=zpott_tcb, ametr_refa=ametr_refa)
        runner.run()

    # read the results
    if do_read:
        risk_results = risk_reader(work_dir=event_dir, line_pairs=bus_pairs, line_positions=[0], id_step=id_step,
                                   bus_loads=bus_loads, load_increase=load_increase, load_factor=load_factor)

        xlName = '_'.join([case_name, test_name, 'faults.xlsx'])
        xlPath = os.path.join(case_dir, xlName)
        xlWriter = pd.ExcelWriter(xlPath, engine='openpyxl')

        for df_name in ['line', 'gen', 'event']:
            risk_df = risk_results[df_name]
            if risk_df is not None:
                risk_df.to_excel(xlWriter, sheet_name=df_name, index=False)
                xlWriter.save()

        xlWriter.close()
    else:
        risk_results = None

    return risk_results


# RUN
work_dir = r"C:\Users\admin\Desktop\AXA\PSCC18_paper"
# info_xl = os.path.join(work_dir, 'input_new.xlsx')
buses = range(1,31)
case_name = 'new39_scopf'
gen_model = 'genrou'
ametr_refa = 0
oosmho_notrip = 1
agc_on = False
test_name = '%s_trip%d_refa%d_agc%d_genn1_bus' % (gen_model, 1-oosmho_notrip, ametr_refa, int(agc_on))
endtime = 10
timestep = 0.00333
chan2csv = True

risk = run_N1(work_dir, case_name, test_name,
              buses=buses,
              faults_per_core=2,
              chan2csv=chan2csv,
              endtime=endtime,
              timestep=timestep,
              gen_model=gen_model,
              ametr_refa=ametr_refa,
              oosmho_notrip=oosmho_notrip,
              agc_on=agc_on,
              gen_neighbourhood=0.01,
              do_run=True,
              do_read=True)