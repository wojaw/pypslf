import argparse
import os
import sys
import re
import pandas as pd
import pickle
import shutil
import numpy as np

import config
from py4pslf.io.fromXLSX import FromXLSX

parser = argparse.ArgumentParser()
parser.add_argument('-i', action="store",
                    help='EXCEL input file',
                    required=True)
parser.add_argument('-l1', action="store",
                    help='L1 xlsx',
                    required=True)
parser.add_argument('-l2', action="store",
                    help='L2 xlsx',
                    required=True)
parser.add_argument('-o', action="store",
                    help='Output',
                    required=True)
args = parser.parse_args()

x_reader = FromXLSX(args.i, line_sheet='lines', position_sheet='positions',
                    load_sheet='loads', zone_sheet='zones')

L1 = pd.read_excel(args.l1, 'Sheet1', index_col=None)
L2  = pd.read_excel(args.l2, 'Sheet1', index_col=None)

l1_losses = np.zeros(34)
l2_losses = np.zeros((34,34))

for index,row in L1.iterrows():
    _id = int(row['id'])-1
    l1_losses[_id] += 0 if row['dem']<row['gen'] else 100*(row['dem']-row['gen'])/row['dem']

for index,row in L2.iterrows():
    _id = int(row['id'])-1
    main = int(row['first_id'])-1
    l2_losses[main][_id] += 0 if row['dem']<row['gen'] else 100*(row['dem']-row['gen'])/row['dem']

print pd.DataFrame(l2_losses)

result = np.zeros((34,34))

for i in range(34):
    for j in range(34):
        result[i][j] = abs(l2_losses[i][j]-l1_losses[i]-l1_losses[j])/(25)

frame = pd.DataFrame(result)

print frame

np.savetxt(args.o, result, delimiter=",")
