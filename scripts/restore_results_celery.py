import json
from functools import partial

from py4pslf.costCalculator import Cost
from sqlalchemy.sql import ClauseElement

from runner.celery import app
from runner.tasks import read_results
import config
import logging
import re
import os
from models import Base, GridModel, Fault, Event, Simulation, Case, FaultedLine, VecBusIsl, MisBusP, \
    get_or_create, Load
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from celery.result import ResultSet
from tqdm import tqdm
import pandas as pd
from subprocess import Popen
from datetime import datetime

logger = logging.getLogger('RestoreResults')

logging.basicConfig(level=logging.DEBUG)
# logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

whole_name = ur'new39opf_gen30_final_line_double'

patterns = {
    # 'result_dir': re.compile(ur'^(20[12][0-9][0-1][0-9][0-3][0-9]_[012][0-9]00-){2}[LS](?P<id>\d+x?\d*)$')
    # 'result_dir': re.compile(ur'^new39opf_line_double_L\d{2}$')
    'result_dir': re.compile(ur'^' + whole_name + ur'_L\d{2}$')
}

# config.WORK_DIR = 'D:\\AXA_workspace\\12-20'

engine = create_engine('sqlite:///' + config.DB_NAME)
# engine = create_engine('mysql+pymysql://py4pslf:axa@localhost:3307/axa_pslf', pool_recycle=3600)
Base.metadata.bind = engine
Base.metadata.create_all(engine)
DBSession = sessionmaker(bind=engine)
session = DBSession()


def store_results(buses, lines, grid_model, bar, task_id, val):
    """
    :param grid_model:
    :param lines:
    :param buses:
    :param  tqdm.tqdm bar:
    :param int task_id:
    :param runner.tasks.TaskResult val: Task result
    :return:
    """
    # return
    if val.status == 'ERROR':
        logger.info('Errors in task {0} for case {1} : {2}'.format(task_id, val.data['case_id'], val.comment))
        bar.update(1)
        return
    if val.comment:
        logger.info(val.comment)
    case = val.data
    """:type py4pslf.mcCase.MCCase"""
    dt = datetime.now()
    db_simulation, created_s = get_or_create(session, Simulation, dict(
        model=grid_model
    ), sim_datetime=dt, base_datetime=dt, load=case.load_factor,
                                             load_increase=case.load_increase, sim_period=case.sim_period,
                                             name=case.simulation_id)

    # db_case, created_c = get_or_create(session, Case, dict(
    #     status=2,
    # ), simulation=db_simulation, hash=case.id)

    if created_s:
        for unit_gen in case.generation:
            db_simulation.loads.append(Load(
                bus=buses[str(unit_gen.id+30)],
                value=unit_gen.generation
            ))
        session.commit()

    db_case = Case(status=2,
                   simulation=db_simulation,
                   hash=case.id)
    for fault in case.faults:
        result = fault.result
        db_fault = Fault(status=2,
                         case=db_case,
                         simulation=db_simulation,
                         num_bus_isl=result.num_bus_isl,
                         mis_sum_p=result.mis_sum_p,
                         gen_sum_p=result.gen_sum_p,
                         dem_sum_p=result.dem_sum_p,
                         loss_sum_p=result.loss_sum_p,
                         )
        for event in fault.events:
            db_event = Event(time=event.dt,
                             type=event.type.upper(),
                             fr=buses[event.fr],
                             to=buses[event.to if event.to != '' else event.fr])

            db_event.additional_value1 = str(event.stage) if event.stage else None
            db_fault.events.append(db_event)
        for line in fault.lines:
            db_fault.faulted_lines.append(FaultedLine(line=lines[str(line.num)],
                                                      position=line.pos,
                                                      time=line.dt))
        for b, vbi in enumerate(result.vec_bus_isl):
            db_fault.vec_bus_isl.append(VecBusIsl(bus=buses[str(b + 1)],
                                                  val=vbi))
        for b, mbp in enumerate(result.mis_bus_p):
            db_fault.mis_bus_p.append(MisBusP(bus=buses[str(b + 1)],
                                              val=mbp))

        cost_input = {'time': [], 'e_type': [], 'info': []}
        for event in fault.events:
            cost_input['time'].append(event.dt)
            cost_input['e_type'].append(event.type.lower())
            if event.type.upper() == 'LSDT1':
                cost_input['info'].append({
                    'bus': event.fr,
                    'stage': event.stage
                })
            elif event.type.upper().startswith('OOS'):
                cost_input['info'].append({
                    'bus': event.to,
                    'gen': event.fr
                })
            else:
                cost_input['info'].append({
                    'to': event.to,
                    'from': event.fr
                })
        if len(cost_input['e_type']) == 0:
            logger.debug('Empty events...skipping')
            bar.update(1)
            continue
        cost_estimator = Cost(pd.DataFrame(cost_input), numBusIsl=result.num_bus_isl,
                              loadIncrease=db_simulation.load_increase, loadFactor=db_simulation.load)
        db_fault.cost_sum_p = cost_estimator.shedCost
        session.add(db_fault)
    session.commit()
    bar.update(1)


# app.conf.update(
#     CELERY_ALWAYS_EAGER=True
# )
if __name__ == '__main__':

    work_dir = config.WORK_DIR

    # work_dir = os.path.join(work_dir,'new39opf_line_double_all')
    work_dir = os.path.join(work_dir, whole_name)
    # work_dir = os.path.join(work_dir,'new39opf_line_triple_all')

    results = []

    grid_model = session.query(GridModel).filter(GridModel.name == 'AXAieee39_mod').one()
    buses = {bus.name: bus for bus in grid_model.buses}
    lines = {line.name: line for line in grid_model.lines}

    for data_dir in os.listdir(work_dir):
        data_path = os.path.join(work_dir, data_dir)
        sim_dir = patterns['result_dir'].match(data_dir)
        if os.path.isdir(data_path) and sim_dir:
            for json_case in os.listdir(os.path.join(data_path, 'cases')):
                with open(os.path.join(data_path, 'cases', json_case), 'rb') as json_case_file:
                    case_data = json.load(json_case_file)
                    if 'sim_id' not in case_data:
                        case_data['sim_id'] = sim_dir.group('id')
                    results.append(read_results.delay(json.dumps(case_data), data_path))
                    # read_results(json_case_file.read(), work_dir)
                    # break
    #
    logger.debug(len(results))
    result_set = ResultSet(results)
    progress_bar = tqdm(total=len(result_set))

    callback = partial(store_results, buses, lines, grid_model, progress_bar)

    result_set.join_native(callback=callback, propagate=True)
