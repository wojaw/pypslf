import json
import logging
from datetime import datetime, timedelta

import pandas as pd
from hashlib import sha1

from celery.result import ResultSet
from runner.tasks import run_case
from py4pslf.io.fromXLSX import FromXLSX
import math
import config
import os
from functools import partial
from tqdm import tqdm
from scripts.utils import generate_line_faults

logger = logging.getLogger('LaunchWorkers')
logging.basicConfig(level=logging.DEBUG)

datetime_format = '%Y%m%d-%H%M'


def read_load_factors_from_xls(load_factors_xls):
    """
    Reads prepared load factors from xls file
    :param string load_factors_xls: name of the xls spreadsheet that contains load factors
    :return:
    """
    av = pd.read_excel(load_factors_xls, 'factor_av', index_col=None, header=None).as_matrix()
    hi = pd.read_excel(load_factors_xls, 'factor_hi', index_col=None, header=None).as_matrix()
    lo = pd.read_excel(load_factors_xls, 'factor_lo', index_col=None, header=None).as_matrix()
    return {'av': av, 'hi': hi, 'lo': lo}


def generate_cases(base_hours, horizon_hours, load_factors, lines, line_outages):
    """
    Generates json files with case configuration for chosen parameters
    :param list[int] base_hours: stating hours
    :param list[int] horizon_hours: hour that computations are going to be done
    :param dict load_factors: matrixes of loaf factors
    :param list[pypslf.Line] lines: list of lines in the model
    :return:
    """
    outages = {int(l): True for l in line_outages}
    line_faults = generate_line_faults(lines, outages)
    for base_hour in base_hours:
        base_datetime = datetime.strptime("20140129-{:02d}00".format(base_hour), datetime_format)
        for h in horizon_hours:
            horizon = base_datetime + timedelta(hours=h)
            for load_factor in load_factors.values():
                load = load_factor[h - 1][base_hour]
                if h != 1:
                    load_increase = (load_factor[h-1][base_hour] - load_factor[h - 2][base_hour]) / \
                                    load_factor[h - 2][base_hour]
                else:
                    load_increase = load_factor[0][base_hour] - 1
                    # load = 1
                for line in lines:
                    if line.num in outages:
                        continue
                    yield json.dumps(dict(
                        id=sha1(repr(line_faults[line.num])).hexdigest(),
                        case="20140129-{:02d}00".format(base_hour),
                        horizon=horizon.strftime(datetime_format),
                        time=1800,
                        faults=line_faults[line.num],
                        gen={},
                        load=load,
                        load_increase=load_increase,
                        outages=line_outages
                    ))
                if h == 0:
                    break


def submit_computations(base_hours, horizon_hours=range(1, 9), line_outages=[], case_variant='bez_wiatru',
                        case_generator=generate_cases):
    """
    Method that sends computation request to celery workers
    :param line_outages:
    :param case_generator:
    :param list[int] base_hours: List of base hours to compute
    :param list[int] horizon_hours: List of hours that should be computed for base hours
    :param string case_variant: name of case variant
    :return list[celery.Task.AsyncResults]: List of celery results objects
    """
    input_xls = os.path.join(config.WORK_DIR, '..', 'input.xlsx')
    load_factors_xls = os.path.join(config.WORK_DIR, '..', 'resultData_factors.xlsx')
    workspace = config.WORK_DIR
    case_dir = os.path.join(config.WORK_DIR, '..', case_variant)

    model = FromXLSX(input_xls, line_sheet='lines', zone_sheet='zones')
    load_factors = read_load_factors_from_xls(load_factors_xls)

    results = []

    for json_case in case_generator(base_hours, horizon_hours, load_factors, model.lines, line_outages):
        # results.append(json.loads(json_case))
        results.append(run_case.apply_async((json_case, workspace, case_dir)))

    logger.debug('Number of cases {0}'.format(len(results)))

    return results


def result_callback(bar, task_id, val):
    """
    Function that is called every time when worker finishes his job
    :param tqdm.tqdm bar: progress bar instance
    :param str task_id: Task id
    :param runner.tasks.TaskResult val: Task result
    :return:
    """
    # prefix = '\t[{0}/{1}]'.format(counter.val, total_task_num)
    prefix = '\n'

    if val.status == 'DONE':
        logger.info('{2} Done {0} in {1:.4f} seconds'.format(task_id, val.runtime, prefix))
    else:
        logger.info('{3} Errors in task {0} for case {1} : {2}'.format(task_id, val.data['case_id'], val.comment,
                                                                       prefix))
    bar.update(1)


#  ============================ MAIN ============================

if __name__ == '__main__':
    result_list = submit_computations(range(0, 24), range(2, 9), [], 'bez_wiatru')

    progress_bar = tqdm(total=len(result_list))
    result_set = ResultSet(result_list)

    callback = partial(result_callback, progress_bar)

    result_set.join_native(callback=callback, propagate=True)

    print("DONE")
