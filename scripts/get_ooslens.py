import logging

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import config
import operator
from models import Base, Fault, Event, Bus

engine = create_engine('sqlite:///' + config.DB_NAME)
Base.metadata.bind = engine
Base.metadata.create_all(engine)
DBSession = sessionmaker(bind=engine)
session = DBSession()

logger = logging.getLogger('RestoreResults')

logging.basicConfig(level=logging.DEBUG)
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

if __name__ == "__main__":
    pass

    # This will load full objects from DB but is not very efficient
    # ooslens = session.query(Event, Bus, Fault
    #                         ).filter(Event.fault_id == Fault.id
    #                                  ).filter(Event.fr_id == Bus.id
    #                                           ).filter(Event.type == 0
    #                                                    ).all()

    # This will load only tuple with selected columns and it's more efficient way
    ooslens = session.query(Event.type, Event.time, Bus.name
                            ).join(Event.fr
                                   ).filter(Event.type == 0  # OOSLEN
                                            ).all()

    print("Total : {0:d}".format(len(ooslens)))
    ranking = {}
    for e_type, e_time, b_name in ooslens:
        if b_name not in ranking:
            ranking[b_name] = 0
        ranking[b_name] += 1

    sorted_buses = sorted(ranking.items(), key=operator.itemgetter(1), reverse=True)

    print('\n'.join(['{0} : {1}'.format(b, n) for b, n in sorted_buses]))
