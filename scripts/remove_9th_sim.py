import logging

import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.orm import aliased
from sqlalchemy.orm import sessionmaker
from tqdm import tqdm

from datetime import datetime, timedelta

from py4pslf.costCalculator import Cost

import config
from models import Base, GridModel, Line, WeatherForecast, Simulation, Case, Fault, FaultedLine, VecBusIsl, MisBusP

logger = logging.getLogger('RestoreResults')

logging.basicConfig(level=logging.DEBUG)
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

engine = create_engine('sqlite:///' + config.DB_NAME)
# engine = create_engine('sqlite:////D:\\axa.db')
Base.metadata.bind = engine
Base.metadata.create_all(engine)
DBSession = sessionmaker(bind=engine)
session = DBSession()

# w_xls = '/home/kgomulski/Pulpit/AXA/axa/demand_pred/Probabilities_table.xlsx'

if __name__ == "__main__":
    for h in range(0, 24):
        base_dt = datetime.strptime('20140129-{:02}00'.format(h), '%Y%m%d-%H%M')
        horizon9_dt = base_dt + timedelta(hours=8)
        simulation = session.query(Simulation).filter(Simulation.base_datetime == base_dt,
                                            horizon9_dt == Simulation.sim_datetime).all()
        for sim in simulation:
            # sim.faults.delete()delete
            # print sim
            session.query(Fault).filter(Fault.simulation_id == sim.id).delete()
            session.query(Case).filter(Case.simulation_id == sim.id).delete()
            session.delete(sim)
            # session.delete(case)
            # session.delete(fault)

            # print sim.base_datetime,sim.sim_datetime
            # for fault in sim.faults:
            #     session.delete(fault)
            # for case in sim.cases:
            #     session.delete(case)
            # session.delete(sim)
    session.commit()


        # base_dt = datetime.strptime('20140129-{:02}00'.format(h), '%Y%m%d-%H%M')
