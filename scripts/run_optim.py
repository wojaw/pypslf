from runner.optim_runner import optim_runner

base_dir = r"C:\Users\admin\Desktop\AXA\optim_paper"
which_lines = None #range(16)

for step in [0.1]:
    for scenario in ['exp']: #, 'imp']:
        for level in [95]:
            base_name = 'new39_opf'
            # base_name = 'new39_scopf_%s_%03d' % (scenario, level)

            try:
                opt_run = optim_runner(base_dir=base_dir, base_name=base_name,
                                       which_lines=which_lines,
                                       const_gens=[9],
                                       case_id='50pos30fix%03dstep1oos0olBcost1orr1zip0wlw1agc' % (10*step),
                                       iteration_power_step=step,
                                       fault_positions=[50],
                                       faults_per_core=2,
                                       run_risk_ass=True,
                                       iteration=0,
                                       optimization='opf',
                                       oosmho_rr=1, wlwscc_on=False)
                opt_run.run(max_iter=999)
            except ValueError:
                print "Oops, try again!!!"


# base_name = 'new39opf_g30'
# opt_run = optim_runner(base_dir=base_dir, base_name=base_name,
#                        which_lines=which_lines,
#                        const_gens=[],
#                        iteration_power_step=2,
#                        fault_positions=[50],
#                        faults_per_core=2)
# opt_run.run(max_iter=50)
