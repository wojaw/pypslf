import os, sys
import pandas as pd
# from openpyxl import load_workbook

from runner.case_runner import case_runner
# from py4pslf.postprocessing.risk_reader import risk_reader

case_name = 'new39opf_gen30_final'
test_name = 'line_double'

work_dir = "C:\\Users\\user\\Desktop\\AXA\\WJ\\Tests\\all_faults\\risk_paper"
result_dir = os.path.join(work_dir, 'results')
event_dir = os.path.join(result_dir, case_name, 'results')
info_xl = os.path.join(work_dir, 'input_new.xlsx')

# set the lines
line_positions = [ 0.01, 0.25, 0.50, 0.75, 0.99 ]
# line_pairs = [ [1, 2], [1, 30], [2, 3], [18, 25], [2, 25], [3, 4], [3, 18], [4, 5], [4, 14], [5, 6], [5, 8], [6, 7], [6, 11], [7, 8], [8, 9], [9, 30], [10, 11], [10, 13], [13, 14], [14, 15], [15, 16], [16, 17], [16, 20], [13, 20], [16, 21], [16, 24], [17, 18], [23, 20], [17, 27], [21, 22], [22, 23], [23, 24], [4, 30], [1, 3], [24, 28], [24, 29], [25, 26], [26, 27], [26, 28], [15, 20], [26, 29], [6, 14], [16, 28], [28, 29] ]
line_df = pd.read_excel(info_xl, sheetname='lines')
line_pairs = tuple( tuple(map(int, line)) for line in line_df[['from', 'to']].values )
line_num = len(line_pairs)
# line_doubles = tuple( (l, ll) for l in range(line_num) for ll in range(line_num) if l != ll )

# prepare the case info
load_factor = 1.0
load_increase = 0.0
# bus_loads = [ 0.000, 0.000, 322.000, 500.000, 0.000, 0.000, 233.800, 522.000, 0.000, 0.000, 0.000, 7.500, 0.000, 0.000, 320.000, 329.400, 0.000, 158.000, 0.000, 680.000, 274.000, 0.000, 247.500, 308.600, 224.000, 139.000, 281.000, 206.000, 283.500, 0.000, 9.200, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 1104.000, ]
# bus_df = pd.read_excel(info_xl, sheetname='loads', header=None, names=['load'])
# bus_loads = tuple( load_factor * float(load[0]) for load in bus_df[['load']].values )

# prepare the input
# def get_line_faults_2(line_ids, positions=(0.0, 0.0), times=(0.0, 0.1)):
#     assert len(line_ids) == 2 and line_ids[0] != line_ids[1]
#     faults = list()
#
#     for f in range(2):
#         fault = {
#             "from": line_pairs[line_ids[f]][0],
#             "to": line_pairs[line_ids[f]][1],
#             "num": str(line_ids[f]+1),
#             "pos": positions[f],
#             "t": times[f],
#         }
#
#         faults.append(fault)
#
#     double_fault = faults[0]
#     double_fault['next'] = faults[1]
#
#     return double_fault
#
# double_faults = [ get_line_faults_2(line_ids=(l, ll), positions=(p, pp)) for l in range(line_num) for ll in range(line_num) if l != ll \
#                   for p in line_positions for pp in line_positions ]

def get_line_fault(line_id, position=0.0, time=0.0, time_delta=0.1):
    line_ids = range(line_num)
    line_ids.remove(line_id)
    faults = list()

    for l in line_ids:
        for p in line_positions:
            fault = {
                "from": line_pairs[line_id][0],
                "to": line_pairs[line_id][1],
                "num": str(line_id + 1),
                "pos": position,
                "t": time,
                "next": {
                    "from": line_pairs[l][0],
                    "to": line_pairs[l][1],
                    "num": str(l + 1),
                    "pos": p,
                    "t": time + time_delta,
                }
            }

            faults.append(fault)

    return faults

Line_id = int(sys.argv[1])
line_id = int(sys.argv[2])
# chosen_line_ids = list()
# chosen_line_ids.append(line_id)
# chosen_line_ids = [ 0, 2 ]
chosen_line_ids = range(Line_id, line_id)
for line_id in chosen_line_ids:
    double_faults = list()
    for p in line_positions:
        double_faults.extend(get_line_fault(line_id=line_id, position=p, time_delta=0.05))

    INPUT = {
        'fault_per_core': 43,
        'files': {
            'pre': 'dyPre.p',
            'post': 'dyCost.p',
            'inrun': 'dyInRun.p',
        },
        'cases': {
            case_name: {
                'id': '_'.join([case_name, test_name, 'L%02d' % line_id]),
                'time': 1800,
                'load': load_factor,
                'load_increase': load_increase,
                'pre_sav_name': None,
                'outages': [],
                'gen': [],
                'faults': double_faults,
            }
        }
    }

    # run the simulations
    runner = case_runner(work_dir, result_dir=result_dir, how_to_run='from_dict', dict_to_run=INPUT)
    runner.run()

