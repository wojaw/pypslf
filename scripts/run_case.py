import argparse
from runner.case_runner import case_runner

# Collect the command line args ...
parser = argparse.ArgumentParser()
parser.add_argument('-w', '--workspace', action="store",
                    help='Directory with case data',
                    required=True)
parser.add_argument('-r', '--result_dir', action="store",
                    help='Directory where results should be placed')
parser.add_argument('-c', '--config', action="store",
                    help='Python config file with proper INPUT variable',
                    default='case.py')

# Parse the args ...
args = parser.parse_args()

# Use the args to run the simulations
runner = case_runner(args.workspace, result_dir=args.result_dir, config=args.config)
runner.run()
