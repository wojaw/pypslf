import os
import pandas as pd

from runner.case_runner import case_runner
from py4pslf.postprocessing.risk_reader import risk_reader


def run_N1(work_dir, case_name, test_name,
           line_positions=(0.01, 0.25, 0.50, 0.75, 0.99), line_pairs=None, id_step=0,
           faults_per_core=None,
           load_factor=1, load_increase=0,
           timestep=0.005, endtime=5, chan2csv=False,
           gen_model='genrou', oosmho_notrip=0, zpott_tcb=0.15, zpott_t1=0.05, agc_on=True, ametr_refa=0,
           do_run=True, do_read=True):
    # set the case id, paths, etc.
    result_dir = os.path.join(work_dir, 'results')
    case_id = '_'.join([case_name, test_name])
    case_dir = os.path.join(result_dir, case_id)
    event_dir = os.path.join(case_dir, 'results')
    info_xl = os.path.join(work_dir, 'input_new.xlsx')

    # set the faults info
    if not line_pairs:
        line_df = pd.read_excel(info_xl, sheetname='lines')
        line_pairs = [ tuple(map(int, line)) for line in line_df[['from', 'to']].values ]
        # line_pairs = [line_pairs[0]] + line_pairs

    # set the case simulation info
    load_df = pd.read_excel(info_xl, sheetname='loads')
    bus_loads = [ load_factor * load for load in load_df.load.values ]

    # prepare the input
    def get_line_fault(line_id, position=0, time=0.0):
        fault = {
            "from": line_pairs[line_id][0],
            "to": line_pairs[line_id][1],
            "num": str(line_id),
            "pos": position,
            "t": time,
        }
        return fault

    faults = [ get_line_fault(line_id=l, position=p) for l in range(len(line_pairs)) for p in line_positions ]
    faults_per_core = faults_per_core if faults_per_core else 2 * len(line_positions)

    INPUT = {
        'fault_per_core': faults_per_core,
        'files': {
            'pre': 'dyPre.p',
            'post': 'dyCost.p',
            'inrun': 'dyInRun.p',
        },
        'cases': {
            case_name: {
                'id': '_'.join([case_name, test_name]),
                'time': endtime,
                'load': load_factor,
                'load_increase': load_increase,
                'pre_sav_name': None,
                'outages': [],
                'gen': [],
                'faults': faults,
            }
        }
    }

    # run the simulations
    if do_run:
        runner = case_runner(work_dir, result_dir=result_dir,
                             how_to_run='from_dict', dict_to_run=INPUT,
                             timestep=timestep,
                             chan2csv=chan2csv)
        runner.get_dyd_files(INPUT=INPUT, gen_model=gen_model, oosmho_notrip=oosmho_notrip,
                             agc_on=agc_on, zpott_tcb=zpott_tcb, zpott_t1=zpott_t1, ametr_refa=ametr_refa)
        runner.run()

    # read the results
    if do_read:
        risk_results = risk_reader(work_dir=event_dir, line_pairs=line_pairs, id_step=id_step,
                                   line_positions=[ lp*100 for lp in line_positions ],
                                   include_oos_trips=True,
                                   bus_loads=bus_loads, load_increase=load_increase, load_factor=load_factor)

        xlName = '_'.join([case_name, test_name, 'faults.xlsx'])
        xlPath = os.path.join(case_dir, xlName)
        xlWriter = pd.ExcelWriter(xlPath, engine='openpyxl')

        for df_name in ['line', 'gen', 'event']:
            risk_df = risk_results[df_name]
            if risk_df is not None:
                risk_df.to_excel(xlWriter, sheet_name=df_name, index=False)
                xlWriter.save()

        xlWriter.close()
    else:
        risk_results = None

    return risk_results


# RUN
# work_dir = r"C:\Users\admin\Desktop\AXA\PSCC18_paper"
work_dir = r"C:\Users\admin\Desktop\AXA\optim_paper"
# info_xl = os.path.join(work_dir, 'input_new.xlsx')
line_pairs = None
line_positions = [0.5]
case_name = 'new39_opf'
gen_model = 'genrou'
ametr_refa = 0
oosmho_notrip = 0
agc_on = True
test_name = '%s-sal39_trip%d_refa%d_agc%d' % (gen_model, 1-oosmho_notrip, ametr_refa, int(agc_on))
endtime = 1800
timestep = 0.005
chan2csv = False

risk = run_N1(work_dir, case_name, test_name,
              line_pairs=line_pairs,
              line_positions=line_positions,
              faults_per_core=None,
              chan2csv=chan2csv,
              endtime=endtime,
              timestep=timestep,
              gen_model=gen_model,
              ametr_refa=ametr_refa,
              oosmho_notrip=oosmho_notrip,
              agc_on=agc_on,
              do_run=True,
              do_read=True)
