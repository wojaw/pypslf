import os
from jinja2 import Environment, FileSystemLoader

THIS_DIR = os.path.dirname(os.path.abspath(__file__))
PROJ_DIR = os.path.abspath(os.path.join(THIS_DIR, '..'))
TMPL_DIR = os.path.join(PROJ_DIR, 'case_tmpl', 'base')

pscc18_dir = r"PSCC18_paper\dyds"
user_env = os.environ['COMPUTERNAME']
user_dir = r"c:\Users\jaworskiw\OneDrive - Narodowe Centrum Badan Jadrowych\PSLFProjects" if user_env=='NB36CIS' else \
    r"C:\Users\admin\Desktop\AXA"

j2_env = Environment(loader=FileSystemLoader(TMPL_DIR), trim_blocks=True)


def get_dyd_file(dyd_name, dyd_dir,
                 dyd_template='new39_template.dyd',
                 gen_model='genrou',
                 oosmho_notrip=0, oosmho_rr=1,
                 zpott_tcb=0.15, zpott_t2t=0.05, zpott_t1=0.05,
                 agc_on=True, ltc_on=False, wlwscc_on=True,
                 ametr_refa=0):
    case_dyd = os.path.join(dyd_dir, dyd_name + '.dyd')

    dyd_out = open(case_dyd, 'w')
    dyd_out.write(j2_env.get_template(dyd_template).
                render(gen_model=gen_model,
                       oosmho_notrip=oosmho_notrip, oosmho_rr=oosmho_rr,
                       zpott_tcb=zpott_tcb, zpott_t2t=zpott_t2t, zpott_t1=zpott_t1,
                       agc_on=agc_on, ltc_on=ltc_on, wlwscc_on=wlwscc_on,
                       ametr_refa=ametr_refa))
    dyd_out.close()


# RUN
case_name = 'new39'
gen_model = 'genrou' #'gencls'
gen_trip = False
agc_on = True
abus_ref = 0
dyd_name = '%s_%s_trip%1d_refa%d_agc%d' % (case_name, gen_model, gen_trip, abus_ref, agc_on)
dyd_dir = os.path.join(user_dir, pscc18_dir)

get_dyd_file(dyd_name, dyd_dir,
             gen_model=gen_model,
             oosmho_notrip=1-int(gen_trip),
             oosmho_rr=1,
             agc_on=agc_on)