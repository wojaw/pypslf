import logging

import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.orm import aliased
from sqlalchemy.orm import sessionmaker
from tqdm import tqdm

from datetime  import datetime, timedelta

from py4pslf.costCalculator import Cost

import config
from models import Base, GridModel, Line, WeatherForecast

logger = logging.getLogger('RestoreResults')

logging.basicConfig(level=logging.DEBUG)
# logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

engine = create_engine('sqlite:///' + config.DB_NAME)
# engine = create_engine('sqlite:////home/kgomulski/Pulpit/axa.db')
Base.metadata.bind = engine
Base.metadata.create_all(engine)
DBSession = sessionmaker(bind=engine)
session = DBSession()

w_xls = 'C:\\Users\\user\\Desktop\AXA\\Probabilities_table.xlsx'

if __name__ == "__main__":
    indicators = pd.read_excel(w_xls, 'Control weather', index_col=None, header=None).as_matrix()[1:35, 2:26]
    probs = pd.read_excel(w_xls, 'Probability only one line', index_col=None, header=None).as_matrix()[1:35, 2:26]

    lines = {int(line.name): line for line in
             session.query(Line).join(Line.model).filter(GridModel.name == 'AXAieee39').all()}

    base_datetime = datetime.strptime('20140129-0000','%Y%m%d-%H%M')

    for h in xrange(0,24):
        for l in xrange(0, 34):
            # print indicators[l][h]
            wf = WeatherForecast(line=lines[l],
                                 weather_factor=indicators[l,h],
                                 fault_probability=probs[l,h],
                                 datetime=base_datetime+timedelta(hours=h))
            session.add(wf)
    session.commit()
