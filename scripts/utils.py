import math


def line_to_dic(_line, _pos, t=0):
    """

    :param t:
    :param _pos:
    :param py4pslf.Line _line:
    :return:
    """
    return {
        "num": _line.num,
        "from": _line.fr,
        "to": _line.to,
        "pos": _pos,
        "t": t
    }


def generate_line_faults(lines, line_outages):
    """

    :param dict line_outages:
    :param list[pypslf.Line] lines:
    :return:
    """
    shortest_line = min([line.length for line in lines]) / 2.
    line_faults = {}
    for line in lines:
        if line.num in line_outages:
            continue
        data = [
            line_to_dic(line, 1. / line.length),
            line_to_dic(line, 1 - 1. / line.length),
        ]
        parts = math.floor(line.length / shortest_line)
        if parts > 4:
            parts = 4.
        for p in range(1, int(parts)):
            p += 0.0
            data.append(line_to_dic(line, p / parts))
        line_faults[line.num] = data
    return line_faults
