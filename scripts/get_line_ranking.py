from __future__ import print_function
import logging

import operator
from sqlalchemy import create_engine
from sqlalchemy.orm import aliased
from sqlalchemy.orm import sessionmaker

import config
from datetime import datetime
from models import Base, Simulation, GridModel, Line, Bus, Fault, FaultedLine
from py4pslf.postprocessing.extractor import DBExtractor

logger = logging.getLogger('RestoreResults')

logging.basicConfig(level=logging.DEBUG)
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

engine = create_engine('sqlite:///' + config.DB_NAME)
# engine = create_engine('mysql+pymysql://py4pslf:axa@localhost:3307/axa_pslf', pool_recycle=3600)
Base.metadata.bind = engine
Base.metadata.create_all(engine)
DBSession = sessionmaker(bind=engine)
session = DBSession()

if __name__ == '__main__':
    model = 'AXAieee39'
    base_datetime = datetime.strptime('20160912-1200', "%Y%m%d-%H%M")
    sim_datetime = datetime.strptime('20160912-1300', "%Y%m%d-%H%M")

    dbe = DBExtractor(session, model)

    lines_data = dbe.get_simulation_costs_per_line(base_datetime, sim_datetime)

    loads = lines_data.load.unique()

    for load in loads:
        print("LOAD FACTOR: %.4f" % load)
        print(lines_data[lines_data.load==load].sort_values(by='cost',ascending=False))

        # faults = session.query(Fault.cost_sum_p, FaultedLine.line_id) \
        #     .join(Fault.simulation) \
        #     .join(Fault.faulted_lines) \
        #     .filter(Simulation.base_datetime == base_datetime,
        #             Simulation.sim_datetime == sim_datetime)
        # bus_fr = aliased(Bus)
        # bus_to = aliased(Bus)
        # lines = {line_id: (b_fr, b_to) for line_id, b_fr, b_to in
        #          session.query(Line.id, bus_fr.name, bus_to.name).join(GridModel).join(bus_fr, Line.fr).join(bus_to,
        #                                                                                                      Line.to).filter(
        #              GridModel.name == model).all()}
        #
        # line_ranking = {key: 0 for key in lines.keys()}
        # line_fault_occurrence = {key: 0 for key in lines.keys()}
        #
        # for cost, line_id in faults:
        #     line_ranking[line_id] += cost
        #     line_fault_occurrence[line_id] += 1
        #
        # sorted_lines = sorted(line_ranking.items(), key=operator.itemgetter(1), reverse=True)
        #
        # print(
        #     '\n'.join(
        #         ['{0}-{1} \t: {2:f}'.format(lines[lid][0], lines[lid][1], cost) for lid, cost in sorted_lines]))
        #
        # print(len([l for l in sorted_lines if l[1] > 0]))
