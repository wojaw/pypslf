from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import config
from colorama import Fore
import os
import sys
from models import Base, GridModel, Line, Bus, Load
from py4pslf.io.fromXLSX import FromXLSX
import logging
import pandas as pd

logger = logging.getLogger('initDB')
logging.basicConfig(level=logging.DEBUG)

if os.path.isfile(config.DB_NAME):
    answer = str(raw_input(Fore.RED + "Data Base already exists. Would you like to delete it? (n): ")).strip()
    while answer != 'n' and answer != 'y' and answer != '':
        answer = str(raw_input('(y/n): ')).strip()
    if answer == 'n' or answer=='':
        print('Then bye!')
        sys.exit()
    elif answer == 'y':
        logger.info('Deleting old DB...')
        os.remove(config.DB_NAME)

logger.info('Creating tables for new DB...')
# engine = create_engine('mysql+pymysql://py4pslf:axa@localhost:3307/axa_pslf', pool_recycle=3600)
engine = create_engine('sqlite:///' + config.DB_NAME)
Base.metadata.bind = engine
Base.metadata.create_all(engine)
DBSession = sessionmaker(bind=engine)
session = DBSession()

logger.info('Populating tables with model data')
input_xls = os.path.join(config.WORK_DIR, 'input_new.xlsx')
model = FromXLSX(input_xls, line_sheet='lines', zone_sheet='zones')

# base_load = [l[0] for l in pd.read_excel(input_xls, 'loads', index_col=None, header=None).as_matrix()]

grid_model = GridModel(name='AXAieee39_mod')

done_bus = {}
loads = []
for line in model.lines:
    if line.fr not in done_bus:
        done_bus[line.fr] = Bus(name=line.fr)
    if line.to not in done_bus:
        done_bus[line.to] = Bus(name=line.to)

    db_line = Line(name=line.num, length=line.length)
    grid_model.lines.append(db_line)
    done_bus[line.fr].lines_fr.append(db_line)
    done_bus[line.to].lines_to.append(db_line)

for bus in done_bus.values():
    grid_model.buses.append(bus)

for bus_nr in range(1,40):
    if bus_nr not in done_bus:
        grid_model.buses.append(Bus(name=bus_nr))
session.add(grid_model)
session.commit()

# for nr,load in enumerate(base_load):
#     loads.append(Load(name='Base',
#                       value=float(base_load[nr-1]),
#                       bus_id=nr+1))

# session.bulk_save_objects(loads)
session.commit()

logger.info('Done!')