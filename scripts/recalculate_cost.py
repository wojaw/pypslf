import logging

import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.orm import aliased
from sqlalchemy.orm import sessionmaker
from tqdm import tqdm

from py4pslf.costCalculator import Cost

import config
from models import Base, Load, Event, Fault, Bus, VecBusIsl, MisBusP, GridModel

logger = logging.getLogger('RestoreResults')

logging.basicConfig(level=logging.DEBUG)
# logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

engine = create_engine('sqlite:///' + config.DB_NAME)
Base.metadata.bind = engine
Base.metadata.create_all(engine)
DBSession = sessionmaker(bind=engine)
session = DBSession()

if __name__ == "__main__":
    ooslen_faults = session.query(Event.fault_id.label('fault_id')) \
        .filter(Event.type == 2) \
        .group_by(Event.fault_id) \
        .subquery()

    bus_fr = aliased(Bus)
    bus_to = aliased(Bus)

    faults = session.query(Fault) \
        .join(Fault.simulation) \
        .filter(Fault.id == ooslen_faults.c.fault_id) \
        .all()

    buses = {bus.id: bus for bus in session.query(Bus).join(Bus.model).filter(GridModel.name == 'AXAieee39').all()}
    db_load = session.query(Load).join(Load.bus).filter(Load.name == 'Base').all()

    # base_load = [l.value for l in sorted(db_load, key=lambda x: int(x.bus.id))]
    #
    # for l in db_load:
    #     print('B {0}: {1}'.format(l.bus.name,l.value))

    progress_bar = tqdm(total=len(faults))

    get_bus = lambda b: int(buses[b].name)

    costs = []
    i = 0
    for fault in faults:
        # result = session.query(Result) \
        #     .filter(Result.fault_id == fault.id) \
        #     .first()
        # print result
        events = session.query(Event) \
            .filter(Event.fault_id == fault.id) \
            .all()

        cost_input = {'time': [], 'e_type': [], 'info': []}

        for event in events:
            cost_input['time'].append(event.time)
            cost_input['e_type'].append(event.type.lower())
            if event.type == 'LSDT1':
                cost_input['info'].append({
                    'bus': get_bus(event.fr_id),
                    'stage': event.stage
                })
            elif event.type == 'OOSLEN':
                cost_input['info'].append({
                    'bus': get_bus(event.to_id),
                    'gen': get_bus(event.fr_id)
                })
            else:
                cost_input['info'].append({
                    'to': get_bus(event.to_id),
                    'from': get_bus(event.fr_id)
                })

        cost_estimator = Cost(pd.DataFrame(cost_input), numBusIsl=fault.num_bus_isl,
                              loadIncrease=fault.simulation.load_increase, loadFactor=fault.simulation.load)
        cost = cost_estimator.shedCost
        # costs.append(cost)
        fault.cost_sum_p = cost
        # i += 1
        # if i > 50:
        #     print('\tMax cost: %f'%max(costs))
        #     i = 0
        progress_bar.update(1)
        # print fault
    # print sorted(costs, reverse=True)
    session.commit()
