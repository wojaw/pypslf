import datetime

from sqlalchemy import Column, String, Integer, Float, ForeignKey, DateTime
from sqlalchemy.orm import relationship, synonym
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import ClauseElement

Base = declarative_base()


class Bus(Base):
    __tablename__ = 'bus'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(255))
    model_id = Column(Integer, ForeignKey('grid_model.id'))
    model = relationship("GridModel", back_populates="buses")
    lines_fr = relationship('Line', back_populates="fr", foreign_keys='Line.fr_id')
    lines_to = relationship('Line', back_populates="to", foreign_keys='Line.to_id')


class Line(Base):
    __tablename__ = 'line'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(255))
    length = Column(Float)
    fr_id = Column(Integer, ForeignKey('bus.id'))
    to_id = Column(Integer, ForeignKey('bus.id'))
    fr = relationship("Bus", back_populates="lines_fr", foreign_keys=[fr_id])
    to = relationship("Bus", back_populates="lines_to", foreign_keys=[to_id])
    model_id = Column(Integer, ForeignKey('grid_model.id'))
    model = relationship("GridModel", back_populates="lines")


class Load(Base):
    __tablename__ = 'load'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(255))
    # datetime = Column(DateTime)
    value = Column(Float)
    bus_id = Column(Integer, ForeignKey('bus.id'))
    bus = relationship('Bus')
    simulation_id = Column(Integer, ForeignKey('simulation.id'))
    simulation = relationship('Simulation')


class GridModel(Base):
    __tablename__ = 'grid_model'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(255), nullable=False)
    description = Column(String(255), default='')
    buses = relationship('Bus', back_populates="model")
    lines = relationship('Line', back_populates="model")
    simulations = relationship('Simulation', back_populates="model")


class Simulation(Base):
    __tablename__ = 'simulation'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(255), nullable=False)
    sim_datetime = Column(DateTime, nullable=False)
    base_datetime = Column(DateTime, nullable=False)
    sim_period = Column(Integer)
    load = Column(Float)
    load_increase = Column(Float)
    cases = relationship('Case', back_populates="simulation", cascade="all, delete-orphan, delete")
    faults = relationship('Fault', back_populates="simulation", cascade="all, delete-orphan, delete")
    loads = relationship('Load', back_populates="simulation", cascade="all, delete-orphan, delete")
    model_id = Column(Integer, ForeignKey('grid_model.id'))
    model = relationship("GridModel", back_populates="simulations")
    status = Column(Integer, default=1)
    risk = Column(Float)

class Case(Base):
    __tablename__ = 'case'
    id = Column(Integer, primary_key=True, autoincrement=True)
    computation_start_datetime = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    computation_end_datetime = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    hash = Column(String(255))
    simulation_id = Column(Integer, ForeignKey('simulation.id'))
    simulation = relationship("Simulation", back_populates="cases", cascade="all, delete-orphan, delete",single_parent=True)
    status = Column(Integer, default=1)
    faults = relationship("Fault", back_populates="case")


class Fault(Base):
    __tablename__ = 'fault'
    id = Column(Integer, primary_key=True, autoincrement=True)
    simulation_id = Column(Integer, ForeignKey(
        'simulation.id'))  # Could be the source of inconsistencies when fault.simulation)d!=fault.case.simulation.id
    simulation = relationship("Simulation", back_populates="faults")  #
    case_id = Column(Integer, ForeignKey('case.id'))
    case = relationship("Case", back_populates="faults")
    status = Column(Integer, default=1)
    events = relationship('Event', back_populates="fault")
    faulted_lines = relationship('FaultedLine', back_populates='fault')
    num_bus_isl = Column(Float)
    mis_sum_p = Column(Float)
    gen_sum_p = Column(Float)
    dem_sum_p = Column(Float)
    loss_sum_p = Column(Float)
    cost_sum_p = Column(Float, default=0)
    vec_bus_isl = relationship('VecBusIsl')
    mis_bus_p = relationship('MisBusP')


class Event(Base):
    __tablename__ = 'event'
    id = Column(Integer, primary_key=True, autoincrement=True)
    fault_id = Column(Integer, ForeignKey('fault.id'))
    fault = relationship("Fault", back_populates="events",cascade="all, delete-orphan, delete",single_parent=True)
    _type = Column('type', Integer)
    time = Column(Float)
    fr_id = Column(Integer, ForeignKey('bus.id'))
    to_id = Column(Integer, ForeignKey('bus.id'))
    fr = relationship("Bus", foreign_keys=[fr_id])
    to = relationship("Bus", foreign_keys=[to_id])
    additional_value1 = Column(String(255))
    additional_value2 = Column(String(255))
    additional_value3 = Column(String(255))
    additional_value4 = Column(String(255))
    additional_value5 = Column(String(255))

    _types_to_str = ['OOSLEN', 'ZPOTT', 'LSDT1', 'LOCTI','OOSMHO']
    _str_to_type = {k: v for v, k in enumerate(_types_to_str)}

    @property
    def type(self):
        return self._types_to_str[self._type]

    @type.setter
    def type(self, value):
        self._type = self._str_to_type[value]

    type = synonym('_type', descriptor=type)

    @property
    def stage(self):
        if self._type == 2:
            return int(self.additional_value1)
        else:
            return None

    @stage.setter
    def stage(self, data):
        if type == 2:
            self.additional_value1 = str(data)

    stage = synonym('additional_value1', descriptor=stage)


class FaultedLine(Base):
    __tablename__ = 'faulted_line'
    id = Column(Integer, primary_key=True, autoincrement=True)
    fault_id = Column(Integer, ForeignKey('fault.id'))
    line_id = Column(Integer, ForeignKey('line.id'))
    time = Column(Float)
    position = Column(Float)
    line = relationship('Line')
    fault = relationship('Fault', back_populates='faulted_lines', cascade="all, delete-orphan, delete",single_parent=True)


class VecBusIsl(Base):
    __tablename__ = 'vec_bus_isl'
    id = Column(Integer, primary_key=True, autoincrement=True)
    fault_id = Column(Integer, ForeignKey('fault.id'))
    fault = relationship('Fault', cascade="all, delete-orphan, delete",single_parent=True)
    bus_id = Column(Integer, ForeignKey('bus.id'))
    bus = relationship('Bus')
    val = Column(Integer)


class MisBusP(Base):
    __tablename__ = 'mis_bus_p'
    id = Column(Integer, primary_key=True, autoincrement=True)
    fault_id = Column(Integer, ForeignKey('fault.id'))
    fault = relationship('Fault', cascade="all, delete-orphan, delete",single_parent=True)
    bus_id = Column(Integer, ForeignKey('bus.id'))
    bus = relationship('Bus')
    val = Column(Float)


class WeatherForecast(Base):
    __tablename__ = 'weather_forecast'
    id = Column(Integer, primary_key=True, autoincrement=True)
    datetime = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    line_id = Column(Integer, ForeignKey('line.id'))
    line = relationship('Line')
    weather_factor = Column(Float)
    fault_probability = Column(Float)


def get_or_create(db_session, model, defaults=None, **kwargs):
    """

    :param db_session:
    :param model:
    :param defaults:
    :param kwargs:
    :return:
    """
    instance = db_session.query(model).filter_by(**kwargs).first()
    if instance:
        # logger.debug('Returning existing instance %d of %s' % (instance.id, model.__tablename__))
        return instance, False
    else:
        # logger.debug('Creating new instance of %s' % model.__tablename__)
        params = dict((k, v) for k, v in kwargs.iteritems() if not isinstance(v, ClauseElement))
        params.update(defaults or {})
        instance = model(**params)
        db_session.add(instance)
        return instance, True
