from distutils.core import setup

setup(
    name='dynpslf',
    version='',
    packages=['py4pslf', 'py4pslf.io'],
    url='',
    license='',
    author='Wojciech Jaworski, Krzysztof Gomulski',
    author_email='',
    description='',
    requires=[
        'numpy',
        'pandas',
        'jinja2',
        'xlrd',
        'matplotlib',
        'celery',
        'simplejson',
        'pymysql',
        # 'python-igraph',  #cannot be installed by pip -> http://igraph.org/python/#pyinstallwin
        'SQLAlchemy',
        'colorama',
        'tqdm',
        'oct2py',
        'Jupyter',
    ]
)
