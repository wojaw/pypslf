define num2inc		7
define num2dec		2
/*
define power2move	800 
*/
define steps2move	5


dim #gens2inc[num2inc]
dim #gens2dec[num2dec]

/* Read the case */
@retcas = getf("cases/bw_h15.sav")

/* Gen ids initialization */
#gens2inc[0] = 0
#gens2inc[1] = 1
#gens2inc[2] = 2
#gens2inc[3] = 5
#gens2inc[4] = 6
#gens2inc[5] = 7
#gens2inc[6] = 8

#gens2dec[0] = 3
#gens2dec[1] = 4

/* Pmax calculations */
@pow2inc = 0
for @num = 0 to num2inc-1
	@ng = #gens2inc[@num]
	@pres = gens[@ng].pmax - gens[@ng].pgen
	@pow2inc = @pow2inc + @pres
next

@pow2dec = 0
for @num = 0 to num2dec-1
	@ng = #gens2dec[@num]
	@pres = gens[@ng].pgen
	@pow2dec = @pow2dec + @pres
next

/* Shifthing parameters */
/*
@power2shift = power2move
*/
@steps2shift = steps2move

if (@pow2dec >= @pow2inc)
	@power2shift = @pow2inc
else
	@power2shift = @pow2dec
endif

for @step2shift = 0 to steps2move

	/* Get the shift values */
	logbuf($case4shift, "cases/bw_h15_s",@step2shift)
	$case4shift = $case4shift + ".sav"
	
	@frac2shift = @step2shift / @steps2shift
	@pow2shift  = @frac2shift * @power2shift
	logterm("# ", @step2shift, " # Shifting ", @frac2shift:5:3, " of ", @power2shift:8:3, " = ", @pow2shift:8:3)

	/* Tests */
	/*
	logterm("# inc: ", @pow2inc, "<")
	logterm("# dec: ", @pow2dec, "<")
	logterm("## shift: ", @pow2shift, "<")
	*/

	/* Power shifting */
	@powFrac2inc = @pow2shift / @pow2inc
	for @num = 0 to num2inc-1
		@ng = #gens2inc[@num]
		@pres = gens[@ng].pmax - gens[@ng].pgen
		@pshift = @pres * @powFrac2inc
		gens[@ng].pgen = gens[@ng].pgen + @pshift
		/* logterm("### inc: ", @num, " :: ", @ng, " :: ", @pres, " :: ", @pshift, "<") */
	next

	@powFrac2dec = @pow2shift / @pow2dec
	for @num = 0 to num2dec-1
		@ng = #gens2dec[@num]
		@pres = gens[@ng].pgen
		@pshift = - @pres * @powFrac2dec
		gens[@ng].pgen = gens[@ng].pgen + @pshift
		/* logterm("### dec: ", @num, " :: ", @ng, " :: ", @pres, " :: ", @pshift, "<") */
	next

	@retsol = soln()
	@retsav = savf($case4shift)
next

end