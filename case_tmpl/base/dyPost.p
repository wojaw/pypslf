define MaxCh	64
define MaxBus	39

dim #powerF[MaxBus][2]
dim *datFile[1][MaxCh]

/* INITILIZE & OPEN */
for @nb = 0 to MaxBus-1
	for @i = 0 to 1
		#powerF[@nb][@i] = 0.0
	next
next

@baseFreq = dypar[0].omegao / 6.283185

*datFile[0] = "data\{{fault_name}}.dat"
@retIsld = isld(*datFile[0])

/*
@retSol = soln()
@retSol = buildeff(1)
*/
@retSol = flowcalc(1)
@retDat = setlog(*datFile[0])

/* RUN & WRITE */
gosub getBusIslands
gosub getBranchesData
gosub getTransData
gosub getGensData
gosub getLoadData

/* CLOSE */
@retDat = close(*datFile[0])
@retSav = savf("cases\fN1\{{fault_name}}.sav")

/*
** SUBROUTINES
*/

subroutine getBusIslands
    logprint(*datFile[0], "<b>")
    for @nb = 0 to casepar[0].nbus-1
        @nisl = busd[@nb].islnum
        logprint(*datFile[0], @nisl:3:0, " ")
    next
return

subroutine getBranchesData
    logprint(*datFile[0], "<<")
    for @nb = 0 to casepar[0].nbrsec-1
        $ck = secdd[@nb].ck
        @from = secdd[@nb].ifrom
        @to = secdd[@nb].ito
        @st = secdd[@nb].st
        @rf = secdd[@nb].rate[0]
        @fFr = @baseFreq + netw[@from].f
        @fTo = @baseFreq + netw[@to].f
        /*
        if (@st = 0)
            @pf = 0
        else
            @pf = flow(0, @from, @to, $ck, 1)
        endif
        */
        @pf = flow(0, @from, @to, $ck, 1)

        @apf = abs(@pf)

        if (@pf >= 0.0)
            #powerF[@from][0] = #powerF[@from][0] - @apf
            #powerF[@to][0] = #powerF[@to][0] + @apf
        else
            #powerF[@from][0] = #powerF[@from][0] + @apf
            #powerF[@to][0] = #powerF[@to][0] - @apf
        endif

        logprint(*datFile[0], "B ", @nb+1:4:0, " ", @from+1:4:0, " ", @to+1:4:0, " ", @st:2:0, " ", @rf:8:2, " ", @pf:8:2, " ", @fFr:8:4, " ", @fTo:8:4, "<")
    next
return

subroutine getTransData
    logprint(*datFile[0], "<")
    for @nt = 0 to casepar[0].ntran-1
        $ck = tran[@nt].ck
        @from = tran[@nt].ifrom
        @to = tran[@nt].ito
        @st = tran[@nt].st
        @rf = tran[@nt].rate[0]
        /*
        if (@st = 0)
            @pf = 0
        else
            @pf = flow(0, @from, @to, $ck, 1)
        endif
        */
        @pf = flow(0, @from, @to, $ck, 1)
        @apf = abs(@pf)

        if (@pf >= 0.0)
            #powerF[@from][0] = #powerF[@from][0] - @apf
            #powerF[@to][0] = #powerF[@to][0] + @apf
        else
            #powerF[@from][0] = #powerF[@from][0] + @apf
            #powerF[@to][0] = #powerF[@to][0] - @apf
        endif

        logprint(*datFile[0], "T ", @nt+1:4:0, " ", @from+1:4:0, " ", @to+1:4:0, " ", @st:2:0, " ", @rf:8:2, " ", @pf:8:2, "<")
    next
return

subroutine getGensData
	@sum = 0.0
	logprint(*datFile[0], "<")
	for @ng = 0 to casepar[0].ngenbc-1
		@st = gens[@ng].st
		@pg = gens[@ng].pgen
		@gb = gens[@ng].ibgen
		#powerF[@gb][0] = #powerF[@gb][0] + @pg /** @st*/
		@gt = busd[@gb].type
		@kg = genbc[@ng].kgen
		@pg = gens[@kg].pgen
		@pm = genbc[@ng].pmech
		@an = genbc[@ng].angle
		@fg = @baseFreq + netw[@gb].f

		@sum = @sum + @pg
		logprint(*datFile[0], "G ", @ng+1:4:0, " ", @gb+1:4:0, " ", @st:2:0, " ", @gt:2:0, " ", @pg:8:3, " ", @pm:8:3, " ", @an:8:3, " ", @fg:8:4, "<")
	next
	logprint(*datFile[0], "G_tot ", @sum:8:3, "<")
return

subroutine getLoadData
	@sumLgen = 0.0
	@sumLdem = 0.0
	logprint(*datFile[0], "<")
	for @nl = 0 to casepar[0].nload-1
		@st = load[@nl].st
		@lb = load[@nl].lbus
		@pb = load[@nl].p
		@pl = #powerF[@lb][0]

		@sumLgen = @sumLgen + @pl
		@sumLdem = @sumLdem + @pb
		logprint(*datFile[0], "L ", @nl+1:4:0, " ", @lb+1:4:0, " ", @st:2:0, " ", @pb:8:3, " ", @pl:8:3, "<")
	next
	logprint(*datFile[0], "L_tot ", @sumLdem:8:3, " ", @sumLgen:8:3, "<")
return

subroutine chf2csvParser
	@chf2csv = chan2csv(1, 0)
return

end