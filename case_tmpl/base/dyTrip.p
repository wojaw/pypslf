/* logterm("#@#@# RUNNING DyTrip !!!<") */

/* Define values - some will be used as array dimensions */
/* define MaxBus		39
define MaxBranch		39*38/2 */
define MaxGen			1536
define MaxChar			16
define MaxStrLen		512
/* Change these from default values */
define TimeTest			0.1
define FlagBranch		0
define FlagGen			1
define WarnBranch		1.00
define TripBranch		1.25
define TripGen			180

/* Initialize arrays */
dim #refGenAng[MaxGen], #refGenKix[MaxGen], #refGenPg[MaxGen], #lossGenPg[2]
dim *logStr[1][MaxStrLen]

/* Set the output files */
/*
$namFile = "chans\G" + format(FlagGen, 1, 0) + "L" + format(FlagBranch, 1, 0)
$logFile = $namFile + ".log"
$PFtFile = $namFile + "pf.tsv"
$LstFile = $namFile + "st.tsv"

@retLog = setlog($logFile)
@retPFt = setlog($PFtFile)
@retLst = setlog($LstFile)
*/
/* Get numbers from casepar and dypar */
@numBus = casepar[0].nbus
@numBranch = casepar[0].nbrsec
@numGen = casepar[0].ngen
@numMod = dypar[0].nmodels

@timeMin = (1.5 * dypar[0].delt * -1)

if ((dypar[0].time < 0) and (dypar[0].time > @timeMin)) 
	
	/* Loop Through Array And NULL Out Values */
	for @ng = 0 to MaxGen-1
		#refGenAng[@ng] = -9999
		#refGenKix[@ng] = -1
	next

	/* Loop Through All Models */
	for @nm = 0 to @numMod-1
		/* Store Model Name / Number */
		@noMod = model[@nm].mod_lib_no

		/* Find All True Generators (No SVC or STATCON Models) In Database And Store Information About The Units */
		if ((modlib[@noMod].type = "g") and (modlib[@noMod].name != "vwscc") and (modlib[@noMod].name != "stcon"))
			/* Store Model Index Number */
			@kIxMod = model[@noMod].k
			
			if(@kIxMod < 0)
				continue
			endif
			
			/* Store Generator Bus Index Number */
			@kIxGen = genbc[@kIxMod].kgen
			/* Store Absolute Generator Angle */
			#refGenAng[@kIxGen] = abs(genbc[@kIxMod].angle - dypar[0].ref_angle)
			/* Store Model Index Number In Array  */
			#refGenKix[@kIxGen] = @kIxMod
			/* Store Generator MW Ouput */
			#refGenPg[@kIxGen] = gens[@kIxGen].pgen * gens[@kIxGen].st
		endif
	next
	
	@timeDelta = 0
	#lossGenPg[0] = 0
	#lossGenPg[1] = 0
	
/* Find Models And Set Values After INIT */
else
	@time = dypar[0].time
	@timeTest = @time - @timeDelta

	if (@timeTest > TimeTest)
		@timeDelta = @timeDelta + TimeTest
		
		/* Check Machine Rotor Angles */
		gosub GenTest
		
		/* Check Branch Power Flow */
		/* gosub BranchTest */

	endif
endif
/*
@retLog = close($logFile)
@retPFt = close($PFtFile)
@retLst = close($LstFile)
*/
end

subroutine GenTest
	logdy("<Testing Machine Rotor Angles At ", @time:5:2, " Seconds<")

	/* Loop Through All Generators */
	for @ng = 0 to @numGen-1

		if (#refGenAng[@ng] >= 0)
			/* Store Generator Terminal Bus Index Number */
			@ixBus = gens[@ng].ibgen
			/* Store Generator Terminal Bus External Number */
			@exBus = busd[@ixBus].extnum

			@stGen = gens[@ng].st

			/* If Generator Is Online */
			if (@stGen = 1)                                       
				/* Locate Associated Model Index Num. */
				@kIx = #refGenKix[@ng]
				/* Store New Absolute Generator Angle */
				@angGen = abs(genbc[@kIx].angle - dypar[0].ref_angle)
				/* Store Generator Rotor Speed */
				@spdGen = genbc[@kIx].speed
				/* Calculate Absolute Angle Difference */
				@angDiff = abs(#refGenAng[@ng] - @angGen)

				/* If The Absolute Angle Difference > TripGen (= 180 Degrees by default) */
				if (@angDiff > TripGen)
					/* Store Generator Area Number */
					@area = busd[@ixBus].area
					/* Store Area Index Number */
					@areaIx = rec_index(1, 7, @area, 0, "**", 1, -1)

					/* If Pgen Is Positive */
					if(#refGenPg[@ng] > 0)
						/* Add Up Positive Lost Gen MW */
						#lossGenPg[0] = #lossGenPg[0] + #refGenPg[@ng]
					else
						/* Add Up Negative Lost Gen MW */
						#lossGenPg[1] = #lossGenPg[1] + #refGenPg[@ng]
					endif

					/* If Trip Flag Set To 1 (Allow Unit Tripping) */
					if (FlagGen >= 1)
						/* Set P.F. Gen Status = 1 */
						gens[@ng].st = 0

						/* Loop Through All Models */
						for @nm = 0 to @numMod-1
							/* Find Associated Models With Same Bus Index   */
							if (model[@nm].bus = @ixBus)
								/* Turn Model Status = 0                        */
								model[@nm].st = 0
							endif
						next

						/* ?WJ? */
						dypar[0].new_fact = 1
					endif

					/*  Report Which Units Were Tripped And Amount Of Generation MW Lost */
					logdy("Tripped unit ", busd[@ixBus].busnam:MaxChar, " ", busd[@ixBus].basekv:6:2)
					logdy(" [", gens[@ng].id:2, "] at time ", @time:5:2, " seconds ")
					logdy("in area ", area[@areaIx].arnum:3:0, " ", area[@areaIx].arname:20, ".<")
					logdy("Tripped ", #refGenPg[@ng]:4:0, " for this unit.<")
					logdy("Positive MW loss = ", #lossGenPg[0]:6:0, " for all units.<")
					logdy("Negative MW loss = ", #lossGenPg[1]:6:0, " for all units.<<")

				endif
			endif
		endif
	next
return

subroutine BranchTest
	logterm("<Testing Branch PowerFlows At: ", dypar[0].time:5:2, " Seconds<")
	@retFlo = flowcalc(1)

	/* Loop Through All Branches */
	/*
	logprint($PFtFile, @time:10:5, ">")
	logprint($LstFile, @time:10:5, ">")
	*/
	for @nb = 0 to @numBranch-1
		$ck = secdd[@nb].ck
		@from = secdd[@nb].ifrom
		@to = secdd[@nb].ito
		@st = secdd[@nb].st
		@rf = secdd[@nb].rate[0]
		/* @pf = flow(0, @from, @to, $ck, 1) */
		
		if (@st > 0)
			@fix = flow_index(busd[@from].extnum, busd[@to].extnum, $ck)
		
			if (@fix >= 0)
				@pf = flox[@fix].p
				/* @pu = flox[@fix].pul */
			endif

			@apf = abs(@pf) * @st
			@pu = @apf / @rf
		else
			@apf = 0
			@pu = 0
		endif
		
		/*	
		logprint($PFtFile, @apf:8:3, ">")
		logprint($LstFile, @st:1:0, ">")
		*/
		if (@st = 1)
			/* logterm(@time:10:5, " Line", @nb+1:4:0, " from ", @from+1:4:0, " to ", @to+1:4:0, " PF ", @apf:6:2, " DIV ", @rf:6:2, " EQ ", @pu:4:2, "<") */
			/* logprint($logFile, @time:10:5, " Line", @nb+1:4:0, " from ", @from+1:4:0, " to ", @to+1:4:0, " PF ", @apf:6:2, " DIV ", @rf:6:2, " EQ ", @pu:4:2, "<") */

			if (@pu >= TripBranch)
				/* logterm(" GE ", TripBranch:3:2, " :: Tripped <") */
				/* logprint($logFile, " GE ", TripBranch:3:2, " :: Tripped <") */

				if (FlagBranch = 1)
					secdd[@nb].st = 0
				endif

			/*
			elseif (@pu >= WarnBranch)
				 logterm(" GE ", WarnBranch:3:2, " :: Overloaded <") 
				 logprint($logFile, " GE ", WarnBranch:3:2, " :: Overloaded <") 

			else
				 logterm(" LT ", WarnBranch:3:2, " :: Checked <") 
				 logprint($logFile, " LT ", WarnBranch:3:2, " :: Checked <") 
			*/

			endif
		endif
	next
	/*
	logprint($PFtFile, "<")
	logprint($LstFile, "<")
	*/
return