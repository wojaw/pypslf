/*
The script to obtain 1 casefiles.sav from 1 Roman's file
Made with substituting the demand historical values with the predicted ones
The prediction for hourFin is made with mean and std values obtained for hourIni
*/

define hours	24
define maxCh	64
define maxSt	3

/* from Roman for hourIni */
$baseCase = "cases/{{case_name}}.sav"
/* to be created for hourIni and hourFin */
$newCase = "cases/{{case_new_name}}.sav"

/* get Roman's case */
@retCase = getf($baseCase)

@numGen  = casepar[0].ngen
@numLoad = casepar[0].nload

/* get the new factor */
@factor = {{load_factor}}

/* change Pgen values */
for @ng = 0 to @numGen-1
	gens[@ng].pgen = gens[@ng].pgen * gens[@ng].st * @factor
next

/* change Pload and Qload values */
for @nl = 0 to @numLoad-1
	load[@nl].p = load[@nl].p * @factor
	load[@nl].q = load[@nl].q * @factor
next
/* solve PowerFlow in order to get Qgen */
@retSol = soln()

/* save the new case */
@retSave = savf($newCase)




