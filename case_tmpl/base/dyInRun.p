/* definitions - the question mark (?) => consider introducing the value as an external parameter */
define LMAX		19				/* max num of loads (?) */
define TFACT	3600			/* time over which the load changes [s] - default = 1 h (?) */
define LFACT	{{sFact}}		/* factor of the load change over time = TFACT */
define TSTEP	0.1				/* timestep of load changes and island check calculations (?) */
define LSTEP	TFACT/TSTEP		/* number of steps for load changes */
define INC		0				/* if 0 - loads changing WILL NOT be performed */
define ISL		0				/* if 0 - island isolation WILL NOT be performed */
define GEN		0				/* if 0 - genbc reading WILL NOT be performed */

/* declaration of each load P & Q step values */ 
dim #Pstep[LMAX]
dim #Qstep[LMAX]

/* current time value */
@timeDy = dypar[0].time

/* initial information */
if (@timeDy < 0)
	@timeDelta = 0
		
	/* set the P & Q step sizes */
	for @nl = 0 to casepar[0].nload-1
		#Pstep[@nl] = LFACT * load[@nl].p / LSTEP
		#Qstep[@nl] = LFACT * load[@nl].q / LSTEP
	next
/* calculations performed ... */
else
	@timeTest = @timeDy - @timeDelta
	
	/* ... every timestep */
	if(@timeTest > TSTEP)
		@timeDelta = @timeDelta + TSTEP
	
		/* isolate islands */
		if (ISL <> 0)
			@retIsl = isolnoswgislds()
			
			@dropIslPd = @retIsl.number[0]
			@dropIslPg = @retIsl.number[0]

			if (@dropIslPd * @dropIslPg <> 0)
				logdy("### InRunIslandIsolation at t = ", @timeDy:8:3, ": Pd = ", @dropIslPd:8:3, " Pg = ", @dropIslPg:8:3, "<")
			endif
		endif	

		/* increase P & Q */
		if (INC <> 0)
			for @nl = 0 to casepar[0].nload-1
				@pl = load[@nl].p + #Pstep[@nl]
				@ql = load[@nl].q + #Qstep[@nl]

				load[@nl].p = @pl
				load[@nl].q = @ql

				/* @Psum = @Psum + @pl */
			next
		endif
	endif
endif

/*
** SUBROUTINES
*/

subroutine getGenBCdata
	@time = dypar[0].time
	
	for @kgen = 0 to casepar[0].ngen-1
		/* @modindex = genbc[@kgen].modindex */

		@angle = genbc[@kgen].angle
		@speed = genbc[@kgen].speed

		@pelec = genbc[@kgen].pelec
		@pmech = genbc[@kgen].pmech

		@efd = genbc[@kgen].efd
		@ifd = genbc[@kgen].ladifd

		/*
		@vref = genbc[@kgen].vref
		@vsig = genbc[@kgen].vsig
		*/
		logdy(@time:7:2, @kgen:3:0, @angle:7:2, @speed:7:2, @efd:7:2, @ifd:7:2, @pelec:9:4, @pmech:9:4, "<")
	next
return