/*******************************************************************************
	DYTOOLS post processor to write out all channel data to ascii text file
	in the format
	channel header in a row followed by
	channel data in the next row
	
	Input  : 
		runs file

		
	Output : 
		.out file containing channel data
	
	Created : BT 
			  10/05/2010	

********************************************************************************/

define MAXPOINTS  10000
define MaxLines   9999

dim  *des[8][48], *inp[8][25]
dim  #abc[MAXPOINTS]
dim  #ord[MAXPOINTS]
dim  *prefix[1][32] 
dim  *chffile[1][64]    /* [.chf] channel file name                       */
dim  *pathout[1][64]    /* path to put  .chf and .log files               */
dim  *outfile[1][64]    /* path to put  .chf and .log files               */
dim  *junk[1][64], *test[1][65]      /* junk */
dim  *stablst[1][64]
dim  *buffer[1][132]

/* start spread time post-fault */ 
/* plotpar[0].start_spread = 0  */ 

label mainpanel
	/*
  @ret = pick("","*.runs","")
  *stablst[0] = ret[0].string[0]
	*/
	*stablst[0] = "dyN1.runs"
	@ret = setinput( *stablst[0] )
   
for @ii = 0 to MaxLines

	@ret = inline( *stablst[0], *buffer[0]    )  
	@ret = inbuf(*buffer[0],$txt)
	$txt = tolower($txt)
	logbuf($test,$txt:4)
	if ( $test = "endt" )
		@ret = inbuf(*buffer[0],$junk,@endtime)
	    if ( @ret < 0 )
			logterm(" Error reading list file line ",*buffer[0],"<<")
			end
		endif
	elseif ($test = "chfp" )
		@ret = inbuf(*buffer[0],$junk,*pathout[0])
	    if ( @ret < 0 )
			logterm(" Error reading list file line ",*buffer[0],"<<")
			end
		endif
	elseif ($test = "case" )
		quitfor
	endif
next
	/* first set up some defaults */
	*inp[0] = "0"
	*des[0] = "Start Time secs"

	/*
	@ret    = panel("ASCII OUTPUT",*des[0], *inp[0], 1, 1)
	if (@ret < 0 )
		end
	endif
	*/
	@tstrt  = atof(*inp[0])
	

for @loop = 0 to 999
	@ret = input( *stablst[0], $test, *junk[0], *junk[0],*junk[0], *junk[0], *chffile[0] )
	if ( (@ret < 0) or ($test = "-1")  or ( $test = "end") or ( $test = "END")  )
	    quitfor
	endif
		
	if ( ($test = "#") or ($test = " ") or ($test = ""))
		continue
	endif
	@dot   = findstr(*chffile[0],".")
	*prefix[0] = ""
	
	for  @i = 0 to @dot-1
		$txt = " "
		logbuf($txt,*chffile[0][@i]:1)
		*prefix[0] = *prefix[0] + $txt
	next
	logterm(" Prefix = ", *prefix[0],"<")
   	*outfile[0] = "plots\\" +  *prefix[0] + " ascii.out"
   	@ret = openlog(*outfile[0])

/* now add in the directory path for the chf file */
  *chffile[0]   = *pathout[0] + *chffile[0]
	logterm(" Chf file is ", *chffile[0],"<")

	plotpar[0].start_spread = @tstrt

/* now print out the worst overall voltage dip at a load or gen bus */
	@ret = getp(*chffile[0])
	if ( @ret < 0 )
		logterm(" Error opening .chf file ", *chffile[0],"<")
		continue
	endif

/* save data from all chanels to a csv file */
	@ret = chan2csv(1, 0)
	if ( @ret < 0 )
		logterm(" Error creating .csv file from ", *chffile[0],"<")
		continue
	endif
	
	for @num = 0 to 19999 /* find the number of points */
			if (selx[0].kchan[@num] < 1)
					quitfor
			endif
	next
	if (@num > MAXPOINTS)
			logterm("Dimension error! ")
			logterm("10000 is not big enough for abc and ord arrays<")
			logterm("increase it to 1 more than ",@num,"<")
			continue
	endif
	
	
	gosub output 


 	@ret = close(*outfile[0]) 	  



next	/* next on the loop through cases */

end

   
subroutine output

	

 	logprint(*outfile[0]," DATE REPORT MADE - ")
 	$ret = date(*outfile[0])
 	logprint (*outfile[0],"<")
 	logprint (*outfile[0],"Input File  ",*chffile[0],"<")
	for @it = 0 to 4
		logprint(*outfile[0],plotpar[0].title[@it],"<")
	next
	for @it = 0 to 14
		logprint(*outfile[0],plotpar[0].comment[@it],"<")
	next


 	logprint (*outfile[0],"Ascii data from the chf file","<")
 	logprint (*outfile[0],"Channel Header [index-plotnumber-modelname-modeltype-frombus-tobus-id-ckt-cmin-cmax]<")
 	logprint (*outfile[0],"---------Data---------<<")
 
 
	for @i = 0 to plotpar[0].nchan-1

		@ii = @i + 1
		@plotnum = channel[@i].pselect
		$type   = channel_head[@i].type
		$modnam = channel_head[@i].modelname
		$id     = channel_head[@i].id


		/*logterm(@ii:4:0,">",channel[@i].pselect,">",channel_head[@i].type,">",channel_head[@i].modelname,">","<")*/
		@ch = getchan(@plotnum,$id,$type,@num)
		if (@ch < 0)
				goto done2
		endif
		
		
		@cmin = channel_head[@i].cmin
		@cmax = channel_head[@i].cmax
		logprint(*outfile[0],@i:6:0,">",channel[@i].pselect:6:0,">",channel_head[@i].modelname:8:0,">",channel_head[@i].type:4:0,">")
		logprint(*outfile[0],channel[@i].bus,">",channel[@i].to,">",channel[@i].cid,">",channel[@i].ck,">",@cmin:5:4,">",@cmax:5:4,"<")

		for @j = 0 to @num-1
				/* logterm(@i:6,">", #abc[@j]:5:4,">",#ord[@j]:5:4,"<") */
				logprint(*outfile[0],#ord[@j]:5:4,",")
		next
		logprint(*outfile[0],"<")


	next
	logprint(*outfile[0],"TIME points <")
	for @j = 0 to @num-1
			/* logterm(@i:6,">", #abc[@j]:5:4,">",#ord[@j]:5:4,"<") */
			logprint(*outfile[0],#abc[@j]:5:4,",")
	next

	logprint(*outfile[0],"<<")
	logprint(*outfile[0],"Total of ",@i," channels written to [",*outfile[0],"]<<")
	logterm("Total of ",@i," channels written to [",*outfile[0],"]<<")

label done2:

return
