from runner.tasks import app, run_case, read_results
from py4pslf.mcCase import MCCase
import os
import shutil
import config
import json

app.conf.update(
    CELERY_ALWAYS_EAGER=True
)

if __name__ == '__main__':
    test_input = dict(
        id='TEST',
        case="20160912-1500",
        horizon="20160912-1600",
        time=1800,
        faults=[{'num': '20',
                 'from': '3',
                 'to': '4',
                 'pos': 0.5,
                 't': 0}],
        gen={
            30: 235.0,
            31: 533.57,
            32: 611.0,
            33: 594.0,
            34: 477.0,
            35: 611.0,
            36: 526.0,
            37: 507.0,
            38: 780.0},
        load=1.,
        load_increase=.2,
        outages=[],
        sim_id=None)

    workspace = config.WORK_DIR
    case_dir = os.path.join(config.WORK_DIR, '..', 'bez_wiatru')

    try:
        shutil.rmtree(os.path.join(workspace, test_input['case'] + '_' + test_input['id']))
    except Exception as e:
        print(e)

    run_case(json.dumps(test_input), workspace, case_dir)
    # read_results(json.dumps(test_input), workspace)
