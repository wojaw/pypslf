from __future__ import absolute_import
import logging
import time
import json
from os import path, mkdir, listdir
import re
import shutil
from jinja2 import TemplateNotFound

from py4pslf import dyCase as dyC
from py4pslf import pslfRunner as dyR
from py4pslf import PSLFExcerption
from py4pslf.postprocessing.reader import get_event_chain, get_loss_data
from runner.celery import app
from py4pslf.mcCase import MCCase

logger = logging.getLogger('CeleryTasks')
logging.basicConfig(level=logging.DEBUG)


class TaskResult(object):
    """
    Class that represents task results
    """

    def __init__(self, status, runtime, data=None, comment=''):
        self.status = status
        self.runtime = runtime
        self.data = data
        self.comment = comment


def read_fault_results(fault, workdir):
    """

    :param fault:
    :param py4pslf.mcCase.Fault:
    :param str workdir:
    :return:
    """
    work_path = path.join(workdir, 'results')
    fault.events = get_event_chain(work_path, fault.data_file_name())
    fault.result = get_loss_data(work_path, fault.data_file_name())
    return fault


def get_workdir(case, workspace):
    """

    :param case:
    :param workspace:
    :return:
    """

    date_format = '%Y%m%d_%H%M'
    if case.simulation_id is not None:
        return path.join(workspace, '{0}-{1}-S{2}'.format(case.base_datetime.strftime(date_format),
                                                          case.sim_datetime.strftime(date_format),
                                                          case.simulation_id))
    return path.join(workspace, '{0}-{1}-L{2}'.format(case.base_datetime.strftime(date_format),
                                                      case.sim_datetime.strftime(date_format),
                                                      case.load_factor_str))


def copy_files_from_dir(src, dst):
    """

    :param src:
    :param dst:
    :return:
    """
    for name, full_path in [(f, path.join(src, f)) for f in listdir(src)]:
        if path.isfile(full_path):
            shutil.copy(full_path, path.join(dst, name))


def search_for_errors(log_file):
    with open(log_file, 'r') as log:
        is_error = re.search(ur'Completed Run', log.read(), re.IGNORECASE)
        if not is_error:
            raise PSLFExcerption('Errors in log file')


@app.task
def run_case(case, workspace, case_dir):
    """

    :param case_dir:
    :param str case: Case representing simulation
    :param str workspace:
    :return:
    """
    # print(case)
    case_dict = json.loads(case)
    case = MCCase.from_dict(case_dict)

    workdir = get_workdir(case, workspace)
    tmp_workdir = '{0}_{1}'.format(workdir, case.id)
    # os_tmp = os.getenv('TMP')
    # os.environ['TMP'] = tmp_workdir
    # os.environ['TEMP'] = tmp_workdir
    try:
        mkdir(workdir)
    except:
        pass
    for dir_ in ['cases', 'logs', 'results']:
        try:
            mkdir(path.join(workdir, dir_))
        except:
            pass
    try:
        mkdir(tmp_workdir)
    except WindowsError as e:
        logger.error(e)
        return TaskResult('ERROR', 0, data={'case_id': case.id}, comment=e.message)

    t0 = time.time()

    dyC.dyCase('bw_h{:02d}'.format(24 if case.sim_datetime.hour == 0 else case.sim_datetime.hour),
               case_dir=case_dir,
               workspace=tmp_workdir,
               case=case,
               # prerun_file='dyPre_f50L06_tG31.p',
               endTime=case.sim_period)
    run = dyR.pslfRunner(workspace=tmp_workdir, silent=True)
    run.createLoadVariants()
    run.runStatic()
    run.runDytools()

    try:
        search_for_errors(path.join(tmp_workdir, 'term.log'))
    except PSLFExcerption as e:
        return TaskResult('ERROR', 0, data={'case_id': case.id}, comment=e.message)
    except IOError as e:
        return TaskResult('ERROR', 0, data={'case_id': case.id}, comment=e.message)

    copy_files_from_dir(path.join(tmp_workdir, 'chans'),
                        path.join(workdir, 'results'))

    # shutil.copy(path.join(tmp_workdir, 'term.log'), path.join(workdir, 'logs', 'log_' + case.id + '.txt'))

    case_dump = open(path.join(workdir, 'cases', case.id + '.txt'), 'wb')
    json.dump(case.to_dict(), case_dump)
    case_dump.close()

    shutil.rmtree(tmp_workdir)

    return TaskResult('DONE', time.time() - t0)


@app.task
def read_results(case, workspace):
    """
    Celery task from parsing the results
    :param str case: Case representing simulation
    :param str workspace:
    :return:b
    """
    t0 = time.time()
    case_dict = json.loads(case)
    case = MCCase.from_dict(case_dict)
    try:
        workdir = get_workdir(case, workspace)
    except:
        workdir = workspace
    faults = []
    error = None
    for fault in case.faults:
        # try:
        faults.append(read_fault_results(fault, workdir))
        # except IOError as e:
        #     logger.error(e)
        #     error = e
        # except AttributeError as e:
        #     logger.error(e)
        #     error = e
        # except TypeError as e:
        #     logger.error(e)
        #     error = e
        # except KeyError as e:
        #     logger.error(e)
        #     error = e
    case.faults = faults
    return TaskResult('DONE', time.time() - t0, data=case,
                      comment='{0} - {1} : {2}'.format(workdir, case.id, str(error)) if error else None)


@app.task
def run_single_case(case, case_dir, results_dir, files, timestep=0.005, chan2csv=False):
    """

    :param case_dir:
    :param str case: Case representing simulation
    :param str workspace:
    :return:
    """
    # print(case)
    case_dict = json.loads(case)
    case = MCCase.from_dict(case_dict)

    workdir = path.join(results_dir, case.id)
    tmp_workdir = '{0}_{1}'.format(workdir, case_dict['hash'])
    # os_tmp = os.getenv('TMP')
    # os.environ['TMP'] = tmp_workdir
    # os.environ['TEMP'] = tmp_workdir
    try:
        mkdir(workdir)
    except:
        pass
    for dir_ in ['cases', 'logs', 'results']:
        try:
            mkdir(path.join(workdir, dir_))
        except:
            pass
    try:
        mkdir(tmp_workdir)
    except WindowsError as e:
        logger.error(e)
        return TaskResult('ERROR', 0, data={'case_id': case.id}, comment=e.message)

    t0 = time.time()
    #
    # print case_dir
    try:
        dyC.UniversalDyCase(case.name,
                            case_dir=case_dir,
                            workspace=tmp_workdir,
                            case=case,
                            prerun_file=files['pre'] if 'pre' in files else None,
                            postrun_file=files['post'] if 'post' in files else None,
                            inrun_file=files['inrun'] if 'inrun' in files else None,
                            endTime=case.sim_period,
                            timestep=timestep)
    except TemplateNotFound as e:
        shutil.rmtree(tmp_workdir)
        return TaskResult('ERROR', 0, data={'case_id': case.id}, comment='Template not found %s'%str(e))
    run = dyR.pslfRunner(workspace=tmp_workdir, silent=True)
    # run.createLoadVariants()
    # run.runStatic()
    run.runDytools(ch2a=chan2csv)

    try:
        search_for_errors(path.join(tmp_workdir, 'term.log'))
    except PSLFExcerption as e:
        return TaskResult('ERROR', 0, data={'case_id': case.id}, comment=e.message)
    except IOError as e:
        return TaskResult('ERROR', 0, data={'case_id': case.id}, comment=e.message)

    copy_files_from_dir(path.join(tmp_workdir, 'chans'),
                        path.join(workdir, 'results'))

    # shutil.copy(path.join(tmp_workdir, 'term.log'), path.join(workdir, 'logs', 'log_' + case.id + '.txt'))

    case_dump = open(path.join(workdir, 'cases', case_dict['hash']+'.txt'), 'wb')
    json.dump(case.to_dict(), case_dump)
    case_dump.close()
    #
    #
    shutil.rmtree(tmp_workdir)

    return TaskResult('DONE', time.time() - t0)
