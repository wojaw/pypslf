from __future__ import absolute_import
import os
import simplejson as json
import logging
from functools import partial
from hashlib import sha1
import importlib
import sys
from jinja2 import Environment, FileSystemLoader

from celery.result import ResultSet
from tqdm import tqdm
from runner.tasks import run_single_case

THIS_DIR = os.path.dirname(os.path.abspath(__file__))
PROJ_DIR = os.path.abspath(os.path.join(THIS_DIR, '..'))


class case_runner(object):
    logger = logging.getLogger('Run case')
    logging.basicConfig(level=logging.DEBUG)
    # skip celery que  FOR DEVELOPMENT ONLY
    # app.conf.update(CELERY_ALWAYS_EAGER=True)
    dyd_template = 'new39_template.dyd'


    @staticmethod
    def partition(alist, indices):
        return [alist[i:j] for i, j in zip([0] + indices, indices + [None])]


    def result_callback(self, bar, task_id, val):
        """
        Function that is called every time when worker finishes his job
        :param tqdm.tqdm bar: progress bar instance
        :param str task_id: Task id
        :param runner.tasks.TaskResult val: Task result
        :return:
        """
        prefix = '\n' #'\t[{0}/{1}]'.format(counter.val, total_task_num)

        if val.status == 'ERROR':
            self.logger.info('{3} Errors in task {0} for case {1} : {2}'.format(task_id, val.data['case_id'], val.comment, prefix))

        bar.update(1)


    def __init__(self, workspace, result_dir=None, config='case.py', how_to_run='from_dir', dict_to_run=None,
                 timestep=0.005, default_per_core=15, chan2csv=False):
        assert os.path.isdir(workspace)

        self.workspace = workspace
        # if results directory is not specified then write to the same workspace
        self.result_dir = result_dir if result_dir else workspace
        self.config = config
        self.timestep = timestep
        self.how_to_run = how_to_run
        self.dict_to_run = dict_to_run
        self.default_per_core = default_per_core
        self.chan2csv = chan2csv
        self.dyd_dir = os.path.join(self.workspace, 'dyds')
        self.tmpl_dir = os.path.join(PROJ_DIR, 'case_tmpl', 'base')
        self.j2_env = Environment(loader=FileSystemLoader(self.tmpl_dir), trim_blocks=True)

        # initialize the resulting list
        self.results = list()


    def run(self):
        # decide how to run
        if self.how_to_run == 'from_dir':
            self.run_from_dir()
        elif self.how_to_run == 'from_dict':
            self.run_from_dict(dict_to_run=self.dict_to_run)
        else:
            print('### It cannot be run this way: how_to_run = from_dir | from_dict')
            return

        result_set = ResultSet(self.results)
        progress_bar = tqdm(total=len(self.results))
        callback = partial(self.result_callback, progress_bar)
        result_set.join_native(callback=callback, propagate=True)


    def run_from_dir(self):
        # import the workspace ...
        sys.path.insert(0, self.workspace)
        # get the module name without '.py' ...
        case_module = importlib.import_module(self.config.split('.')[0])
        # take the module's INPUT ...
        INPUT = case_module.INPUT

        # parse and use the INPUT to run the cases ...
        self.run_from_dict(dict_to_run=INPUT)


    def run_from_dict(self, dict_to_run=None):
        assert dict_to_run or self.dict_to_run
        INPUT = dict_to_run if dict_to_run else self.dict_to_run

        for case_name, case_data in INPUT['cases'].items():
            # if 'id' in case_data:
            #     case_data['id'] = case_name

            base = {key: case_data[key] for key in ['id', 'time', 'gen', 'load', 'load_increase', 'outages', 'pre_sav_name']}
            base['sim_id'] = case_name

            ind = range(0, len(case_data['faults']), INPUT['fault_per_core'] if 'fault_per_core' in INPUT else self.default_per_core)
            faults = self.partition(case_data['faults'], ind[1:])

            for f in faults:
                base['faults'] = f
                base['name'] = case_name
                base['hash'] = sha1(repr(f)).hexdigest()

                self.results.append(run_single_case.apply_async((json.dumps(base), self.workspace, self.result_dir,
                                                                 INPUT['files'], self.timestep, self.chan2csv)))


    def get_dyd_files(self, INPUT, gen_model='genrou',
                      oosmho_notrip=0,
                      zpott_tcb=0.15, zpott_t2t=0.05, zpott_t1=0.05,
                      agc_on=True, ltc_on=False,
                      ametr_refa=0):
        for case in INPUT['cases'].keys():
            case_dyd = os.path.join(self.dyd_dir, case + '.dyd')

            dyd_out = open(case_dyd, 'w')
            dyd_out.write(self.j2_env.get_template(self.dyd_template).
                        render(gen_model=gen_model, oosmho_notrip=oosmho_notrip,
                               zpott_tcb=zpott_tcb, zpott_t2t=zpott_t2t, zpott_t1=zpott_t1,
                               agc_on=agc_on, ltc_on=ltc_on, ametr_refa=ametr_refa))
            dyd_out.close()