from __future__ import absolute_import
from celery import Celery
import os

import config

app = Celery('runner',
             broker='amqp://guest:guest@localhost//',
             backend='rpc://',
             include=['runner.tasks'])

app.conf.update(
    CELERY_RESULT_BACKEND='rpc',
    CELERY_TASK_RESULT_EXPIRES=36000,
    # CELERYD_STATE_DB=os.path.join(config.WORK_DIR, 'worker_state.db')
)

if __name__ == '__main__':
    app.start()
