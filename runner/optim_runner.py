from __future__ import absolute_import
from oct2py import octave as oc
# from mlab.releases import latest_release as mlb
from jinja2 import Environment, FileSystemLoader
import pandas as pd
import os, shutil, json
from copy import deepcopy
from openpyxl import load_workbook

from py4pslf.pslfRunner import pslfRunner
from py4pslf.postprocessing.risk_reader import risk_reader
from py4pslf.mcCase import UnitGeneration
from runner.case_runner import case_runner
from scripts.get_dyd_file import get_dyd_file

THIS_DIR = os.path.dirname(os.path.abspath(__file__))
PROJ_DIR = os.path.abspath(os.path.join(THIS_DIR, '..'))

class optim_runner(object):
    matpower = r"C:\Octave\matpower6.0"
    scripts_tmpl = {
        'sav2mpc': 'sav2mpc.p',
        'opf2sav': 'opf2sav.p',
        'dyd': 'new39_template.dyd'
    }

    num_gen = 10
    case_dict = { 'time': 1800, 'load': 1, 'load_increase': 0 }
    init_pmax = [ 1040.0, 646.0, 725.0, 917.0, 508.0, 800.0, 816.0, 564.0, 865.0, 1100.0 ]
    init_pmin = [ 416.0, 258.4, 290.0, 366.8, 203.2, 320.0, 326.4, 225.6, 346.0, 880.0 ]
    mpc_pmax = None


    def __init__(self, base_name, base_dir,
                 case_id='',
                 iteration=0,
                 # xl_line_probs=('Probabilities_new39.xlsx', 'lines'),
                 xl_line_probs=('input_new.xlsx', 'probs_raw'),
                 which_lines=None,
                 const_gens=list(),
                 faults_per_core=20,
                 fault_time=0,
                 fault_positions=[50], # in percent [%]
                 iteration_power_step=3, # in percent [%]
                 run_risk_ass=True,
                 optimization='scopf',
                 oosmho_rr=1,
                 wlwscc_on=True
                 ):

        self.case_id = case_id
        self.iteration = iteration
        self.iteration_power_step = iteration_power_step
        self.base_name = base_name
        # self.base_name = base_name if self.iteration==0 else self.iter2str(pre=base_name+'_')
        self.base_id = (self.base_name + '_' + self.case_id) if self.case_id else self.base_name
        self.fault_positions = fault_positions
        self.fault_time = fault_time
        self.run_risk_ass = run_risk_ass # if self.iteration==0 else False # ???
        self.optimization = optimization if optimization else 'scopf'
        self.oosmho_rr = oosmho_rr
        self.wlwscc_on = wlwscc_on

        # prepare and check the directories
        self.base_dir = base_dir
        assert os.path.isdir(self.base_dir)
        self.cases_dir = os.path.join(self.base_dir, 'cases')
        assert os.path.isdir(self.cases_dir)
        self.dyds_dir = os.path.join(self.base_dir, 'dyds')
        assert os.path.isdir(self.base_dir)
        self.scripts_dir = os.path.join(self.base_dir, 'scripts')
        if not os.path.exists(self.scripts_dir): os.mkdir(self.scripts_dir)
        self.result_dir = os.path.join(self.base_dir, 'results')
        if not os.path.exists(self.result_dir): os.mkdir(self.result_dir)

        ## initialize globals:
        # ... for jinja
        self.tmpl_dir = os.path.join(PROJ_DIR, 'case_tmpl', 'base')
        self.j2_env = Environment(loader=FileSystemLoader(self.tmpl_dir), trim_blocks=True)
        # ... for oct2py
        oc.addpath(self.matpower)
        oc.addpath(self.cases_dir)
        # ... for mlab
        # mlb.addpath(self.cases_dir)
        # ... for pslf
        self.pslf = pslfRunner(workspace=self.base_dir, silent=True)

        # ... case files ...
        self.base_sav = os.path.join(self.cases_dir, self.base_name + '.sav')
        self.base_dyd = os.path.join(self.dyds_dir, self.base_name + '.dyd')
        assert os.path.isfile(self.base_sav)
        # assert os.path.isfile(self.base_dyd)
        if not os.path.isfile(self.base_dyd):
            get_dyd_file(dyd_dir=self.dyds_dir, dyd_name=self.base_name,
                         oosmho_rr=self.oosmho_rr, wlwscc_on=self.wlwscc_on)

        if self.iteration == 0:
            shutil.copyfile(self.base_sav, self.base_sav.replace('.sav', self.iter2str(pre='_', post='.sav')))
            shutil.copyfile(self.base_dyd, self.base_dyd.replace('.dyd', self.iter2str(pre='_', post='.dyd')))
        else:
            shutil.copyfile(self.base_sav.replace('.sav', self.iter2str(pre='_', post='.sav', add_it=-1)),
                            self.base_sav.replace('.sav', self.iter2str(pre='_', post='.sav')))
            shutil.copyfile(self.base_dyd.replace('.dyd', self.iter2str(pre='_', post='.dyd', add_it=-1)),
                            self.base_dyd.replace('.dyd', self.iter2str(pre='_', post='.dyd')))



        # read lines: from, to, fault probabilities
        self.which_lines = which_lines
        self.xl_line_probs = (os.path.join(self.base_dir, xl_line_probs[0]), xl_line_probs[1])
        self.lines_data = self.get_lines_data()
        # ... & the generators to remain constant
        self.const_gens = const_gens

        # prepare to run the dyn. sim.
        # self.case_runner = case_runner(workspace=self.base_dir, result_dir=self.result_dir, how_to_run='from_dict')

        self.dict_to_run = {
            'fault_per_core': faults_per_core,
            'files': { 'pre': 'dyPre.p', 'post': 'dyCost.p' },
            'cases': {},
        }


    def get_input(self):
        # prepare the input
        case_data = self.sav2mpc()
        gen_data = self.run_opf(case_data['case'], optimization=self.optimization)
        case_name = case_data['case']

        case_dict = deepcopy(self.case_dict)
        case_dict['id'] = '_'.join([case_name, self.case_id]) if self.case_id else case_name
        case_dict['gen'] = gen_data['list']
        case_dict['outages'] = list()
        case_dict['faults'] = self.get_input_faults()
        # saving the case.sav for next iteration - corrected with opf2sav() in run_opf()
        # case_dict['pre_sav_name'] = os.path.join('..', '..', 'cases', case_name.replace(self.iter2str(), self.iter2str(add_it=1)))
        case_dict['pre_sav_name'] = None

        dict_input = deepcopy(self.dict_to_run)
        dict_input['cases'][case_name] = case_dict

        # save the INPUT dict for every iteration
        py_name = self.iter2str(pre='input_', post='.py')
        py_file = os.path.join(self.scripts_dir, py_name)
        py_out = open(py_file, 'w')
        py_out.write('INPUT = \n')
        py_out.write(json.dumps(dict_input, indent=2))
        py_out.close()

        return { 'dict_input': dict_input, 'gen_data': gen_data, 'case_name': case_dict['id'] }


    def run1iteration(self):
        iter_input = self.get_input()
        gen_df = iter_input['gen_data']['df']
        # if self.iteration == 0:
        #     self.init_pmax = gen_df.g_pmax.tolist()

        # run the dynamic simulations
        # self.case_runner.dict_to_run = iter_input['dict_input']
        # self.case_runner.run()
        if self.run_risk_ass:
            iter_runner = case_runner(workspace=self.base_dir, result_dir=self.result_dir,
                                      how_to_run='from_dict', dict_to_run=iter_input['dict_input'])
            iter_runner.run()
        self.run_risk_ass = True

        # set the resulting dir name
        work_path = os.path.abspath(os.path.join(self.result_dir, iter_input['case_name']))
        work_dir = os.path.join(work_path, 'results')

        # get the generator risks
        gen_risk_df = self.get_risk_gens(work_dir=work_dir, gen_df=gen_df)
        gen_risk_df = self.pmax4opf(gen_risk_df)

        # TODO - verify!
        self.mpc_pmax = gen_risk_df.g_pmax.tolist()
        self.mpc_pmax = [ min(p) for p in zip(gen_risk_df.g_pmax.tolist(), self.init_pmax) ]

        # saving the case.dyd for the next iteration - corrected with opf2sav() in run_opf()
        # shutil.copyfile(self.base_dyd, self.base_dyd.replace('.dyd', self.iter2str(pre='_', post='.dyd')))

        # preparing the output from OPF
        # TODO - cost & VoLL in the same units
        gen_cost = iter_input['gen_data']['cost'] / 1000
        gen_risk = float(sum(gen_risk_df.g_risk))
        gen_risk_lsd = float(sum(gen_risk_df.g_risk_lsd))
        gen_risk_oos = float(sum(gen_risk_df.g_risk_oos))
        gen_risk_ovl = float(sum(gen_risk_df.g_risk_ovl))
        gen_total = gen_cost + gen_risk
        gen_solv = iter_input['gen_data']['success']

        gen_pmax = sum(gen_risk_df['g_pmax'])
        gen_pgen = sum(gen_risk_df['g_pgen'])
        gen_pratio = (gen_pmax - gen_pgen) / gen_pmax

        gen_NM_margin = sum(gen_risk_df['g_NM_margin'])
        gen_CM_step = sum(gen_risk_df['g_CM_step'])

        gen_oos_trips = int(sum(gen_risk_df.g_trips))

        # reporting ...
        print('\n### Iteration: %02d :: cost = %.1f, risk = %.1f, total = %.1f, oos_trips = %d' % (
            self.iteration, gen_cost, gen_risk, gen_total, gen_oos_trips))

        # cleaning ...
        logs_dir = os.path.join(work_path, 'logs')
        if os.path.isdir(logs_dir):
            shutil.rmtree(logs_dir)
        result_files = os.listdir(work_dir)
        for resfile in result_files:
            if resfile.endswith(".log"):
                os.remove(os.path.join(work_dir, resfile))

        result = { 'iter': self.iteration, 'cost': gen_cost, 'total': gen_total, 'solv': gen_solv,
                   'oos_trips': gen_oos_trips, 'pratio': gen_pratio, 'CM_step': gen_CM_step, 'NM_margin': gen_NM_margin,
                    'risk': gen_risk, 'risk_lsd': gen_risk_lsd, 'risk_oos': gen_risk_oos, 'risk_ovl': gen_risk_ovl }

        # increasing the iteration number!
        self.iteration += 1

        return result


    def run(self, max_iter=99, min_risk=0):
        test = True
        results = list()

        result_out = open(os.path.join(self.base_dir, self.base_id + '_results.json'), 'a')
        result_out.write('[')

        while test:
            result = self.run1iteration()
            results.append(result)

            result_out.write('\n')
            result_out.write(json.dumps(result, indent=2))
            result_out.write(',')

            # TODO - change the testing condition..?
            tests = list()
            tests.append(self.iteration <= max_iter)
            tests.append(result['solv'] == 1)
            tests.append(result['risk'] > min_risk)
            # tests.append(result['pratio'] <= 0 or result['pratio'] > 0.001)
            tests.append(result['CM_step'] > 0)

            test = all(tests)

        result_out.write('\b\n]')
        result_out.close()

        result_df = pd.DataFrame(results)
        xlName = self.base_id + '_results.xlsx'
        xlWriter = pd.ExcelWriter(os.path.join(self.base_dir, xlName), engine='xlsxwriter')
        result_df.to_excel(xlWriter, sheet_name='data', index=False)
        xlWriter.save()

        ## clean..!
        for dydfile in os.listdir(self.dyds_dir):
            if dydfile.endswith(".dyd"):
                os.remove(os.path.join(self.dyds_dir, dydfile))


    @staticmethod
    def get_fault(line, lines, position, time=0):
        return {'num': line, 'from': lines[int(line)][0], 'to': lines[int(line)][1], 'pos': position, 't': time}


    def get_input_faults(self):
        all_positions = [ pos/100.0 for pos in self.fault_positions ]
        line_positions = { str(line_id): all_positions for line_id in range(self.lines_data['num']) }

        faults = list()
        for line, positions in line_positions.items():
            faults += map(lambda p: self.get_fault(line, self.lines_data['pairs'], p, self.fault_time), positions)

        return faults


    def iter2str(self, add_it=0, pre='', post=''): #, sep=''):
        ins = '%03d' % (self.iteration + add_it)
        # return sep.join([pre, ins, post])
        return pre + ins + post


    def sav2mpc(self, cases_dir='cases'):
        # if self.mpc_pmax is not None the default value = -1 is set for all the generators (the pmax will be taken from the case.scv)
        gen_pmax = self.mpc_pmax if self.mpc_pmax else [ -1 for _ in range(self.num_gen) ]
        for g in self.const_gens:
            gen_pmax[g] = -2

        case_name = self.iter2str(pre=self.base_name + '_')

        sav_name = case_name + '.sav'
        sav_case = os.path.join(cases_dir, sav_name)

        mpc_name = case_name + '.m'
        mpc_case = os.path.join(cases_dir, mpc_name)

        p_name = self.iter2str(pre='sav2mpc_', post='.p')
        p_file = os.path.join(self.scripts_dir, p_name)

        # create the sav2mpc
        p_out = open(p_file, 'w')
        p_out.write(self.j2_env.get_template(self.scripts_tmpl['sav2mpc']).
                    render(sav_case=sav_case, mpc_case=mpc_case, case_name=case_name, gen_pmax=gen_pmax))
        p_out.close()

        self.pslf.runScript(p_file)

        return { 'sav': sav_name, 'mpc': mpc_name, 'case': case_name }


    def opf2sav(self, generation, cases_dir='cases'):
        """
        Create the cases: sav and dyd file for the next iteration
        :param generation:
        :param cases_dir:
        :return:
        """
        sav_name = self.iter2str(pre=self.base_name + '_', post='.sav')
        sav_case = os.path.join(cases_dir, sav_name)
        opf_sav_name = self.iter2str(pre=self.base_name + '_', post='.sav', add_it=1)
        opf_sav_case = os.path.join(cases_dir, opf_sav_name)

        p_name = self.iter2str(pre='opf2sav_', post='.p')
        p_file = os.path.join(self.scripts_dir, p_name)
        p_out = open(p_file, 'w')
        p_out.write(self.j2_env.get_template(self.scripts_tmpl['opf2sav']).
                    render(sav_case=sav_case, opf_sav_case=opf_sav_case, generation=generation, scenario=''))
        p_out.close()

        # make the case.sav with the OPF results
        self.pslf.runScript(p_file)
        # copy the case.dyd without changes
        shutil.copyfile(self.base_dyd, self.base_dyd.replace('.dyd', self.iter2str(pre='_', post='.dyd', add_it=1)))

        return opf_sav_case

    def run_opf(self, mpc_name, GB=0, PG=1, VG=5, PD=2, PMAX=8, PMIN=9,
                start_iteration = -1, optimization='scopf'):
        assert os.path.isfile(os.path.join(self.cases_dir, mpc_name+'.m')), 'Error: mpc case file does not exist'
        # mpc = oc.loadcase(mpc_name)
        # mpc = oc.runpf(mpc_name)

        if self.iteration > start_iteration:
            if optimization == 'scopf':
                # mpc = mlb.run39_scopf(mpc_name)
                # oc.push('const_gens', self.const_gens)
                mpc = oc.run39_scopf_oct(mpc_name) #, self.const_gens)
                # gen_cost = sum(mpc.gen[:, PG] * mpc.gencost[:, 5])
            else:
                mpopt = oc.mpoption('verbose', 0)
                mpc = oc.runopf(mpc_name, mpopt)
                # mpopt = mlb.mpoption('verbose', 0)
                # mpc = mlb.runopf(mpc_name) #, mpopt)
                # gen_cost = mpc.f
        else:
            mpopt = oc.mpoption('verbose', 0)
            mpc = oc.runpf(mpc_name, mpopt)
            # gen_cost = sum(mpc.gen[:, PG] * mpc.gencost[:, 5])

        gen_costs = mpc.gencost[:, 5]
        gen_cost = sum(mpc.gen[:, PG] * gen_costs)
        assert sum(mpc.bus[:, PD]) <= sum(mpc.gen[:, PMAX]), 'Error: sum(PMAX) < sum(PD)'

        if mpc.success == 1:
            gen_keys = [ 'id', 'pgen', 'pmax', 'bus', 'vsched' ]
            gen_vals = [ -1 for _ in gen_keys ]
            gen_num = len(mpc.gen)
            gen_id = range(gen_num)
            gen_list = [ dict(zip(gen_keys, gen_vals)) for _ in gen_id ]
            gen_bus = list(mpc.gen[:, GB])
            gen_pgen = list(mpc.gen[:, PG])
            gen_pmax = list(mpc.gen[:, PMAX])
            gen_pmin = list(mpc.gen[:, PMIN])
            gen_vsched = list(mpc.gen[:, VG])

            for gid in gen_id:
                gen_list[gid]['id'] = int(gid)

                # g_bus = int(gen_bus[gid])
                # gen_list[gid]['bus'] = g_bus
                # gen_pow = gen_pgen[gid]
                # gen_list[gid]['pgen'] = gen_pow
                # gen_list[gid]['pmax'] = gen_pow if gid in self.const_gens else gen_pmax[gid]
                # gen_list[gid]['pmin'] = gen_pow if gid in self.const_gens else gen_pmin[gid]

                gen_list[gid]['bus'] = int(gen_bus[gid])
                gen_list[gid]['pgen'] = gen_pgen[gid]
                gen_list[gid]['pmax'] = gen_pmax[gid]
                gen_list[gid]['pmin'] = gen_pmin[gid]

                gen_list[gid]['vsched'] = gen_vsched[gid]
                gen_list[gid]['g_fixed'] = gid in self.const_gens

                gen_list[gid]['g_gencost'] = gen_costs[gid]

            gen_list = sorted(gen_list, key=lambda g: g['id'])
            gen_df = map(lambda g: { 'g_oos': g['bus'], 'g_id': g['id'], 'g_fixed': g['g_fixed'],
                                     'g_pgen': g['pgen'], 'g_pmax': g['pmax'], 'g_pmin': g['pmin'],
                                     'g_vsched': g['vsched'], 'g_gencost': g['g_gencost'] },
                         gen_list)
            gen_df = pd.DataFrame(gen_df)
            gen_units = [ UnitGeneration(gen) for gen in gen_list ]

            gen_dict = { 'list': gen_list, 'cost': gen_cost, 'df': gen_df, 'success': mpc.success, 'units': gen_units }

            # save OPF results into a new case.sav - for the next iteration
            self.opf2sav(gen_units)

        else:
            gen_dict = { 'list': None, 'cost': None, 'df': None, 'success': mpc.success }

        return gen_dict


    def get_lines_data(self, prob_col_id=2):
        assert os.path.isfile(self.xl_line_probs[0]) and prob_col_id >= 2

        lines_df = pd.read_excel(self.xl_line_probs[0], sheetname=self.xl_line_probs[1])
        from_col = lines_df.columns[0]
        to_col = lines_df.columns[1]
        prob_col = lines_df.columns[prob_col_id]

        which_lines = self.which_lines if self.which_lines else range(len(lines_df))

        line_pairs = [ list(line) for line in lines_df.loc[which_lines, [from_col, to_col]].values ]
        line_pairs = map(lambda pair: (int(pair[0]), int(pair[1])), line_pairs)
        line_probs = list(lines_df.loc[which_lines, prob_col].values)
        line_dict = { 'pairs': line_pairs, 'probs': line_probs, 'num': len(line_pairs) }

        return line_dict


    def get_risk_gens(self, work_dir, gen_df):
        num_positions = len(self.fault_positions)
        line_probs = self.lines_data['probs']
        line_positions = self.fault_positions
        line_pairs = self.lines_data['pairs']
        res_risk = risk_reader(work_dir=work_dir,
                               line_probs=line_probs, line_pairs=line_pairs, line_positions=line_positions,
                               include_oos_trips=True)

        # save the events data frames in XL
        xlName = self.base_id + '_events.xlsx'
        shName = self.iter2str(pre='iter_')
        xlPath = os.path.join(self.base_dir, xlName)
        xlWriter = pd.ExcelWriter(xlPath, engine='openpyxl')
        if self.iteration > 0 and os.path.isfile(xlPath):
            xlBook = load_workbook(xlPath)
            xlWriter.book = xlBook
            xlWriter.sheets = dict((ws.title, ws) for ws in xlBook.worksheets)
        res_risk['event'].to_excel(xlWriter, sheet_name=shName, index=False)
        xlWriter.save()

        gen_risk = res_risk['gen']
        none_risk = gen_risk is None
        if not none_risk:
            gen_risk = gen_risk.merge(gen_df, on='g_oos')

            gen_test = gen_risk.groupby(['l_id', 'l_pos']).agg({'g_pgen': lambda g: sum(g)})
            gen_test.reset_index(inplace=True)
            gen_test.columns = ['l_id', 'l_pos', 'g_pgen_tot']

            gen_risk = gen_risk.merge(gen_test, on=('l_id', 'l_pos'))
            gen_risk['g_df'] = gen_risk.g_pgen / gen_risk.g_pgen_tot
            gen_risk['g_risk'] = gen_risk.g_df * gen_risk.l_risk
            gen_risk['g_risk_lsd'] = gen_risk.g_df * gen_risk.l_risk_lsd
            gen_risk['g_risk_oos'] = gen_risk.g_df * gen_risk.l_risk_oos
            gen_risk['g_risk_ovl'] = gen_risk.g_df * gen_risk.l_risk_ovl
        else:
            gen_risk = gen_df
            # gen_risk['g_df'] = 0
            gen_risk['g_risk'] = 0
            gen_risk['g_risk_lsd'] = 0
            gen_risk['g_risk_oos'] = 0
            gen_risk['g_risk_ovl'] = 0

        final_gen_risk = gen_risk.groupby(['g_id', 'g_oos']).agg({
            'g_risk': lambda g: sum(g) / num_positions,
            'g_risk_lsd': lambda g: sum(g) / num_positions,
            'g_risk_oos': lambda g: sum(g) / num_positions,
            'g_risk_ovl': lambda g: sum(g) / num_positions,
            'g_oos': 'count',
        })
        final_gen_risk.g_oos = int(not none_risk) * final_gen_risk.g_oos
        final_gen_risk.rename(columns={'g_oos': 'g_trips'}, inplace=True)
        final_gen_risk.reset_index(inplace=True)
        final_gen_risk = gen_df.merge(final_gen_risk, how='outer').fillna(0)

        return final_gen_risk


    def pmax4opf(self, gen_risk):
        iter_step = self.iteration_power_step / 100.0
        # TODO - check if: gen_risk.g_pmax => self.init_pmax !!!
        gen_risk['g_margin'] = gen_risk.g_pmax - gen_risk.g_pgen

        risk_limit_NM = 0
        gen_risk['g_CM'] = gen_risk.g_risk > risk_limit_NM

        # Define CM and NM, checking if we  have enough margin in NM
        count_gen = 0
        enough_margin = False
        not_fixed = (gen_risk.g_fixed == False)

        gen_risk['g_CM_step'] = 0
        gen_risk['g_NM_margin'] = 0
        gen_risk['g_CM_limit'] = 0

        while not enough_margin or count_gen == self.num_gen:
            gen_risk.g_CM_step = gen_risk.g_pgen * gen_risk.g_CM * iter_step * not_fixed
            gen_risk.g_NM_margin = gen_risk.g_margin * (gen_risk.g_CM == False) * not_fixed
            step_CM = sum(gen_risk.g_CM_step.values)
            margin_NM = sum(gen_risk.g_NM_margin.values)

            if step_CM > margin_NM:
                gen_risk.g_CM_limit = gen_risk.g_risk * gen_risk.g_CM * not_fixed
                risk_limits = list(gen_risk.g_CM_limit[gen_risk.g_CM_limit > 0])
                risk_limit_NM = min(risk_limits) if risk_limits else 0
                gen_risk.g_CM = gen_risk.g_risk > risk_limit_NM

                count_gen += 1
            else:
                enough_margin = True

        # Generation shifted
        gen_risk.g_CM_step = gen_risk.g_pgen * gen_risk.g_CM * iter_step * not_fixed
        gen_risk.g_NM_margin = gen_risk.g_margin * (gen_risk.g_CM == False) * not_fixed
        gen_risk.g_CM_limit = gen_risk.g_risk * gen_risk.g_CM * not_fixed

        pgen_shift_CM = sum(gen_risk.g_CM_step.tolist())
        risk_shift_CM = sum(gen_risk.g_CM_limit.tolist())

        if risk_shift_CM == 0:
            gen_risk.g_CM_limit = 0
        else:
            gen_risk.g_CM_limit /= risk_shift_CM

        # new Pmax..!
        # print '\n', self.iteration, '0', gen_risk.g_pmax.tolist()
        gen_risk.g_pmax = gen_risk.g_pmax * ((gen_risk.g_CM == False) | (gen_risk.g_CM & gen_risk.g_fixed)) +\
                          (gen_risk.g_pgen * gen_risk.g_CM * not_fixed) - (gen_risk.g_CM_limit * pgen_shift_CM)
        # print self.iteration, '1', gen_risk.g_pmax.tolist()
        gen_risk.g_pmax = [ max(p) for p in zip(gen_risk.g_pmax.tolist(), self.init_pmin) ]
        # print self.iteration, '2', gen_risk.g_pmax.tolist()
        gen_risk.g_pmax = [ min(p) for p in zip(gen_risk.g_pmax.tolist(), self.init_pmax) ]
        # print self.iteration, '3', gen_risk.g_pmax.tolist()
        # TODO - test
        gen_risk.g_pmin = self.init_pmin

        # writing the resulting data frame to XL
        xlName = self.base_id + '_gen_data.xlsx'
        shName = self.iter2str(pre='iter_')
        xlPath = os.path.join(self.base_dir, xlName)
        xlWriter = pd.ExcelWriter(xlPath, engine='openpyxl')
        if self.iteration > 0 and os.path.isfile(xlPath):
            xlBook = load_workbook(xlPath)
            xlWriter.book = xlBook
            xlWriter.sheets = dict((ws.title, ws) for ws in xlBook.worksheets)
        gen_risk.to_excel(xlWriter, sheet_name=shName, index=False)
        xlWriter.save()

        return gen_risk




