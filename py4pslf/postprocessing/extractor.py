from sqlalchemy.orm import aliased
import pandas as pd

from models import GridModel, Line, Bus, Fault, FaultedLine, Simulation


class DBExtractor(object):
    def __init__(self, db_session, model='AXAieee39'):
        self._db_session = db_session
        bus_fr = aliased(Bus)
        bus_to = aliased(Bus)
        self._lines = {line_id: (b_fr, b_to) for line_id, b_fr, b_to in
                       db_session.query(Line.id, bus_fr.name, bus_to.name).join(GridModel).join(bus_fr, Line.fr).join(
                           bus_to,
                           Line.to).filter(
                           GridModel.name == model).all()}

    def get_simulation_costs_per_line(self, base_datetime, sim_datetime):
        """

        :param datetime.datetime base_datetime:
        :param datetime.datetime sim_datetime:
        :return pandas.DataFrame:
        """
        fault_data = self._db_session.query(Fault.cost_sum_p, FaultedLine.line_id, Simulation.load) \
            .join(Fault.simulation) \
            .join(Fault.faulted_lines) \
            .filter(Simulation.base_datetime == base_datetime,
                    Simulation.sim_datetime == sim_datetime)

        tmp = {}

        for cost, line_id, load in fault_data:
            if load not in tmp:
                tmp[load] = {key: [0, 0] for key in self._lines.iterkeys()}  # (cost, occurence
            tmp[load][line_id][0] += cost
            tmp[load][line_id][1] += 1

        res_raw = []

        for load, lines in tmp.iteritems():
            for line_id, (cost, count) in lines.items():
                res_raw.append({
                    'line': line_id,
                    'cost': cost / count if count > 0 else 0,
                    'load': load
                })

        return pd.DataFrame(res_raw)

    @property
    def lines_dict(self):
        return self._lines
