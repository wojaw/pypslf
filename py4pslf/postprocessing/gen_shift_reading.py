# import os
from py4pslf.postprocessing.reader import *
from py4pslf.costCalculator import Cost
import pandas as pd
import os

tdir = 'C:\\Users\\user\\Desktop\\AXA\\WJ\\Tests\\new_lines'
shift_stages = [0] #range(0, 6)
bus_loads = [ 0.000, 0.000, 354.200, 550.000, 0.000, 0.000, 257.180, 574.200, 0.000, 0.000, 0.000, 8.250, 0.000, 0.000, 352.000, 362.340, 0.000, 173.800, 0.000, 748.000, 301.400, 0.000, 272.250, 339.460, 246.400, 152.900, 309.100, 226.600, 311.850, 0.000, 10.120, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 1214.400, ]
# pairs = Cost.fromToPairs
pairs = [ [1, 2], [1, 30], [2, 3], [18, 25], [2, 25], [3, 4], [3, 18], [4, 5], [4, 14], [5, 6], [5, 8], [6, 7], [6, 11], [7, 8],
        [8, 9], [9, 30], [10, 11], [10, 13], [13, 14], [14, 15], [15, 16], [16, 17], [16, 20], [13, 20], [16, 21], [16, 24],
        [17, 18], [23, 20], [17, 27], [21, 22], [22, 23], [23, 24], [4, 30], [1, 3], [24, 28], [24, 29], [25, 26], [26, 27],
        [26, 28], [15, 20], [26, 29], [6, 14], [16, 28], [28, 29] ]
lines = range(len(pairs))
positions = [ 1, 25, 50, 75, 99 ]
colnames = ['shift_stage', 'line_id', 'line_from', 'line_to', 'position', 'ev_time', 'ev_from', 'ev_to', 'ev_type', 'ev_stage', 'cost']

events_df_list = list()
costs_df_list = list()

for shift_stage in shift_stages:
    # wdirname = os.path.join(tdir, 'results\\new_case_s%d\\results' % shift_stage)
    wdirname = os.path.join(tdir, 'results\\new39opf\\results')

    # inserting data
    for line in lines:
        for position in positions:
            # print 'Processing events for shift_stage = %d: line = %2d -> pos = %2d' % (shift_stage, line+1, position)

            fname = 'fP%d_L%d_T000' % (position, line)
            evchain = get_event_chain(wdirname, fname)

            if len(evchain) == 0:
                print('%d :: %s ==> NO EVENTS!!!' % (shift_stage, fname))
                continue

            cost_input = {'time': [], 'e_type': [], 'info': [] }
            costs_list = list()
            ev_input = { 'from': [], 'to': [], 'stage': [] }

            for evnum, event in enumerate(evchain):
                event_fr = int(event.fr) if event.fr != '' else event.fr
                event_to = int(event.to) if event.to != '' else event.to
                event_type = event.type.lower()

                cost_input['time'].append(event.dt)
                cost_input['e_type'].append(event_type)

                if event_type == 'lsdt1':
                    cost_input['info'].append({
                        'bus': event_fr,
                        'stage': event.stage,
                    })
                elif event.type.startswith('oos'): # event.type == 'oosmho' or event.type == 'ooslen':
                    cost_input['info'].append({
                        'bus': event_to,
                        'gen': event_fr,
                    })
                elif event_type == 'zpott':
                    cost_input['info'].append({
                        'to': event_to,
                        'from': event_fr,
                        'stage': event.stage,
                    })

                    if evnum == 0:
                        line_fr = event_fr
                        line_to = event_to
                        line_id = filter(lambda l: pairs[l][0] == event_fr and pairs[l][1] == event_to, lines)[0] + 1

                else:
                    cost_input['info'].append({
                        'to': event_to,
                        'from': event_fr,
                    })

                cost_df = pd.DataFrame(cost_input)
                cost_estimator = Cost(cost_df, busBaseLoads=bus_loads, fromToPairs=pairs)
                cost = cost_estimator.shedCost

                costs_list.append(cost)
                ev_input['from'].append(event_fr)
                ev_input['to'].append(event_to)
                ev_input['stage'].append(event.stage)

                events_df = pd.DataFrame()
                events_df['ev_time'] = cost_df['time']
                events_df['ev_type'] = cost_df['e_type']
                events_df['cost'] = costs_list
                events_df['ev_from'] = ev_input['from']
                events_df['ev_to'] = ev_input['to']
                events_df['ev_stage'] = ev_input['stage']
                events_df['shift_stage'] = shift_stage
                events_df['position'] = position
                events_df['line_id'] = line_id
                events_df['line_from'] = line_fr
                events_df['line_to'] = line_to

                events_df = events_df[colnames]

            costs_df = events_df.tail(1)[[ colnames[c] for c in [0, 1, 2, 3, 4, 10] ]]

            events_df_list.append(events_df)
            costs_df_list.append(costs_df)

# Writing to XL
all_events_df = pd.concat(events_df_list)
all_costs_df = pd.concat(costs_df_list)
xlWriter = pd.ExcelWriter(os.path.join(tdir, 'new39_z200_1agc_flt0_opf_dem110.xlsx'), engine='xlsxwriter')
all_events_df.to_excel(xlWriter, 'events', index=False)
all_costs_df.to_excel(xlWriter, 'costs', index=False)
xlWriter.save()
