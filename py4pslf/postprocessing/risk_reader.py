import pandas as pd

from py4pslf.postprocessing.reader import *
from py4pslf.costCalculator import Cost

# Default parameters
work_dir = 'c:\\Users\\wojaw\\OneDrive\\PSLFProjects\\my_tests\\gen_shifts\\oosmho\\all_1129\\results'
line_positions = [ 1, 25, 50, 75, 99 ]
# line_pairs = Cost.fromToPairs
line_pairs = [[1, 2], [1, 3], [1, 30], [2, 3], [2, 25], [3, 4], [3, 18], [4, 5], [4, 14], [4, 30], [5, 6], [5, 8], [6, 7], [6, 11], [6, 14], [7, 8], [8, 9], [9, 30], [10, 11], [10, 13], [13, 14], [13, 20], [14, 15], [15, 16], [15, 20], [16, 17], [16, 20], [16, 21], [16, 24], [16, 28], [17, 18], [17, 27], [18, 25], [21, 22], [22, 23], [23, 20], [23, 24], [24, 28], [24, 29], [25, 26], [26, 27], [26, 28], [26, 29], [28, 29]]
# line_pairs = [[1, 2], [1, 30], [2, 3], [18, 25], [2, 25], [3, 4], [3, 18], [4, 5], [4, 14], [5, 6], [5, 8], [6, 7], [6, 11], [7, 8],
#               [8, 9], [9, 30], [10, 11], [10, 13], [13, 14], [14, 15], [15, 16], [16, 17], [16, 20], [13, 20], [16, 21], [16, 24],
#               [17, 18], [23, 20], [17, 27], [21, 22], [22, 23], [23, 24], [4, 30], [1, 3], [24, 28], [24, 29], [25, 26], [26, 27],
#               [26, 28], [15, 20], [26, 29], [6, 14], [16, 28], [28, 29]]
bus_loads = [0.0, 0.0, 322.0, 500.0, 0.0, 0.0, 233.8, 522.0, 0.0, 0.0, 0.0, 7.5, 0.0, 0.0, 320.0, 329.4, 0.0,
            158.0, 0.0, 680.0, 274.0, 0.0, 247.5, 308.6, 224.0, 139.0, 281.0, 206.0, 283.5, 0.0, 9.2, 0.0, 0.0,
            0.0, 0.0, 0.0, 0.0, 0.0, 1104.0]

# TODO - verify and consider moving oos costs to costCalculator!


def tupelize(this):
    return tuple([gg for gg in sorted(list(this))])


def risk_reader(work_dir=work_dir,
                line_pairs=line_pairs, line_positions=line_positions, line_probs=None, default_prob=1,
                bus_loads=bus_loads, load_factor=1, load_increase=0,
                include_oos_trips=False,
                fault_name='fP%d_L%d_T000',
                id_step=0):
    """

    :param work_dir:
    :param line_pairs:
    :param line_positions:
    :param line_probs:
    :param default_prob:
    :param bus_loads:
    :param fault_name:
    :return:
    """

    lines = range(len(line_pairs))
    num_positions = len(line_positions)
    probs = line_probs if line_probs else [ default_prob for _ in lines ]

    event_df_list = list()
    line_cost_df_list = list()
    gen_cost_df_list = list()
    cost_dict = dict()

    for line in lines:
        # TODO - verify..!
        line_id = line + id_step
        line_fr = line_pairs[line][0]
        line_to = line_pairs[line][1]

        cost_dict[line] = list()

        for position in line_positions:
            fname = fault_name % (int(position), line_id)
            # print 'Processing %s...' % fname
            evchain = get_event_chain(work_dir, fname)
            cost_input = {'time': [], 'e_type': [], 'info': [] }
            ev_input = { 'from': [], 'to': [], 'stage': [] }

            for evnum, event in enumerate(evchain):
                event_fr = int(event.fr) if event.fr != '' else event.fr
                event_to = int(event.to) if event.to != '' else event.to
                event_type = event.type.lower()

                cost_input['time'].append(event.dt)
                cost_input['e_type'].append(event_type)

                if event_type == 'lsdt1':
                    cost_input['info'].append({
                        'bus': event_fr,
                        'stage': event.stage,
                    })
                elif event.type.startswith('oos'): # event.type == 'oosmho' or event.type == 'ooslen':
                    cost_input['info'].append({
                        'bus': event_to,
                        'gen': event_fr,
                    })
                elif event_type == 'zpott':
                    cost_input['info'].append({
                        'to': event_to,
                        'from': event_fr,
                        'stage': event.stage,
                    })

                    # TODO - check if this is necessary..!
                    # if evnum == 0:
                    #     line_fr = event_fr
                    #     line_to = event_to
                    #     line_id = filter(lambda l: line_pairs[l][0] == event_fr and line_pairs[l][1] == event_to, lines)[0] #+ 1

                else:
                    cost_input['info'].append({
                        'to': event_to,
                        'from': event_fr,
                    })

                # collect the data on events
                ev_input['from'].append(event_fr)
                ev_input['to'].append(event_to)
                ev_input['stage'].append(event.stage)

            # process the events and get the costs
            cost_df = pd.DataFrame(cost_input)
            costCalc = Cost(cost_df,
                        busBaseLoads=bus_loads,
                        fromToPairs=line_pairs,
                        loadIncrease=load_increase,
                        loadFactor=load_factor,
                        include_oos_trips=include_oos_trips)

            # take different costs separately
            cost = costCalc.totalCost
            shedCost = costCalc.shedCost
            trippedCost = costCalc.trippedCost
            overloadedCost = costCalc.overloadedCost

            cost_df['l_cost'] = cost
            cost_df['l_cost_oos'] = trippedCost
            cost_df['l_cost_lsd'] = shedCost
            cost_df['l_cost_ovl'] = overloadedCost

            cost_df['l_id'] = line
            cost_df['l_from'] = line_fr
            cost_df['l_to'] = line_to
            cost_df['l_pos'] = position
            cost_df['l_prob'] = probs[line]
            cost_df['e_from'] = ev_input['from']
            cost_df['e_to'] = ev_input['to']
            cost_df['e_stage'] = ev_input['stage']

            # collect the events
            event_df_list.append(cost_df[['l_id', 'l_from', 'l_to', 'l_pos', 'time', 'e_type', 'e_from', 'e_to', 'e_stage']])

            # continue the data frame preparation
            cost_df = cost_df[map(lambda e_type: e_type.startswith('oos'), cost_df['e_type'].values)]
            if not cost_df.empty:
                cost_df['g_oos'] = map(lambda info: int(info['gen']), cost_df['info'].values)

                cost_df = cost_df[['l_id', 'l_from', 'l_to', 'l_pos', 'g_oos', 'l_prob',
                                   'l_cost', 'l_cost_oos', 'l_cost_lsd', 'l_cost_ovl']]
                cost_dict[line].append(cost)

                # line costs
                line_cost_df = cost_df.groupby(['l_id', 'l_from', 'l_to', 'l_pos']).agg({'g_oos': lambda g: tupelize(g)})
                line_cost_df['g_trips'] = [ len(g) for g in line_cost_df.g_oos ]
                line_cost_df['l_cost'] = cost
                line_cost_df['l_cost_oos'] = trippedCost
                line_cost_df['l_cost_lsd'] = shedCost
                line_cost_df['l_cost_ovl'] = overloadedCost
                line_cost_df['l_prob'] = probs[line]

                line_cost_df = line_cost_df[line_cost_df['l_cost'] > 0]

                if not line_cost_df.empty:
                    line_cost_df_list.append(line_cost_df)

                if not cost_df.empty: # and cost > 0:
                    gen_cost_df_list.append(cost_df)

    event_df = pd.DataFrame(pd.concat(event_df_list))

    cost_dict = dict(filter(lambda (l, c): sum(c), cost_dict.items()))

    # TODO - clear the exceptions like IndexError: list index out of range..!
    len_line_df = len(line_cost_df_list)
    if len_line_df > 1:
        line_risk_df = pd.DataFrame(pd.concat(line_cost_df_list))
        line_risk_df.reset_index(inplace=True)
    elif len_line_df == 1:
        line_risk_df = pd.DataFrame(line_cost_df_list[0])
    else:
        line_risk_df = None

    len_gen_df = len(gen_cost_df_list)
    if len_gen_df > 1:
        gen_risk_df = pd.DataFrame(pd.concat(gen_cost_df_list))
        gen_risk_df.reset_index(inplace=True)
    elif len_gen_df == 1:
        gen_risk_df = pd.DataFrame(gen_cost_df_list[0])
    else:
        gen_risk_df = None

    if line_risk_df is not None and gen_risk_df is not None:
        line_risk_df['l_risk'] = line_risk_df['l_prob'] * line_risk_df['l_cost']
        gen_risk_df['l_risk'] = gen_risk_df['l_prob'] * gen_risk_df['l_cost']
        line_risk_df['l_risk_lsd'] = line_risk_df['l_prob'] * line_risk_df['l_cost_lsd']
        gen_risk_df['l_risk_lsd'] = gen_risk_df['l_prob'] * gen_risk_df['l_cost_lsd']
        line_risk_df['l_risk_oos'] = line_risk_df['l_prob'] * line_risk_df['l_cost_oos']
        gen_risk_df['l_risk_oos'] = gen_risk_df['l_prob'] * gen_risk_df['l_cost_oos']
        line_risk_df['l_risk_ovl'] = line_risk_df['l_prob'] * line_risk_df['l_cost_ovl']
        gen_risk_df['l_risk_ovl'] = gen_risk_df['l_prob'] * gen_risk_df['l_cost_ovl']

        risk_tot = sum(line_risk_df['l_risk']) / num_positions
        risk_lsd = sum(line_risk_df['l_risk_lsd']) / num_positions
        risk_oos = sum(line_risk_df['l_risk_oos']) / num_positions
        risk_ovl = sum(line_risk_df['l_risk_ovl']) / num_positions
    else:
        risk_tot = 0
        risk_lsd = 0
        risk_oos = 0
        risk_ovl = 0

    risk_results = { 'line': line_risk_df, 'gen': gen_risk_df, 'dict': cost_dict, 'event': event_df,
                     'tot': risk_tot, 'lsd': risk_lsd, 'oos': risk_oos, 'ovl': risk_ovl }

    return risk_results

    # risk_df = pd.DataFrame(pd.concat(cost_df_list))
    # risk_df.reset_index(inplace=True)

    # risk_df = risk_df.loc[map(lambda l: l in cost_dict.keys(), risk_df['l_id']), :]
    # risk_df = risk_df.groupby(['l_id', 'l_from', 'l_to']).agg({ 'g_oos': lambda g: join_gen(g) })
    # risk_df.reset_index(inplace=True)
    # risk_df['cost'] = 0
    #
    # risk_dict = dict().fromkeys(cost_dict)
    #
    # for ll, cc in cost_dict.items():
    #     line_cost = sum(cc) / num_positions
    #     line_risk = probs[ll] * line_cost
    #
    #     risk_df.loc[risk_df['l_id'] == ll, 'cost'] = line_cost
    #     cost_dict[ll] = line_cost
    #     risk_dict[ll] = line_risk
    #
    # risk_df['prob'] = map(lambda l: probs[l], risk_df['l_id'])
    # risk_df['risk'] = risk_df['prob'] * risk_df['cost']
    #
    # rist_tot = sum(risk_dict.values())
    # risk_results = { 'df': risk_df, 'dict': risk_dict, 'tot': risk_tot }