import os
import re
import pickle
import csv

import config

# precompiled regex patterns for further use
patterns = {
    'case_dir': re.compile(
        '^L(?P<lines>(\d{1,2},?)+)+_L(?P<load>\d+)$'),
    # 'case_dir': re.compile(
    #     '^L(?P<lines>(\d{1,2},?)+)+_T(?P<dt>([\d\.]-?)+)*_P(?P<pos>([\d\.]-?)+)*_L(?P<load>\d+)_C(?P<dl>-?\d+)$'),
    'data_file': re.compile('^f(?P<pos>\d{2})l(?P<line>\d{2})-(?P<load>\d{3})-(?P<dl>[-\d]\d{2}).dat$'),
    'event_file': re.compile('^f(?P<pos>\d{2})l(?P<line>\d{2})-(?P<load>\d{3})-(?P<dl>[-\d]\d{2})_event.csv$'),
    'data': re.compile(ur'((G_tot\s+(?P<gen>\d+\.\d+))|(L_tot\s+(?P<dem>\d+\.\d+)\s+(?P<load>\d+\.\d+)))'),
    # 'event_bus': re.compile(ur'^\s+(?P<bus>[\d\w]+)\s+\d*\s+\d+\.\d+\s+$'),
    'event_bus': re.compile(ur'\s+(\d+\s+(?P<bus>[\w\d-]+)\s+\d+\.\d+)|(\d+)'),
    'line_info': re.compile(
        ur'(B\s+(?P<id>\d+)\s+(?P<from>\d+)\s+(?P<to>\d+)\s+[01]\s+\d+\.\d+\s+)(?P<load>-?\d+\.\d+)'),
    'loss_data': re.compile(ur'numBusIsl:\s(?P<numBusIsl>\d+.?\d*)\n' +
                            ur'.+vecBusIsl:\s+(?P<vecBusIsl>(\s*\d+,)+\s+\d+),\s*\n' +
                            ur'.+misBusP:\s+(?P<misBusP>(\s*-?\d+\.\d+,)+\s*-?\d+\.\d+),\s*\n' +
                            ur'.+misSumP:\s+(?P<misSumP>-?\d+\.\d+)\s*\n' +
                            ur'.+genSumP:\s+(?P<genSumP>-?\d+.?\d*)\s*\n' +
                            ur'.+demSumP:\s+(?P<demSumP>-?\d+.?\d*)\s*\n' +
                            ur'.+lossSumP:\s+(?P<lossSumP>-?\d+\.\d*)\s*\n' +
                            ur'.+costSumP:\s+(?P<costSumP>-?\d+\.\d*)'),
    'lsdt_stage': re.compile(ur'stage\s+(?P<stage>\d+)\s+at'),
    'zpott_zone': re.compile(ur'Zone\s+(?P<zone>\d+)\s+'),
}

class PLSFEvent(object):
    def __init__(self, etype, fr, to, dt, stage=None):
        self.type = etype.strip()
        self.fr = fr
        self.to = to
        self.dt = float(dt)
        self.stage = stage

    def __str__(self):
        return '%s_%d_%d' % (self.type, self.fr, self.to)

    def __eq__(self, other):
        """
        Ignores dt property
        """
        if isinstance(other, PLSFEvent):
            return other.type == self.type and other.fr == self.fr and other.to == self.to
        else:
            raise TypeError


class PLSFResults(object):
    def __init__(self, num_bus_isl=None, vec_bus_isl=None, mis_bus_p=None, mis_sum_p=None,
                 gen_sum_p=None, dem_sum_p=None, loss_sum_p=None, cost_sum_p=None):
        self.num_bus_isl = num_bus_isl
        self.vec_bus_isl = vec_bus_isl
        """:type list[int]"""
        self.mis_bus_p = mis_bus_p
        """:type list[float]"""
        self.mis_sum_p = mis_sum_p
        self.gen_sum_p = gen_sum_p
        self.dem_sum_p = dem_sum_p
        self.loss_sum_p = loss_sum_p
        self.cost_sum_p = cost_sum_p


def get_loss_data(workdir, fault_file_name):
    with open(os.path.join(workdir, fault_file_name + '.log')) as result_log:
        results = re.search(patterns['loss_data'], result_log.read())
        if not results:
            print([workdir,fault_file_name])
            pass
        loss = PLSFResults(
            num_bus_isl=int(results.group('numBusIsl')),
            vec_bus_isl=[int(n) for n in results.group('vecBusIsl').split(',')],
            mis_bus_p=[float(n) for n in results.group('misBusP').split(',')],
            mis_sum_p=float(results.group('misSumP')),
            gen_sum_p=float(results.group('genSumP')),
            dem_sum_p=float(results.group('demSumP')),
            loss_sum_p=float(results.group('lossSumP')),
            cost_sum_p=float(results.group('costSumP')))
    return loss


def get_event_chain(workdir, fault_name):
    chain = []

    with open(os.path.join(workdir, fault_name + '_event.csv')) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            event_type = row['MODEL']
            if re.match(ur'^\s+$', event_type):
                continue
            from_bus = patterns['event_bus'].match(row['FROM_BUS']).group('bus')
            try:
                to_bus = patterns['event_bus'].match(row['TO_BUS']).group('bus')
            except AttributeError:
                to_bus = ''
            stage = None
            if event_type == 'lsdt1':
                stage = int(re.search(patterns['lsdt_stage'], row['EVENT DESCRIPTION']).group('stage'))
            elif event_type == 'zpott':
                re_zpott = re.search(patterns['zpott_zone'], row['EVENT DESCRIPTION'])
                stage = int(re_zpott.group('zone')) if re_zpott else 0
            chain.append(PLSFEvent(event_type, from_bus, to_bus, row['TIME'], stage=stage))
    return chain


def get_lines_load(case_dir, fault_name):
    lines = {}
    with open(os.path.join(config.WORK_DIR, case_dir, 'data', fault_name + '.dat')) as data:
        results = re.finditer(patterns['line_info'], data.read())
        for result in results:
            _id = int(result.group('id'))
            lines[_id] = abs(float(result.group('load')))
    return lines
