# import os
from py4pslf.postprocessing.reader import *
from py4pslf.costCalculator import Cost
import xlsxwriter

wdirname = 'c:\\Users\\jaworskiw\\OneDrive\\PSLFProjects\\RKtests\\zpott_oosmho\\results\\chans'
zpottimes = [210] #[150, 180, 210, 240]
lines = range(34)
positions = [ 10*p for p in range(1, 10) ]
colnames = ['zpott_time', 'line_id', 'line_from', 'line_to', 'position', 'ev_time', 'ev_from', 'ev_to', 'ev_type', 'ev_stage']
pairs = Cost.fromToPairs

wbook = xlsxwriter.Workbook('c:\\Users\\jaworskiw\\OneDrive\\PSLFProjects\\RKtests\\oosmho_zpottest.xlsx')
wsheet = wbook.add_worksheet('results')
wrow = 1

for colnum, colname in enumerate(colnames):
    wsheet.write(0, colnum, colname)

for zpottime in zpottimes:
    wdir = wdirname.replace('ZPT', '%03d' % zpottime)

    for line in lines:
        for position in positions:
            fname = 'f%02dl%02d-100-000' % (position, line)
            evchain = get_event_chain(wdir, fname)

            for event in evchain:
                wsheet.write(wrow, 0, zpottime)
                wsheet.write(wrow, 1, line + 1)
                wsheet.write(wrow, 2, pairs[line][0])
                wsheet.write(wrow, 3, pairs[line][1])
                wsheet.write(wrow, 4, position)
                wsheet.write(wrow, 5, event.dt)
                wsheet.write(wrow, 6, int(event.fr) if event.fr != '' else event.fr)
                wsheet.write(wrow, 7, int(event.to) if event.to != '' else event.to)
                wsheet.write(wrow, 8, event.type)
                wsheet.write(wrow, 9, event.stage)

                wrow += 1

wbook.close()

