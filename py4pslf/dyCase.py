import os
import shutil

import errno

from jinja2 import Template

import config
from jinja2 import Environment, FileSystemLoader


def recursive_overwrite(src, dest):
    """
    Util function for coping and overwriting files in directory tree.
     Implemented because shutil does not provide overwriting with shutil.copytree
    :param src: source path
    :param dest: destination path
    :return:
    """
    if os.path.isdir(src):
        if not os.path.isdir(dest):
            os.makedirs(dest)
        files = os.listdir(src)
        for f in files:
            recursive_overwrite(os.path.join(src, f),
                                os.path.join(dest, f))
    else:
        shutil.copyfile(src, dest)


THIS_DIR = os.path.dirname(os.path.abspath(__file__))


class dyCase(object):
    def __init__(self, caseName='bc1agc', case_dir=None, case=None, endTime=900,
                 workspace=None, done=set(), static_file='stRun.p', inrun_file='dyInRun.p',
                 prerun_file='dyPre.p', postrun_file='dyCost.p', runs_file='dyN1.runs', instant_render_file=True,
                 fault_file='dyFaultLine.p', dem_pred_file='demPred.p', dyd_file=None, timestep=0.005):
        """

        @type case: py4pslf.mcCase.MCCase
        @type workspace: String
        :param caseName:
        :param endTime:
        :param workspace:
        :param done: set of done faults name
        """
        self.caseNameBase = caseName
        self.caseName = caseName + '_' + case.load_factor_str  # case name form file with modified load
        self.endTime = endTime
        self.workspace = workspace if workspace else config.WORK_DIR

        # nl = faultedLineNum
        self.j2_env = Environment(loader=FileSystemLoader(os.path.join(THIS_DIR, '..', 'case_tmpl', 'base')),
                                  trim_blocks=True)
        self.case = case
        self.case_dir = case_dir
        self.faults = [fault for fault in case.faults if fault.data_file_name() not in done]
        self.static_file = static_file
        self.prerun_file = prerun_file
        self.postrun_file = postrun_file
        self.runs_file = runs_file
        self.inrun_file = inrun_file
        self.fault_file = fault_file
        self.dem_pred_file = dem_pred_file

        self.dyd_file = dyd_file if dyd_file else self.caseName + '.dyd'
        self.timestep = timestep

        self.buildWorkDir()
        if instant_render_file:
            self.renderCaseFiles()

    def renderCaseFiles(self):
        self.getPreFiles()
        self.getInRunFiles()
        self.getFaultFiles()
        self.getPostFiles()
        self.getRunsFile()
        self.getDemPredictionGeneratorFiles()

    def buildWorkDir(self):
        recursive_overwrite(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'case_tmpl'),
                            self.workspace)
        directories = ['pre', 'inrun', 'dyds', 'post', 'stat', 'data', 'chans', 'cases/fN1', 'dyds']
        for new_dir in directories:
            try:
                os.makedirs(os.path.join(self.workspace, new_dir))
            except OSError as exc:
                if exc.errno != errno.EEXIST:
                    raise exc
                pass
        if self.case_dir:
            shutil.copyfile(os.path.join(self.case_dir, '{0}.sav'.format(self.caseNameBase)),
                            os.path.join(self.workspace, 'cases', '{0}.sav'.format(self.caseNameBase)))
            shutil.copyfile(os.path.join(self.case_dir, '{0}.dyd'.format(self.caseNameBase)),
                            os.path.join(self.workspace, 'dyds', '{0}.dyd'.format(self.caseNameBase)))

    def getDemPredictionGeneratorFiles(self):
        r_out = open(os.path.join(self.workspace, self.dem_pred_file), 'w')
        r_out.write(self.j2_env.get_template(self.dem_pred_file).render(
            case_name=self.caseNameBase,
            case_new_name=self.caseName,
            load_factor=self.case.load_factor))
        r_out.close()

    def getStatFiles(self):
        # n_out = re.sub('.p', '_' + self.case.name + '.p', self.files['st'])
        r_out = open(os.path.join(self.workspace, self.static_file), 'w')
        r_out.write(self.j2_env.get_template(self.static_file).render(
            caseName=self.caseName,
            loadFactor=self.case.faults,
            load=self.case.name,  # TODO change naming
            brNum=self.faults[0].lines[0].str))
        r_out.close()

    def getPreFiles(self, template=None):
        """
        :param :jinja2.Template template:
        :return:
        """
        if not template:
            template = self.j2_env.get_template(self.prerun_file)
        n_out = os.path.join(self.workspace, 'pre', 'pq' + self.case.name + 'dy.p')
        p_out = open(os.path.join(self.workspace, n_out), 'w')
        p_out.write(template.render(loadFactor="1",
                                    generation=self.case.generation,
                                    pre_sav_name=self.case.pre_sav_name,
                                    outaged_line=self.case.outages[0] if len(self.case.outages) > 0 else -1))
        p_out.close()

    def getFaultFiles(self):
        # print self.fault_file
        for fault in self.faults:
            f_out = open(os.path.join(self.workspace, 'dyds', fault.data_file_name() + '.p'), 'w')
            f_out.write(self.j2_env.get_template(self.fault_file).render(lines=fault.lines))
            f_out.close()

    def getInRunFiles(self, template=None):
        """

        :param :jinja2.Template template:
        :return:
        """
        if not template:
            template = self.j2_env.get_template(self.inrun_file)
        p_out = open(os.path.join(self.workspace, 'inrun', 'inrun.p'), 'w')
        p_out.write(template.render(sFact='%.2f' % self.case.load_increase,
                                    end=str(self.endTime)))
        p_out.close()

    def getPostFiles(self, template=None):
        """

        :param :jinja2.Template template:
        :return:
        """
        if not template:
            template = self.j2_env.get_template(self.postrun_file)
        for fault in self.faults:
            f_out = open(os.path.join(self.workspace, 'post', fault.data_file_name() + '.p'), 'w')
            f_out.write(template.render(fault_name=fault.data_file_name()))
            f_out.close()

    def getRunsFile(self):
        r_out = open(os.path.join(self.workspace, self.runs_file), 'w')
        r_out.write(self.j2_env.get_template(self.runs_file).render(case_name=self.caseNameBase,
                                                                    case_new_name=self.caseName,
                                                                    inrun_file='inrun\inrun.p' if self.inrun_file else 'none',
                                                                    post_file=self.postrun_file is not None,
                                                                    pre_file='pre\pq%sdy.p' % self.case.name if self.prerun_file else 'none',
                                                                    faults=self.faults,
                                                                    end_time=self.endTime,
                                                                    dyd_file=self.dyd_file,
                                                                    timestep=self.timestep,
                                                                    id_str=self.case.name))
        r_out.close()


class UniversalDyCase(dyCase):  # WTF naming....
    def __init__(self, caseName='case', case_dir=None, case=None, endTime=1800,
                 workspace=None, inrun_file='dyInRun.p',
                 prerun_file='dyPre.p', postrun_file='dyCost.p', runs_file='dyN1.runs',
                 fault_file='dyFaultLine.p', timestep=0.005):
        dyCase.__init__(self, caseName=caseName, case_dir=case_dir, case=case, endTime=endTime,
                        workspace=workspace, inrun_file=inrun_file,
                        prerun_file=prerun_file, postrun_file=postrun_file, runs_file=runs_file,
                        fault_file=fault_file, timestep=timestep, instant_render_file=False)
        self.caseName = case.name  # hack because I'm lazy

        self.renderCaseFiles()

    def renderCaseFiles(self):
        if self.prerun_file: self.getPreFiles()
        if self.inrun_file: self.getInRunFiles()
        self.getFaultFiles()
        if self.postrun_file: self.getPostFiles()
        self.getRunsFile()

    def buildWorkDir(self):
        # recursive_overwrite(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'case_tmpl'),
        #                     self.workspace)
        directories = ['pre', 'inrun', 'dyds', 'post', 'stat', 'base',
                       'data', 'chans', 'cases', 'cases/fN1', 'dyds']
        for new_dir in directories:
            try:
                os.makedirs(os.path.join(self.workspace, new_dir))
            except OSError as exc:
                if exc.errno != errno.EEXIST:
                    raise exc
                pass
        # print self.case_dir
        for f in ['dyPlot.p', 'dyRun.p', 'dyCh2a.p']:
            shutil.copyfile(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'case_tmpl', f),
                            os.path.join(self.workspace, f))
        shutil.copyfile(os.path.join(self.case_dir, 'cases', '{0}.sav'.format(self.case.name)),
                        os.path.join(self.workspace, 'cases', '{0}.sav'.format(self.case.name)))
        shutil.copyfile(os.path.join(self.case_dir, 'dyds', '{0}.dyd'.format(self.case.name)),
                        os.path.join(self.workspace, 'dyds', '{0}.dyd'.format(self.case.name)))

    def getPreFiles(self, template=None):
        try:
            with open(os.path.join(self.case_dir, 'pre', self.prerun_file)) as temp:
                super(UniversalDyCase, self).getPreFiles(Template(temp.read()))
        except:
            super(UniversalDyCase, self).getPreFiles()

    def getPostFiles(self, template=None):
        try:
            with open(os.path.join(self.case_dir, 'post', self.postrun_file)) as temp:
                super(UniversalDyCase, self).getPostFiles(Template(temp.read()))
        except Exception as e:
            super(UniversalDyCase, self).getPostFiles()

    def getInRunFiles(self, template=None):
        try:
            with open(os.path.join(self.case_dir, 'inrun', self.inrun_file)) as temp:
                super(UniversalDyCase, self).getInRunFiles(Template(temp.read()))
        except:
            super(UniversalDyCase, self).getInRunFiles()
