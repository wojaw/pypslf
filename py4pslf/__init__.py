class Line(object):
    """
    Object that line
    """
    __slots__ = ['fr', 'to', 'str', 'num', 'length', 'zone']  # RAM saving trick

    def __init__(self, _id='', l_from=0, l_to=1, length=-1, zone=-1):
        """

        :param _id: indexing from 0 in all cases
        :param l_from: node from, indexing from 1
        :param l_to: node to, indexing from 1
        :param length:
        """
        try:
            self.num = int(_id)
            self.str = "%02d" % _id
        except TypeError:
            self.num = _id
            self.str = "%s" % str(_id)
        # self.id = self.num+1
        try:
            self.fr = int(l_from)
        except ValueError:
            self.fr = l_from
        try:
            self.to = int(l_to)
        except ValueError:
            self.to = l_to
        self.length = float(length)
        self.zone = int(zone)

    @property
    def f_t(self):
        """
        Return string containing from and to bus
        """
        return '%d-%d' % (self.fr, self.to)

    def __getstate__(self):
        return self.to_dict()

    def __setstate__(self, d):
        self.__init__(_id=d['num'],
                      l_from=d['fr'],
                      l_to=d['to'])

    def to_dict(self):
        return dict(
            num=self.num,
            str=self.str,
            fr=self.fr,
            to=self.to, )

    def __str__(self):
        return str(self.to_dict())


class PSLFExcerption(Exception):
    pass
