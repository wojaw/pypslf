import pandas as pd
# import networkx as nx

# dfLogs = pd.DataFrame({
#     'time': [0.105, 1.234, 53.8, 60.3, 121.2, 200, 250, 300],
#     'e_type': ['ooslen', 'zpott', 'lsdt', 'lsdt', 'lsdt', 'lsdt', 'lsdt', 'locti'],
#     'info': [{ 'bus': 12, 'gen': 31 }, { 'from': 16, 'to': 19 },
#              { 'bus': 3, 'stage': 1 }, { 'bus': 4, 'stage': 1 }, { 'bus': 4, 'stage': 3 },
#              { 'bus': 20, 'stage': 2 }, { 'bus': 20, 'stage': 3 }, { 'from': 14, 'to': 15 }]
# })

class Cost(object):
    maxSimTime = 3600
    simTools = ('ooslen', 'zpott', 'lsdt1', 'locti', 'oosmho')
    lsdtStage = {
        1: {'freq': 49.0, 'frac': 0.2},
        2: {'freq': 48.7, 'frac': 0.2},
        3: {'freq': 48.5, 'frac': 0.1},
    }
    busBaseLoads = [0.0, 0.0, 322.0, 500.0, 0.0, 0.0, 233.8, 522.0, 0.0, 0.0, 0.0, 7.5, 0.0, 0.0, 320.0, 329.4, 0.0,
                    158.0, 0.0, 680.0, 274.0, 0.0, 247.5, 308.6, 224.0, 139.0, 281.0, 206.0, 283.5, 0.0, 9.2, 0.0, 0.0,
                    0.0, 0.0, 0.0, 0.0, 0.0, 1104.0]
    fromToPairs = [[1, 2], [1, 39], [2, 3], [2, 25], [3, 4], [3, 18], [4, 5], [4, 14], [5, 6], [5, 8], [6, 7], [6, 11],
                   [7, 8], [8, 9], [9, 39], [10, 11], [10, 13], [13, 14], [14, 15], [15, 16], [16, 17], [16, 20], # [16, 19],
                   [16, 21], [16, 24], [17, 18], [17, 27], [21, 22], [22, 23], [23, 24], [25, 26], [26, 27], [26, 28],
                   [26, 29], [28, 29]]
    gen_trip_costs = {'cold': 120, 'warm': 90, 'hot': 60}  # [10^3 PLN]
    oos_trip_cost = gen_trip_costs['hot']
    overload_cost = 0.0

    def getCurrentBusLoad(self, busId, simTime):
        assert simTime <= self.endSimTime, 'The given time value exceeds %d s!!!' % self.endSimTime
        facSimTime = simTime / self.maxSimTime
        return self.busCaseLoads[busId] * (1 + self.loadIncrease * facSimTime)

    def __init__(self, df_logs, priceVoLL=9.98, numBusIsl=1, vecBusIsl=None, loadBusMis=None, loadIncrease=0, loadFactor=1,
                 endSimTime=1800, busBaseLoads=None, fromToPairs=None, include_oos_trips=False):
        """
        :param df_logs:
        :param priceVoLL:
        :param numBusIsl:
        :param vecBusIsl:
        :param loadBusMis:
        :param loadIncrease:
        :param loadFactor:
        :param endSimTime:
        ;param busBaseLoads:
        :param fromToPairs:
        """
        self.priceVoLL = priceVoLL  # [1000 PLN / MWh]
        self.numBusIsl = numBusIsl
        self.vecBusIsl = vecBusIsl
        self.loadBusMis = loadBusMis
        self.loadIncrease = loadIncrease
        self.loadFactor = loadFactor
        self.endSimTime = endSimTime
        self.oos_cost = self.oos_trip_cost if include_oos_trips else 0

        if fromToPairs: self.fromToPairs = fromToPairs
        if busBaseLoads: self.busBaseLoads = busBaseLoads
        self.busCaseLoads = map(lambda busLoad: busLoad * self.loadFactor, self.busBaseLoads)

        if not df_logs.empty:
            self.df = df_logs.sort_values(by='time', ascending=True)
            self.df['timeLeft'] = self.endSimTime - self.df['time']
            self.df['loadLost'] = 0
            self.df['energyLost'] = 0

            # self.gridGraph = self.getGraph()
            self.shedCost = self.getShedCost()
            self.isolatedCost = self.getIsolatedCost()
            self.trippedCost = self.getTrippedCost()
            self.overloadedCost = self.getOverloadedCost()
            self.totalCost = self.shedCost + self.isolatedCost + self.trippedCost + self.overloadedCost
        else:
            self.shedCost = self.isolatedCost = self. trippedCost = self.overloadedCost = self.totalCost = 0.0

    # def getGraph(self):
    #     G = nx.Graph()
    #     G.add_edges_from(self.fromToPairs)
    #     return G

    def getTrippedCost(self):
        oos = self.df[map(lambda e_type: e_type.startswith('oos'), self.df.e_type.values)]
        oos_count = len(oos)
        return float(oos_count * self.oos_cost)

    def getOverloadedCost(self):
        ol = self.df[self.df.e_type == 'locti']
        ol_count = len(ol)
        return float(ol_count * self.overload_cost)

    def getShedCost(self):
        # print self.df.e_type
        lsdt = self.df[self.df.e_type == 'lsdt1']
        energyShed = 0
        busesShed = map(lambda bb: { 'stage': 0, 'energy': 0, 'load': 0 }, self.busBaseLoads)

        for (id, row) in list(lsdt.iterrows()):
            # print('# ', id, row['time'])
            t = row['time']
            busId = row['info']['bus']
            busNum = int(busId) - 1
            # isl = self.vecBusIsl[bus]
            stage = row['info']['stage']

            if stage - busesShed[busNum]['stage'] == 1:
                frac = self.lsdtStage[stage]['frac']
            else: # the stage does not increase by 1
                # cumulative fraction, e.g. starting with stage=2 results in the loss of 0.2+0.2=0.4 of the load
                # frac = sum([self.lsdtStage[stg]['frac'] for stg in range(busesShed[busNum] + 1, stage + 1)])
                # assumption that the total load is lost
                frac = 1.0

            busesShed[busNum]['stage'] = stage

            load = self.getCurrentBusLoad(busNum, t)
            loadLost = load * frac
            busesShed[busNum]['load'] += loadLost
            energyLost = loadLost * row['timeLeft'] / self.maxSimTime
            busesShed[busNum]['energy'] += energyLost

            self.df.loc[id, 'loadLost'] = loadLost
            self.df.loc[id, 'energyLost'] = energyLost

            energyShed += energyLost

        self.busStages = [ (b+1, s) for (b, s) in enumerate(busesShed) if s['stage'] > 0]

        return energyShed * self.priceVoLL

    def getIsolatedCost(self):
        # lines = self.df[self.df.type in ['zpott', 'locti']]
        energyIsolated = 0

        # TODO
        # for (id, row) in list(lines.iterrows()):


        return energyIsolated * self.priceVoLL
