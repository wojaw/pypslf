import os
import shutil
import subprocess
from datetime import datetime

import re

import config


class pslfRunner(object):
    names = ['pslf', 'plot']

    paths = {
        'py': 'py4pslf',
        'dir': "%PSLFDIR%",
        'cp': "%PSLFPATH%",
        'java': "%PSLFJAVA%",
        'jar': dict(),
        'cmd': dict(),
    }

    pFiles = {
        'static': 'stRun',
        'dytools': 'dyRun',
        'dyplot': 'dyCh2a',
        'demPred': 'demPred'
    }

    def __init__(self, strId='', workspace=None, silent=False):
        # self.caseName = caseName
        self.dt = ''
        self.workspace = workspace if workspace else config.WORK_DIR
        self.silent = silent
        self.output = {
            'dat': list(),
            'log': list(),
        }

        self.results = dict()

        for name in self.names:
            jar = os.path.join("%PSLFPATH%", name + '.jar')
            self.paths['jar'][name] = jar
            self.paths['cmd'][name] = ' '.join([self.paths['java'], '-Xmx512m', '-jar', jar])

        self.strId = str(strId)
        self.getDatetime()
        self.name = self.dt + '_' + self.strId

    def runStatic(self):
        self.runScript(self.pFiles['static'])

    def runDytools(self, ch2a=False):
        self.runScript(self.pFiles['dytools'])
        if ch2a:
            self.runScript(self.pFiles['dyplot'], jar='plot')
        self.backupAndClean()

    def createLoadVariants(self):
        self.runScript(self.pFiles['demPred'])

    def getDatetime(self):
        dt = datetime.now().__format__("%m%d%H%M%S")
        self.dt = str(dt)
        return dt

    def runCommands(self, cmdList):
        p = self.dt + '.p'
        epcl = open(p, 'w')
        [epcl.write(cmd + '\n') for cmd in cmdList]
        epcl.close()

        self.runScript(p)
        os.remove(p)

    def runScript(self, pFile='test', jar='pslf'):
        if str(pFile).endswith('.p'):
            pRun = pFile
        else:
            pRun = pFile + '.p'
        bat = os.path.join(self.workspace, self.dt + '.bat')
        batch = open(bat, 'w')
        batch.write('epcl("' + pRun + '")\nstop')
        batch.close()

        self.runBatch(bat, jar)
        os.remove(bat)

    def runBatch(self, batFile, jar='pslf'):
        if jar in self.names:
            # os.chdir(self.workspace)
            env = os.environ
            env['TMP'] = self.workspace
            env['TEMP'] = self.workspace
            batch = ('cd %s & ' % self.workspace) + ' '.join([self.paths['cmd'][jar], batFile])
            # print batch
            if self.silent:
                with open(os.devnull, 'w') as devnull:
                    subprocess.call(batch, shell=True, stdout=devnull, stderr=devnull, env=env)
            else:
                subprocess.call(batch, shell=True, env=env)
                # os.system(batch)
        else:
            print('JAR NameError: ' + jar + '\n')

    def backupAndClean(self):
        # pass
        for case_dir in ['stat', 'dyds', 'cases', 'post', 'pre', 'inrun', 'base']:
            shutil.rmtree(os.path.join(self.workspace, case_dir))
        for chans in os.listdir(os.path.join(self.workspace, 'chans')):
            if re.match('^.+\.(log|csv)$', chans):
                continue
            try:
                os.remove(os.path.join(self.workspace, 'chans', chans))
            except:
                pass
        for case_file in os.listdir(self.workspace):
            if os.path.isdir(os.path.join(self.workspace, case_file)):
                continue
            if re.match('^(case|term|dyN1).*', case_file):
                continue
            try:
                os.remove(os.path.join(self.workspace, case_file))
            except:
                pass
