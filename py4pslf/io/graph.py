from igraph import Graph


class GraphModel(object):
    """
    Class that igraph.Graph object with some usefull methods specific to our usage
    """
    def __init__(self, reader):
        """
        Creates igraph object from system defined in config file
        :type reader: py4pslf.io.ModelReader
        """
        g = Graph()
        g.add_vertices(39)
        g.add_edges([(line.fr - 1, line.to - 1) for line in reader.lines])
        for line in reader.lines:
            g.es[line.num]['length'] = line.length
            g.es[line.num]['zone'] = line.zone
        self._graph = g

    @property
    def graph(self):
        return self._graph

    def get_lines_from_nodes(self, nodes):
        """
        Returns ids of line connected to a given list of nodes
        :type nodes: list[int]
        :return:
        """
        res = set()
        for line in self._graph.es.select(_source_in=nodes):
            res.add(line.index)
        for line in self._graph.es.select(_target_in=nodes):
            res.add(line.index)

        return list(res)
