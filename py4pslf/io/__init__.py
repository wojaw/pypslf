class ModelReader(object):
    def __init__(self):

        self._lines = []
        """:type list[py4pslf.Line]"""

        self._line_fault_probabilities = {}
        self._zone_membership = {}
        self._position_values = {}
        self._load_values = {}
        self._load_probabilities = {}

    @property
    def lines(self):
        return self._lines

    @property
    def line_length(self):
        return {line.num: line.length for line in self._lines}

    @property
    def line_fault_probabilities(self):
        return self._line_fault_probabilities

    @property
    def zone_membership(self):
        return self._zone_membership

    @property
    def load_values(self):
        return self._load_values

    @property
    def position_values(self):
        return self._position_values

    @property
    def load_probabilities(self):
        return self._load_probabilities

    def get_random_lc_value(self):
        raise "Implement IT!"

    def get_random_position_value(self):
        raise "Implement IT!"