import pandas as pd
from numpy.random import rand
from py4pslf.mcCase import Line
from py4pslf.io import ModelReader


def _get_random_val_from_prob_dens(data, prob_key='probability', val_key='value'):
    data_arr = zip(data[prob_key], data[val_key])
    lc = rand()
    tmp = 0
    for i in data_arr:
        tmp += i[0]
        if lc <= tmp:
            return i[1]


class FromXLSX(ModelReader):
    def __init__(self, x_file, line_sheet='', position_sheet='',
                 load_sheet='', zone_sheet=''):
        ModelReader.__init__(self)
        self._line_data = pd.read_excel(x_file, line_sheet, index_col=None)
        # self._position_data = pd.read_excel(x_file, position_sheet, index_col=None)
        #  self._load_data = pd.read_excel(x_file, load_sheet, index_col=None)
        # self._zone_data = pd.read_excel(x_file, zone_sheet, index_col=None).set_index('id').T.to_dict()

        self._lines = []
        """:type list[py4pslf.Line]"""
        zone_membership = {}
        # line_fault_probabilities = {}
        line_length = {}
        for _id, l_from, l_to, zone, line_len in zip(self._line_data['id'],
                                                     self._line_data['from'],
                                                     self._line_data['to'],
                                                     self._line_data['zone'],
                                                     self._line_data['len_km']):
            self._lines.append(Line(_id=_id, l_from=l_from, l_to=l_to, length=line_len))
            # line_fault_probabilities[_id] = self._zone_data[zone]['fault_factor'] * line_len / 100
            line_length[_id] = line_len
            if zone not in zone_membership:
                zone_membership[zone] = []
            zone_membership[zone].append(_id)
        # self._line_length = line_length
        # self._zone_membership = zone_membership
        # self._line_fault_probabilities = line_fault_probabilities
        # self._load_probabilities = {'%.2f' % load: prob for load, prob in
        #                             zip(self._load_data['value'], self._load_data['probability'])}

    @property
    def lines(self):
        return self._lines

    # @property
    # def load_values(self):
    #     return self._load_data['value']
    #
    # @property
    # def position_values(self):
    #     return self._position_data['position']
    #
    # def get_random_lc_value(self):
    #     return _get_random_val_from_prob_dens(self._load_data,
    #                                           prob_key='probability',
    #                                           val_key='value')
    #
    # def get_random_position_value(self):
    #     return _get_random_val_from_prob_dens(self._position_data,
    #                                           prob_key='probability',
    #                                           val_key='position')
