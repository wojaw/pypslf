import math

from py4pslf import Line
from datetime import datetime


class FaultedLine(Line):
    """
    Object that represents single line failure with its position and timestamp
    """

    # default clearing time
    t_clear = 5000  # to avoid automatic pauses during the simulation...

    # RAM saving trick
    __slots__ = ['dt', 'fr', 'pos', 'to', 'str', 'num',
                 'length', 'zone', 'ck', 'sec', 'R', 'X',
                 't_from_clear', 't_to_clear', 'pause']

    def __init__(self, _id='', l_from=0, l_to=1, pos=0, dt=0, ck=1, sec=1, R=0, X=0, t_from_clear=None, t_to_clear=None,
                 pause=None):
        Line.__init__(self, _id, l_from, l_to)

        self.pos = float(pos)
        self.dt = float(dt)
        self.ck = int(ck)
        self.sec = int(sec)
        self.R = float(R)
        self.X = float(X)
        self.t_from_clear = t_from_clear if t_from_clear else self.t_clear
        self.t_to_clear = t_to_clear if t_to_clear else self.t_clear
        self.pause = pause if pause else self.dt # WTF !!! - PSLF magic (c8

    def __getstate__(self):
        return self.to_dict()

    def __setstate__(self, d):
        self.__init__(_id=d['num'],
                      l_from=d['fr'],
                      l_to=d['to'],
                      pos=d['pos'],
                      dt=d['dt'],
                      ck=d['ck'],
                      sec=d['sec'],
                      R=d['R'],
                      X=d['X'],
                      t_from_clear=d['t_from_clear'],
                      t_to_clear=d['t_from_clear'],
                      pause=d['t_from_clear'])  # not the best idea but I'm lazy

    def to_dict(self):
        return dict(
            num=self.num,
            str=self.str,
            fr=self.fr,
            to=self.to,
            pos=self.pos,
            dt=self.dt,
            ck=self.ck,
            sec=self.sec,
            R=self.R,
            X=self.X,
            t_from_clear=self.t_from_clear,
            t_to_clear=self.t_to_clear,
            pause=self.pause)

    def __str__(self):
        return str(self.to_dict())


class Fault(object):
    def __init__(self, fault_data):
        """
        :param dict fault_data:
        """
        self._lines = self._parse_line_data(fault_data)
        """:type list[FaultedLine]"""

        # the next few should help to manage the results
        self.events = []
        """:type list[p4pslf.postprocessing.reader.PLSFEvent]"""
        self.probability = 0
        self.loss = 0
        self.result = None
        """:type list[p4pslf.postprocessing.reader.PLSFResults]"""

    @staticmethod
    def _parse_line_data(fault_data):
        print fault_data
        lines = [FaultedLine(_id=fault_data['num'],
                             l_from=fault_data['from'],
                             l_to=fault_data['to'],
                             pos=fault_data['pos'],
                             dt=fault_data['t'])]
        root = fault_data
        try:
            while root['next']:
                line = root['next']
                lines.append(FaultedLine(_id=line['num'],
                                         l_from=line['from'],
                                         l_to=line['to'],
                                         pos=line['pos'],
                                         dt=line['t']))
                root = line
        except KeyError as e:
            pass
        return lines

    @property
    def risk(self):
        return self.probability * math.floor(self.loss)

    @property
    def lines(self):
        return self._lines

    @lines.setter
    def lines(self, lines):
        self._lines = lines

    def data_file_name(self):
        return 'fP%s_L%s_T%s' % ('p'.join(['%d' % (line.pos * 100) for line in self._lines]),
                                 'l'.join(['%s' % str(line.num) for line in self._lines]),
                                 't'.join(['%03d' % line.dt for line in self._lines]))

    def __str__(self):
        return str(self.__dict__)


def get_number_with_format_class(number_type):
    if number_type != int and number_type != float:
        raise TypeError('Type should be int or float')

    class NumberWithFormatting(number_type):
        def __new__(cls, val, static_format):
            if type(val) != int and type(val) != float:
                raise TypeError('Type should be int or float')
            num = number_type.__new__(cls, val)
            num.format = static_format
            return num

        def __str__(self):
            return self.format.format(self)

    return NumberWithFormatting


class UnitGeneration:
    def __init__(self, gen):
        assert gen.has_key('id') and gen.has_key('pgen')

        test = gen.has_key('bus') and gen.has_key('vsched')
        test = test and gen['bus'] != -1 and gen['vsched'] != -1

        self._id = gen['id']
        self._bus = gen['bus'] if test else -1
        self._pgen = gen['pgen']
        self._vsched = gen['vsched'] if test else -1

    @property
    def id(self):
        return self._id

    @property
    def bus(self):
        return self._bus

    @property
    def pgen(self):
        return self._pgen

    @property
    def vsched(self):
        return self._vsched


class MCCase(object):
    FloatWithFormatting = get_number_with_format_class(float)
    IntWithFormatting = get_number_with_format_class(int)

    def __init__(self, faults_list=[], load_factor=None, load_increase=1, cid=None, pre_sav_name=None,
                 generation=[], base_datetime=None, sim_datetime=None, simulation_id=None,
                 sim_period=900, outages=[], name=None):
        """
        Object that represents portion of data that will be simulated within MC run
        :param list[dict] faults_list: list of faulted lines in order of fault occurrence
        :param float load_factor: dictionary containing node names and their load at simulation time
        :param dict generation: dictionary containing node names and their generation at simulation time
        :param str sim_datetime:
        :param int sim_time: time of a simulation in seconds
        """
        if generation is None:
            generation = {}
        self._load_factor = load_factor
        self._load_increase = load_increase
        self._id = cid
        self._name = name
        self._simulation_id = simulation_id
        """:type str"""
        self.pre_sav_name = pre_sav_name
        self.generation = [ UnitGeneration(gen) for gen in generation ]
        self._sim_period = sim_period
        self._outages = outages
        self._sim_datetime = datetime.strptime(sim_datetime, '%Y%m%d-%H%M') if sim_datetime else ''
        self._base_datetime = datetime.strptime(base_datetime, '%Y%m%d-%H%M') if base_datetime else ''
        """:type datetime.datetime"""
        self._faults = self._generate_faults(faults_list)
        """:type list[Fault]"""
        self._dict = {
            "gen": generation,
            "load": load_factor,
            "load_increase": load_increase,
            "id": cid,
            "faults": faults_list,
            "case": base_datetime,
            "horizon": sim_datetime,
            "time": sim_period,
            "outages": [],
            "sim_id": simulation_id,
            "name": name,
        }

    @staticmethod
    def _generate_faults(faults_list):
        """
        Creates list of `Fault` object from provided dictionary
        :param list[dict] faults_list:
        :return list[Fault]:
        """
        faults = []

        for fault in faults_list:
            faults.append(Fault(fault))

        return faults

    @property
    def load_factor(self):
        return self._load_factor

    @property
    def simulation_id(self):
        return self._simulation_id

    @property
    def load_factor_str(self):
        return '{:.5f}'.format(self._load_factor).replace('.', 'x')

    @property
    def load_increase(self):
        return self._load_increase

    @property
    def id(self):
        return self._id

    @property
    def sim_datetime(self):
        return self._sim_datetime

    @property
    def base_datetime(self):
        return self._base_datetime

    @property
    def sim_period(self):
        return self._sim_period

    @property
    def faults(self):
        return self._faults

    @property
    def outages(self):
        return self._outages

    @faults.setter
    def faults(self, new_faults):
        for f in new_faults:
            assert type(f) == Fault
        self._faults = new_faults

    @property
    def name(self):
        if self._name:
            return self._name
        return self._sim_datetime.strftime("%Y%m%d-%H%M") + '_' + self.load_factor_str

    def to_dict(self):
        return self._dict

    @staticmethod
    def from_dict(input_dict):
        return MCCase(faults_list=input_dict['faults'],
                      cid=input_dict['id'],
                      sim_period=input_dict['time'],
                      sim_datetime= input_dict['horizon'] if 'horizon' in input_dict else None,
                      base_datetime=input_dict['case'] if 'case' in input_dict else None,
                      load_factor=input_dict['load'],
                      load_increase=input_dict['load_increase'],
                      generation=input_dict['gen'],
                      # pre_sav_name=input_dict['pre_sav_name'],
                      outages=input_dict['outages'],
                      name=input_dict['name'] if 'name' in input_dict else None,
                      simulation_id=input_dict['sim_id'])
