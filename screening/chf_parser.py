import pandas as pd
import re
import math
import cmath


def get_my_sign(x):
    return x and (1, -1)[x < 0]


def get_swing_ids(test):
    t0 = test[0]
    tt = [ 1 for _ in test ]
    tt0 = tt[0]
    for i, t in enumerate(test[1:]):
        if t != t0:
            tt0 += 1
        tt[i+1] = tt0
    return tt


def get_delta_angle(vt, it, phi, xq, deg=False):
    vt_c = cmath.rect(vt, 0.0)
    ix_c = cmath.rect(it * xq, math.pi/2 - phi)
    EQ_c = vt_c + ix_c
    return cmath.phase(EQ_c) * (180/math.pi if deg else 1)


def get_step_deltas(df, col, loc='bef'):
    if col in df.columns:
        col_lo = df[col].values[:-1]
        col_hi = df[col].values[1:]
    else:
        col_lo = df.index.values[:-1]
        col_hi = df.index.values[1:]
    if loc=='bef':
        col_delta = [0.0] + list(col_hi - col_lo)
    else:
        col_delta = list(col_hi - col_lo) + [0.0]
    return pd.Series(col_delta)
    

def change_chf_col_name(col, col_reps={ 'pg': 'pelec', 'ph': 'pmech', 'abus': 'vbus', 'qg': 'qelec', 'sigx': 'oostr' }):
    col_new = re.sub(r'\s+', '_', re.split('-', col)[0])
    for name, rep in col_reps.iteritems():
        col_new = col_new.replace(name, rep)
    return col_new.lower()
    

def read_chf_csv(chf_file, # Xq_dict,
                 fault_time=0.0,
                 zpott_time=0.2,
                 relative_angles=False,
                 ref_gen_num=30,
                 gen_nums=range(30, 40),
                 gen_model='genrou',
                 gen_oosmho=True
                ):
    clearing_time = fault_time + zpott_time

    chf_df = pd.read_csv(chf_file, quotechar=r'"')
    chf_cols = chf_df.columns

    var_names = [ 'Time', 'ang', 'spd', 'pg', 'qg', 'efd', 'vt', 'it', 'abus' ]
    var_names = var_names if gen_model=='gencls' else var_names + [ 'pl', 'ph' ]
    var_names = var_names + ['sigx'] if gen_oosmho else var_names
    var_regexp = r'^' + r'|'.join(var_names)
    col_names = filter(lambda col: re.match(var_regexp, col), chf_cols)
    chf_df = chf_df[col_names]
    # column names replacement
    col_names = map(change_chf_col_name, col_names)
    chf_df.columns = col_names

    chf_df['delta_time'] = get_step_deltas(chf_df, 'time')
    
    time_init = chf_df.time[chf_df.time < 1/50.0].min()
    chf_df['time_period'] = [ 'pre' if t < fault_time else ('on' if t < clearing_time else 'post') for t in chf_df.time ]

    for gen_num in gen_nums:
        gen_str, ref_gen_str = str(gen_num), str(ref_gen_num)
        ang_str, ref_ang_str = 'ang_' + gen_str, 'ang_' + ref_gen_str
        rel_ang_str, phi_ang_str = 'rel_ang_' + gen_str, 'phi_ang_' + gen_str
        del_ang_str, rel_del_ang_str = 'del_ang_' + gen_str, 'rel_del_ang_' + gen_str
        pelec_str, qelec_str = 'pelec_' + gen_str, 'qelec_' + gen_str
        pmech_str, pacc_str = 'pmech_' + gen_str, 'pacc_' + gen_str
        psign_str, pswing_str = 'sign_pacc_' + gen_str, 'swing_pacc_' + gen_str
        spd_str, ssign_str, sswing_str = 'spd_' + gen_str, 'sign_spd_' + gen_str, 'swing_swing_' + gen_str
        vt_str, it_str, vbus_str = 'vt_' + gen_str, 'it_' + gen_str, 'vbus_' + gen_str

        # relative rotor angle
        if relative_angles:
            chf_df[rel_ang_str] = chf_df[ang_str]
        else:
            chf_df[rel_ang_str] = chf_df[ang_str] - chf_df[ref_ang_str]
            
        # mechanical power for gencls == no ieeeg1 == no ph channel
        if 'ph' not in var_names:
            gen_init = round(float(chf_df[pelec_str][chf_df.time == time_init]))
            chf_df[pmech_str] = gen_init
            
        # accelarating power
        chf_df[pacc_str] = chf_df[pmech_str] - chf_df[pelec_str]
        chf_df['delta_' + pacc_str] = get_step_deltas(chf_df, pacc_str)
        chf_df[psign_str] = map(get_my_sign, chf_df[pacc_str])
        chf_df.loc[chf_df.time_period=='pre', pswing_str] = -1
        chf_df.loc[chf_df.time_period=='on', pswing_str] = 0
        chf_df.loc[chf_df.time_period=='post', pswing_str] = get_swing_ids(chf_df.loc[chf_df.time_period=='post', psign_str].values)
        
        # speed-based acceleration...
        spd_delta = get_step_deltas(chf_df, spd_str)
        chf_df['delta_spd_'+gen_str] = spd_delta
        chf_df[ssign_str] = map(get_my_sign, spd_delta)
        # chf_df[ssign_str] = map(get_my_sign, chf_df[spd_str] - 1.0)
        chf_df.loc[chf_df.time_period=='pre', sswing_str] = -1
        chf_df.loc[chf_df.time_period=='on', sswing_str] = 0
        chf_df.loc[chf_df.time_period=='post', sswing_str] = get_swing_ids(chf_df.loc[chf_df.time_period=='post', ssign_str].values)
        
        # power angle
        chf_df[phi_ang_str] = map(math.atan, chf_df[pelec_str] / chf_df[qelec_str])
        
        # P-delta angle - with abus
        del_ang_ref = chf_df[vbus_str][chf_df.time == time_init].values[0]
        chf_df[del_ang_str] = chf_df[ang_str] - del_ang_ref
        # chf_df.loc[chf_df.time_period=='pre', del_ang_str] = chf_df[ang_str] - chf_df.loc[chf_df.time_period=='pre', vbus_str]
        # chf_df.loc[chf_df.time_period=='on', del_ang_str] = chf_df[ang_str] - chf_df.loc[chf_df.time_period=='on', vbus_str].mean()
        # chf_df.loc[chf_df.time_period=='post', del_ang_str] = chf_df[ang_str] - chf_df.loc[chf_df.time_period=='post', vbus_str] #.mean()
        # chf_df[del_ang_str] = chf_df[rel_ang_str] - chf_df['vbus_'+gen_str]
        # # chf_df[rel_del_ang_str] = chf_df[rel_ang_str] - chf_df['vbus_'+gen_str]
        # chf_df[rel_del_ang_str] = chf_df[del_ang_str] - chf_df['del_ang_'+ref_gen_str]
        
        # P-delta angle - with vectors
        # Xq = Xq_dict[gen_num]
        # chf_df[del_ang_str] = map(lambda (vt, it, phi): get_delta_angle(vt, it, phi, Xq, deg=True),
        #                               chf_df[[vt_str, it_str, phi_ang_str]].values)

    return chf_df[sorted(chf_df.columns)] #chf_df.reindex_axis(sorted(chf_df.columns), axis=1)


def get_gen_chf_df(gen_num, chf_df):
    gen_str = '_' + str(gen_num)
    gen_cols = filter(lambda col: col.endswith(gen_str), chf_df.columns)
    gen_df = chf_df[gen_cols]
    # gen_df.index = chf_df.time
    gen_df.loc[:, 'time'] = chf_df.time
    gen_df.columns = map(lambda col: col.replace(gen_str, ''), gen_df.columns)
    gen_df.loc[:, 'gen_id'] = gen_num #[ gen_num for _ in range(len(gen_df)) ]
    return gen_df