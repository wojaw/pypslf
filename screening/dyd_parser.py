import pandas as pd
import re
import os
from math import pi

THIS_DIR = os.path.dirname(os.path.abspath(__file__))
PROJ_DIR = os.path.abspath(os.path.join(THIS_DIR, '..'))
TMPL_DIR = os.path.join(PROJ_DIR, 'case_tmpl', 'base')
TMPL_DYD = os.path.join(TMPL_DIR, 'new39_template.dyd')

freqHz_steady = 50
omega_steady = 2 * pi * freqHz_steady
# baseMVA = 100


def get_dyd_lines(dyd_path):
    with open(dyd_path, 'r') as dyd_file:
        for dyd_line in dyd_file:
            yield dyd_line
        dyd_file.close()


def parse_dyd_line(dyd_line, dyd_model):
    if dyd_line.startswith(dyd_model):
        dyd_row = dyd_line.split()[1:]
        dyd_re = [ re.search('(\d+.?\d*)', dr) for dr in dyd_row ]
        dyd_list = [ float(dr.group(0)) for dr in dyd_re if dr ]
        return dyd_list
    else:
        return list()


def read_genrou(dyd_path):
    # col_names = 'n name kv id rl mva tpdo tppdo tpqo tppqo h d ld lq lpd lpq lppd ll s1 s12 ra rcomp xcomp accel'.split()
    col_names = 'n name kv id rl mva tpdo tppdo tpqo tppqo h d x_d x_q xx_d xx_q xxx_d ll s1 s12 ra rcomp xcomp accel'.split()
    dyd_iter = get_dyd_lines(dyd_path)
    dyd_rows = list()
    for dyd_line in dyd_iter:
        dyd_list = parse_dyd_line(dyd_line, dyd_model='genrou')
        if dyd_list:
            dyd_rows.append(dyd_list)

    dyd_frame = pd.DataFrame(dyd_rows, columns=col_names).set_index('n')
    dyd_frame.index = map(int, dyd_frame.index)
    dyd_frame['m'] = 2 * dyd_frame['h'] * dyd_frame['mva'] / omega_steady
    return dyd_frame


def read_gensal(dyd_path):
    # col_names = 'n name kv id rl mva tpdo tppdo tppqo h d ld lq lpd lppd ll s1 s12 ra rcomp xcomp'.split()
    col_names = 'n name kv id rl mva tpdo tppdo tppqo h d x_d x_q xx_d xxx_d ll s1 s12 ra rcomp xcomp'.split()
    dyd_iter = get_dyd_lines(dyd_path)
    dyd_rows = list()
    for dyd_line in dyd_iter:
        dyd_list = parse_dyd_line(dyd_line, dyd_model='gensal')
        if dyd_list:
            dyd_rows.append(dyd_list)

    dyd_frame = pd.DataFrame(dyd_rows, columns=col_names).set_index('n')
    dyd_frame.index = map(int, dyd_frame.index)
    dyd_frame['m'] = 2 * dyd_frame['h'] * dyd_frame['mva'] / omega_steady
    return dyd_frame


def read_gencls(dyd_path):
    # col_names = 'n name kv id rl mva h d ra lppd rcomp xcomp t0 file tf0 tf1'.split()[:-4]
    col_names = 'n name kv id rl mva h d ra xxx_d rcomp xcomp t0 file tf0 tf1'.split()[:-4]
    dyd_iter = get_dyd_lines(dyd_path)
    dyd_rows = list()
    for dyd_line in dyd_iter:
        dyd_list = parse_dyd_line(dyd_line, dyd_model='gencls')
        if dyd_list:
            dyd_rows.append(dyd_list)

    dyd_frame = pd.DataFrame(dyd_rows, columns=col_names).set_index('n')
    dyd_frame.index = map(int, dyd_frame.index)
    dyd_frame['m'] = 2 * dyd_frame['h'] * dyd_frame['mva'] / omega_steady
    return dyd_frame


def read_gen_df(dyd_path=TMPL_DYD, gen_model='genrou'):
    if gen_model=='genrou':
        return read_genrou(dyd_path)
    elif gen_model=='gencls':
        return read_gencls(dyd_path)
    elif gen_model=='gensal':
        return read_gensal(dyd_path)
    else:
        print 'BAD MODEL NAME!'
        return None

def concat_gen_dfs(gen_models, ref_id=0, dyd_path=TMPL_DYD):
    gen_dfs = [ read_gen_df(gen_model=gen_model, dyd_path=dyd_path) for gen_model in gen_models ]
    ref_df = gen_dfs[ref_id]
    col_names = ref_df.columns
    for gen_df in gen_dfs[1:]:
        for col in gen_df.columns:
            if col not in col_names:
                gen_df[col] = None

    concat_df = pd.concat(gen_dfs)[col_names]
    return concat_df


def get_xq_dict(gen_df, gen_model):
    return dict(gen_df.x_q.iteritems()) if gen_model=='genrou' else dict(gen_df.xxx_d.iteritems())

    
def read_zpott(dyd_path):
    col_names = 'nf namef kvf id nt namet kvt ck sec rl notrip nfar tcb shape1 ang1 rf1 rr1 wt1 wr1 t1 shape2f ang2f rf2f rr2f wt2f wr2f t2f shape2t ang2t rf2t rr2t wt2t wr2t t2t'.split()
    dyd_iter = get_dyd_lines(dyd_path)
    dyd_rows = list()
    for dyd_line in dyd_iter:
        dyd_list = parse_dyd_line(dyd_line, dyd_model='zpott')
        if dyd_list:
            dyd_rows.append(dyd_list)

    dyd_frame = pd.DataFrame(dyd_rows, columns=col_names).set_index(['nf', 'nt'])
    dyd_frame['tcl'] = dyd_frame['tcb'] + dyd_frame['t1'] # ???
    return dyd_frame


