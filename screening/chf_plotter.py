# from matplotlib import colors
gen_pmax_dict = dict(zip(range(30,40), (1100, 646, 725, 917, 508, 800, 816, 564, 865, 1040)))


def get_var_df(time_range, chf_df, var_name, var_ref=0.0, gen_name='', drop_times=None, drop_delta=0.005):
    time_lo, time_hi = time_range
    var_cols = filter(lambda col: col.startswith(var_name + '_' + gen_name), chf_df.columns)
    var_df = chf_df[var_cols] - var_ref
    var_df.columns = map(lambda col: col.split('_')[-1], var_cols)
    var_df.index = chf_df.time
    var_df = var_df.loc[time_lo:time_hi]

    if drop_times:
        for t in drop_times:
            var_df.drop(var_df[abs(var_df.index - t) < drop_delta].index, inplace=True)
    return var_df


def plot_gen_var_df(plt, time_range, chf_df, var_name, var_ref=0.0, figsize=(16,9), fontsize=12, plot=True, show=True, save=False):
    gen_colormap = plt.cm.get_cmap('Paired')
    plt.rcParams.update({'font.size': fontsize, 'font.family': 'sans-serif'})
    fig, ax = plt.subplots(figsize=figsize)
    var_df = get_var_df(time_range, chf_df, var_name, var_ref)
    if plot:
        var_df.plot(linewidth=3, colormap=gen_colormap, ax=ax, legend=False)
        ax.legend(loc='lower right')
        ax.set_ylabel(var_name + ' - %.1f' % var_ref)
        if show:
            plt.show()
        if save:
            plt.savefig(var_name + '_df_plot.png')
    return var_df


def plot_gen_var_dfs(plt, time_range, chf_df, var_names, figsize=(16,6), fontsize=12, plot=True, show=True, save=False, ylims=None):
    gen_colormap = plt.cm.get_cmap('Paired')
    plt.rcParams.update({'font.size': fontsize, 'font.family': 'sans-serif'})
    var_num = len(var_names)
    fig, ax = plt.subplots(var_num, sharex=True, figsize=(figsize[0], var_num * figsize[1]))
    time_lo, time_hi = time_range
    var_dfs_dict = dict()

    for var_id, var_name in enumerate(var_names):
        var_df = get_var_df(time_range, chf_df, var_name)
        var_dfs_dict[var_name] = var_df
        if plot:
            var_df.plot(linewidth=3, colormap=gen_colormap, ax=ax[var_id], legend=False)
            ax[var_id].legend(loc='lower right')
            ax[var_id].set_ylabel(var_name)
            if ylims:
                ax[var_id].set_ylim(ylims)
            
    if plot:
        if save:
            plt.savefig('_'.join(var_names) + '_dfs_plot.pdf', pad_inches=0)
        if show:
            plt.show()

    return var_dfs_dict


def get_gen_P_delta(time_range, chf_df, gen_num, ang_name='del_ang', drop_times=None, drop_delta=0.005):
    gen_str = str(gen_num)
    var_names = (ang_name, 'sign_pacc', 'pelec', 'pmech', 'oostr')
    var_dfs = dict((var_name, get_var_df(time_range, chf_df, var_name, gen_name=gen_str, drop_times=drop_times, drop_delta=drop_delta)) for var_name in var_names)
    return var_dfs
    

def plot_gen_P_delta(plt, time_range, chf_df,
                     gen_nums=range(30,40),
                     ang_name='del_ang',
                     gen_pmax_dict=gen_pmax_dict,
                     gen_step=5, fontsize=12,
                     drop_times=None, drop_delta=0.005,
                     show=True, save=False):
    num_gen = len(gen_nums)
    plt.rcParams.update({'font.size': fontsize, 'font.family': 'sans-serif'})
    fig, ax = plt.subplots(num_gen/2, 2, figsize=(16,20))

    for ng, gen_num in enumerate(gen_nums):
        row_id, col_id = divmod(ng, 2)
        gen_vars = get_gen_P_delta(time_range, chf_df, gen_num, ang_name=ang_name, drop_times=drop_times, drop_delta=drop_delta)

        gen_psign = gen_vars['sign_pacc']
        gen_oostr = gen_vars['oostr']
        gen_color = [ 'red' if o>=2 else 'blue' if s>=0 else 'purple' for (s, o) in zip(gen_psign.values, gen_oostr.values) ]
        gen_ang = gen_vars[ang_name]
        gen_pelec = gen_vars['pelec']
        gen_pmech = gen_vars['pmech']
        gen_pelec_max = gen_pelec.copy()
        # gen_pelec_max[str(gen_num)] = gen_pmax_dict[gen_num]

        ax[row_id, col_id].set_title('Gen. ' + str(gen_num) + ' - Pmax = %d' % gen_pmax_dict[gen_num] + ' MW')
        if row_id==num_gen/2-1:
            ax[row_id, col_id].set_xlabel('Angle [deg]')
        if col_id==0:
            ax[row_id, col_id].set_ylabel('Power [MW]')
        ax[row_id, col_id].plot(gen_ang, gen_pmech, 'g-', linewidth=1)
        # ax[row_id, col_id].plot(gen_ang.iloc[::gen_step], gen_pmech.iloc[::gen_step], 'ro', markersize=1)
        ax[row_id, col_id].plot(gen_ang.iloc[0], gen_pmech.iloc[0], 'go', markersize=10)
        ax[row_id, col_id].plot(gen_ang, gen_pelec, '-', color='black', linewidth=0.25)
        ax[row_id, col_id].scatter(gen_ang[::gen_step], gen_pelec[::gen_step], color=gen_color[::gen_step])
        # ax[row_id, col_id].plot(gen_ang, gen_pelec_max, '-', color='purple', linewidth=0.5)
    
    if save:
        plt.savefig('P-delta_plot.pdf', papertype='a4', pad_inches=0)
    if show:
        plt.show()
