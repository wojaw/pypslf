def plot_omib_vars(plt, tsa, gen_step=1, fontsize=12, show=True, save=False, figsize=(16,10)):
    plt.rcParams.update({'font.size': fontsize, 'font.family': 'sans-serif'})
    fig, ax = plt.subplots(2, 2, figsize=figsize)
    tsa_df = tsa['df']
    t_stable = tsa['stable']
    t_instable = tsa['instable']

    # Speed
    ax[0,0].set_title('OMIB speed')
    ax[0,0].set_ylabel('ref. spd [p.u.]')
    ax[0,0].axhline(0, color='black', linestyle='--', linewidth=1.5)
    ax[0,0].plot(tsa_df.index, tsa_df.spd, linewidth=1, color='black')
    ax[0,0].scatter(tsa_df.index[::gen_step], tsa_df.spd[::gen_step], linewidth=2,
                color=['green' if pacc<0 else 'blue' for pacc in tsa_df.pacc.values[::gen_step]])

    # Acc. power
    ax[1,0].set_title('OMIB acc. power')
    ax[1,0].set_xlabel('time [s]')
    ax[1,0].set_ylabel('power [MW]')
    ax[1,0].axhline(0, color='black', linestyle='--', linewidth=1.5)
    ax[1,0].plot(tsa_df.index, tsa_df.pacc, linewidth=1, color='black')
    ax[1,0].scatter(tsa_df.index[::gen_step], tsa_df.pacc[::gen_step], linewidth=2,
                    color=['red' if pacc>0 else 'blue' for pacc in tsa_df.delta_pacc.values[::gen_step]])

    # Angle
    ax[0,1].set_title('OMIB angle')
    ax[0,1].set_ylabel('angle [deg]')
    ax[0,1].plot(tsa_df.index, tsa_df.ang, linewidth=2, color='blue')

    # Power: elec & mech
    ax[1,1].set_title('OMIB powers')
    ax[1,1].set_xlabel('time [s]')
    ax[1,1].set_ylabel('power [MW]')
    ax[1,1].plot(tsa_df.index, tsa_df.pelec, linewidth=2, color='blue')
    ax[1,1].plot(tsa_df.index, tsa_df.pmech, linewidth=2, color='purple')

    # vertical lines
    for r in range(2):
        for c in range(2):
            ax[r, c].axvline(t_stable, color='g', linewidth=2)
            ax[r, c].axvline(t_instable, color='r', linewidth=2)

    if save:
        plt.savefig('OMIB_plots.pdf', pad_inches=0)
    if show:
        plt.show()


def plot_omib_P_delta(plt, tsa, gen_step=1, fontsize=12, show=True, save=False, figsize=(16,9), markersize=50, max_time=None):
    plt.rcParams.update({'font.size': fontsize, 'font.family': 'sans-serif'})
    tsa_df = tsa['df']
    max_time = max_time if max_time else tsa_df.index.max()
    tsa_df = tsa_df[:max_time]

    plot_time = tsa['time'] if tsa.has_key('time') else max_time
    gen_color = ['blue' if (p=='on') else 'purple' if t <= plot_time else 'red' for (t, p) in zip(tsa_df.index.values, tsa_df.time_period.values)]

    gen_ang = tsa_df['ang']
    gen_pelec = tsa_df['pelec']
    gen_pmech = tsa_df['pmech']

    fig, ax = plt.subplots(figsize=figsize)
    ax.set_title('OMIB')  # - Pmax = %d' % get_omib_avg_out(pd.Series(gen_pmax_dict), mach_dict, iner_dict) + ' MW')
    ax.set_xlabel('angle [deg]')
    ax.set_ylabel('power [MW]')
    ax.axvline(gen_ang.iloc[0], color='black', linestyle='--', linewidth=1.5)
    ax.plot(gen_ang, gen_pmech, color='black', linewidth=0.5, linestyle='--')
    ax.scatter(gen_ang[::gen_step], gen_pmech[::gen_step], color=gen_color[::gen_step] if gen_color else 'blue', marker='s', s=markersize/2.0)
    # ax.plot(gen_ang.iloc[0], gen_pmech.iloc[0], color='black', markersize=15)
    ax.plot(gen_ang, gen_pelec, '-', color='black', linewidth=0.5)
    ax.scatter(gen_ang[::gen_step], gen_pelec[::gen_step], color=gen_color[::gen_step] if gen_color else 'blue', s=markersize)

    # area information
    acc_df = tsa['acc_df']
    dec_df = tsa['dec_df']
    ax.text(acc_df.ang.mean(), (acc_df.pelec + acc_df.pmech).mean()/2, 'acc. area = %.2f' % tsa['acc_area']) #, color='blue')
    ax.text(dec_df.ang.mean(), (dec_df.pelec + dec_df.pmech).mean()/2, 'dec. area = %.2f' % tsa['dec_area']) #, color='purple')

    if save:
        plt.savefig('OMIB_P-delta.pdf', pad_inches=0)
    if show:
        plt.show()