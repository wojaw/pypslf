import os
from dyd_parser import read_gen_df, concat_gen_dfs
from chf_parser import *
import omib_maker
reload(omib_maker)
from omib_maker import *

user_dir = os.getenv('USERPROFILE')
proj_dir = os.path.join(user_dir, 'Desktop', 'AXA', 'PSCC18_paper')


def get_sime_tsa(time_range, fault_times, chf_df, gen_df, gen_oosmho=True, drop_delta=0.005):
    # get data frames needed for omib creation
    omib_dfs = get_omib_dfs(time_range, chf_df, gen_oosmho=gen_oosmho)

    # get the 1st run of tsa - with CMs & NMs identified every time step
    omib_0 = get_omib_vars_df(omib_dfs, gen_dict=None, gen_df=gen_df, chf_df=chf_df,
                              drop_times=fault_times, drop_delta=drop_delta)
    tsa_0 = get_omib_tsa(omib_0, fault_times)

    # get the 2nd run of tsa - with CMs & NMs taken from the 1st tsa
    omib_1 = get_omib_vars_df(omib_dfs, gen_dict=tsa_0['gen_dict'], gen_df=gen_df,
                              drop_times=fault_times, drop_delta=drop_delta)
    tsa_1 = get_omib_tsa(omib_1, fault_times=fault_times)

    return tsa_1


def run_sime_tsa(fault_line, fault_pos,
                 case_name, test_name,
                 proj_dir=proj_dir, res_dir='results',
                 gen_model='genrou', gen_oosmho=True, #gen_trip=True,
                 fault_time=0.0, max_time=5.0, zpott_time=0.2, drop_delta=0.005,
                 fault_name='fP%d_L%d_T000.csv'):
    fault_times = (fault_time, fault_time+zpott_time)
    time_range = (fault_time, max_time)
    chf_name = fault_name % (fault_pos, fault_line)

    case_dir = os.path.join(proj_dir, res_dir, case_name + '_' + test_name)
    chf_dir = os.path.join(case_dir, 'results')

    # TODO
    # xl_file = os.path.join(case_dir, case_name + '_faults.xlsx')
    chf_file = os.path.join(chf_dir, chf_name)
    gen_model_chf = gen_model[0] if type(gen_model)==list else gen_model
    chf_df = read_chf_csv(chf_file, gen_model=gen_model_chf, gen_oosmho=gen_oosmho, fault_time=fault_time)
    gen_df = concat_gen_dfs(gen_model) if type(gen_model)==list else read_gen_df(gen_model=gen_model)
    tsa = get_sime_tsa(time_range, fault_times, chf_df, gen_df, gen_oosmho=gen_oosmho, drop_delta=drop_delta)
    return tsa