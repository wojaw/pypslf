import pandas as pd

from chf_plotter import get_var_df
from chf_parser import get_step_deltas, get_my_sign


def get_omib_dfs(time_range, chf_df, ang_name='del_ang', gen_oosmho=False, gen_nums=range(31, 40)):
    gen_names = map(str, gen_nums)
    var_names = [ang_name, 'spd', 'pelec', 'pmech', 'pacc', 'sign_spd', 'sign_pacc']
    var_names = (var_names + ['oostr']) if gen_oosmho else var_names
    # var_dfs = { var_name + '_' + str(gen_num): get_var_df(time_range, chf_df, var_name, gen_name=str(gen_num))
    #            for var_name in var_names for gen_num in gen_nums }
    # var_dfs = { var_name.split('_')[-1]: get_var_df(time_range, chf_df, var_name)[gen_names] for var_name in var_names }
    var_dfs = dict()
    for var_name in var_names:
        # new_var_name = var_name.split('_')
        new_var_name = 'ang' if var_name.endswith('ang') else var_name
        var_df = get_var_df(time_range, chf_df, var_name)[gen_names] - int(var_name=='spd')
        var_df.columns = map(int, var_df.columns)
        var_dfs[new_var_name] = var_df
    return var_dfs


def identify_machines(ang_df_row, gen_df):
    # CMs & NMs identification
    ang_row = ang_df_row.sort_values()
    ang_rows = (ang_row[:-1].as_matrix(), ang_row[1:].as_matrix())
    ang_diffs = map(abs, ang_rows[0] - ang_rows[1])
    ang_seps = range(1, len(ang_row))
    ang_diff, ang_sep = max(zip(ang_diffs, ang_seps))
    mach_dict = dict()
    mach_dict['NM'], mach_dict['CM'] = [ sorted(map(int, sep.index)) for sep in (ang_row.iloc[:ang_sep], ang_row.iloc[ang_sep:]) ]

    # inertia constants calculation
    gen_nums = ang_row.index #map(int, ang_row.index)
    iner_dict = dict(gen_df.loc[gen_nums, 'm'])
    for M in ['CM', 'NM']:
        iner_list = [ iner_dict[g] for g in mach_dict[M] ]
        iner_tot = sum(iner_list)
        iner_dict[M] = iner_tot
        # iner_dict[M+'_tot'] = iner_tot
    iner_dict['OMIB'] = (iner_dict['CM'] * iner_dict['NM']) / (iner_dict['CM'] + iner_dict['NM'])

    return { 'mach': mach_dict, 'iner': pd.Series(iner_dict) }


def get_omib_avg_in(var_row, mach_dict, iner_row):
    CMs, NMs = mach_dict['CM'], mach_dict['NM']
    M_CM, M_NM = iner_row['CM'], iner_row['NM']
    var_CM = sum(var_row.loc[CMs] * iner_row.loc[CMs]) / M_CM
    var_NM = sum(var_row.loc[NMs] * iner_row.loc[NMs]) / M_NM
    return var_CM - var_NM


def get_omib_avg_out(var_row, mach_dict, iner_row):
    CMs, NMs = mach_dict['CM'], mach_dict['NM']
    M_CM, M_NM, M_OMIB = iner_row['CM'], iner_row['NM'], iner_row['OMIB']
    var_CM = sum(var_row.loc[CMs]) / M_CM
    var_NM = sum(var_row.loc[NMs]) / M_NM
    return (var_CM - var_NM) * M_OMIB


def get_omib_vars_row(var_rows, gen_dict=None, omib_names=('ang', 'spd', 'pelec', 'pmech', 'pacc')):
    ang_row, spd_row, pelec_row, pmech_row, pacc_row = [ var_rows[nam] for nam in omib_names ]
    mach_dict, iner_row = gen_dict['mach'], gen_dict['iner']
    omib_dict = dict()
    omib_dict['ang'] = get_omib_avg_in(ang_row, mach_dict, iner_row)
    omib_dict['spd'] = get_omib_avg_in(spd_row, mach_dict, iner_row)
    omib_dict['pelec'] = get_omib_avg_out(pelec_row, mach_dict, iner_row)
    omib_dict['pmech'] = get_omib_avg_out(pmech_row, mach_dict, iner_row)
    omib_dict['pacc'] = get_omib_avg_out(pacc_row, mach_dict, iner_row) # omib_dict['pmech'] -  omib_dict['pelec']
    omib_dict['sign_pacc'] = get_my_sign(omib_dict['pacc'])
    omib_dict['sign_spd'] = get_my_sign(omib_dict['spd'])
    if var_rows.has_key('oostr'):
        omib_dict['oostr'] = max(var_rows['oostr'])
    return omib_dict


def get_omib_vars_df(var_dfs, gen_dict=None, gen_df=None, chf_df=None, omib_names=('ang', 'spd', 'pelec', 'pmech', 'pacc'),
                     drop_times=None, drop_delta=0.005):
                     # spd_stable_test=0.0005, pacc_stab_test=25):
    ang_df = var_dfs['ang']
    col_names = var_dfs.keys() #list(omib_names) + ['pacc', 'psign', 'oostr']
    omib_df = pd.DataFrame(columns=col_names)
    gen_ident = gen_dict is not None
    omib_gens = dict()

    for id_row, ang_row in ang_df.iterrows():
        var_rows = {var: var_dfs[var].loc[id_row] for var in col_names}
        gen_dict = gen_dict if gen_ident else identify_machines(ang_row, gen_df)
        omib_dict = get_omib_vars_row(var_rows, gen_dict, omib_names)
        omib_df.loc[id_row] = pd.Series(omib_dict)
        omib_gens[id_row] = gen_dict
        for M in ['CM', 'NM']:
            omib_df.loc[id_row, M] = '_'.join(map(str, gen_dict['mach'][M]))

    for var in ('spd', 'pacc'):
        omib_df['delta_'+var] = get_step_deltas(omib_df, var).values

    if drop_times:
        for t in drop_times:
            omib_df.drop(omib_df[abs(omib_df.index - t) < drop_delta].index, inplace=True)

    if chf_df is not None:
        omib_df = omib_df.merge(chf_df[['time', 'time_period']].set_index(['time']), how='left', left_index=True, right_index=True)

    # omib_df.sort_index(inplace=True)
    return { 'df': omib_df, 'gens': omib_gens }


def get_omib_tsa(omib, fault_times):
    omib_df = omib['df']
    tsa_df = omib_df[fault_times[0]:].copy(deep=True)
    tsa_df['time_period'] = [ 'on' if t < fault_times[1] else 'post' for t in tsa_df.index ]
    for var in ('spd', 'pacc'):
        tsa_df['switch_'+var] = list(tsa_df['sign_'+var].values[1:] * tsa_df['sign_'+var].values[:-1]) + [0.0]

    # pacc sign switches - possible instable points
    instable_times = tsa_df.index[(tsa_df.time_period == 'post') & ((tsa_df.switch_pacc <= 0) & (tsa_df.delta_pacc > 0))]
    instable_suspect = instable_times.min()
    # spd sign switches - possible stable points
    stable_times = tsa_df.index[(tsa_df.time_period == 'post') & ((tsa_df.switch_spd <= 0) & (tsa_df.pacc < 0))]
    stable_times = sorted(filter(lambda t: t < instable_suspect, stable_times))
    stable_suspect = stable_times[0] if stable_times else None

    # oosmho benchmark
    oostr_suspect = tsa_df[tsa_df.oostr >= 1].index.min() if 'oostr' in tsa_df.columns else None
    oostr_suspect = oostr_suspect if oostr_suspect > 0.0 else None

    # test
    # print stable_suspect, instable_suspect, oostr_suspect
    # tsa_limit = oostr_suspect if oostr_suspect else instable_suspect
    tsa_limit = max(oostr_suspect, instable_suspect, fault_times[1])
    tsa_df = tsa_df[:tsa_limit]

    # resulting dict
    tsa = dict()
    tsa['df'] = tsa_df
    tsa['instable'] = instable_suspect
    tsa['stable'] = stable_suspect
    tsa['result'] = 'stable' if stable_suspect and stable_suspect < instable_suspect else 'instable'
    tsa['time'] = stable_suspect if stable_suspect and stable_suspect < instable_suspect else instable_suspect
    tsa['oostr'] = oostr_suspect
    tsa['gen_dict'] = omib['gens'][tsa['time']]
    tsa['acc_df'] = tsa_df[tsa_df.time_period == 'on']
    tsa['acc_test'] = all(tsa['acc_df'].sign_pacc >= 0)
    area = get_tsa_area(tsa['acc_df'])
    tsa['acc_sign'] = area > 0
    tsa['acc_area'] = area
    for M in ('CM', 'NM'):
        tsa[M] = tsa_df.loc[tsa['time'], M] #.split('_')

    tsa['dec_df'] = tsa_df[:tsa['time']].loc[tsa_df.time_period == 'post']
    area = get_tsa_area(tsa['dec_df'])
    tsa['dec_area'] = -area

    # (in)stability margin
    if tsa['result'] == 'instable':
        # tsa['dec_df'] = tsa_df.loc[tsa_df.time_period == 'post']
        # area = get_tsa_area(tsa['dec_df'])
        # tsa['dec_area'] = -area
        tsa['margin'] = tsa['acc_area'] - tsa['dec_area']
        tsa['deviation'] = 0
    else:
        # tsa['dec_df'] = tsa_df[:tsa['time']].loc[tsa_df.time_period == 'post']
        # area = get_tsa_area(tsa['dec_df'])
        # tsa['dec_area'] = -area
        tsa['margin'] = 0
        tsa['deviation'] = tsa['acc_area'] - tsa['dec_area']
    tsa['dec_sign'] = area < 0
    tsa['dec_test'] = all(tsa['dec_df'].sign_pacc <= 0)

    tsa['test_area'] = tsa['acc_sign'] and tsa['dec_sign'] and tsa['acc_test'] #and tsa['dec_test']
    return tsa


def get_tsa_area(tsa_df, cols={'x':'ang','y':'pacc'}, fac=3.1415/180, drop_index=True):
    col_x, col_y = cols['x'], cols['y']
    area_df = tsa_df[[col_x, col_y]].reset_index(drop=drop_index)
    area_hi = area_df[1:].reset_index(drop=True)
    area_lo = area_df[:-1].reset_index(drop=True)
    area_df = area_hi.merge(area_lo, left_index=True, right_index=True, suffixes=('_hi', '_lo'))
    area_df['area'] = 0.5 * (area_df[col_y+'_hi'] + area_df[col_y+'_lo']) * (area_df[col_x+'_hi'] - area_df[col_x+'_lo'])
    area = area_df.area.sum()
    return area * fac