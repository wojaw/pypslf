
lines = [[1,2],[1,39],[2,3],[2,25],[3,4],[3,18],[4,5],[4,14],[5,6],[5,8],[6,7],[6,11],[7,8],[8,9],[9,39],
         [10,11],[10,13],[13,14],[14,15],[15,16],[16,17],[16,19],[16,21],[16,24],[17,18],
         [17,27],[21,22],[22,23],[23,24],[25,26],[26,27],[26,28],[26,29],[28,29]]


def get_fault(line,position,time=0):
    """
    Returns proper dictionary describing single fault
    """
    return {'num': line,
            'from': lines[int(line)-1][0],
            'to': lines[int(line)-1][1],
            'pos': position,
             't': time}

# line_positions  = {
#     '2':[1,99],
#     '3':[1,25,50,75,99],
#     '1':[1,25,50,75,99],
#     '5':[1,25,50,75,99],
#     '8':[1,25,50,75,99],
#     '15':[1,25,50,75,99],
#     '6':[1,25,50,75,99]
# }
# One could also do that:
line_positions = { str(i):[1,25,50,75,99] for i in range (1,25)}

faults = []

# Example on how to generate faults array
for line,positions in line_positions.items():
    faults += map(lambda p:get_fault(line,p,0), positions)


'''
Dictionary that is and input for whose multicore computational stuff.
'''
INPUT = {
    'fault_per_core': 20,  # how many faults should be computed by on single core, 15 is default
    'files': {
        'pre': 'dyPre.p',
        'post': 'dyCost.p',
        'inrun': 'dyInRun.p',
    },
    'cases':{  #Keys are case file names i.e.: case.sav -> 'case' is dictionary key
        'bw_h01':{
            'id': 'TakiTamTest', # if not None that results will be in directory of this name, if None the ditectory will be named after case file name
            'time': '1800', # simulation time, 1800 is default
            'faults': faults, # array with all faults
            'load' : 1, # change overal base load by x , 1 is default so no change
            'load_increase': .2, # increase load to 120% during cource of simulation, default is 1 so no change
            'gen': [
                { 'id': 0, 'bus': 39, 'pgen': 1000, 'vsched': 1.045 },
                # -1 or missing value for 'bus' or 'vsched' WILL NOT affect the bus' voltage
                { 'id': 1, 'bus': 31, 'pgen': 533.57, 'vsched': -1 },
                { 'id': 2, 'pgen': 611.0, 'vsched': 1.02 },
            ],
            # 'gen': { # generation change key:value -> gen_name:new_generation
            #     '30': 235.0,
            #     '31': 533.57,
            #     '32': 611.0,
            #     '33': 594.0,
            #     '34': 477.0,
            #     '35': 611.0,
            #     '36': 526.0,
            #     '37': 507.0,
            #     '38': 780.0},
            'outages': [],
        },
    }
}
