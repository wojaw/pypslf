lines = [ [1, 2], [1, 30], [2, 3], [18, 25], [2, 25], [3, 4], [3, 18], [4, 5], [4, 14], [5, 6], [5, 8], [6, 7], [6, 11], [7, 8],
        [8, 9], [9, 30], [10, 11], [10, 13], [13, 14], [14, 15], [15, 16], [16, 17], [16, 20], [13, 20], [16, 21], [16, 24],
        [17, 18], [23, 20], [17, 27], [21, 22], [22, 23], [23, 24], [4, 30], [1, 3], [24, 28], [24, 29], [25, 26], [26, 27],
        [26, 28], [15, 20], [26, 29], [6, 14], [16, 28], [28, 29] ]
# lines = [ [10, 13] ]

all_positions = [0.01, 0.25, 0.50, 0.75, 0.99]
# all_positions = [ 0.25 ]

###

line_positions = { str(line_id): all_positions for line_id in range(len(lines)) }

# Example on how to generate faults array
def get_fault(line, position, time=0):
    """
    Returns proper dictionary describing single fault
    """
    return {'num': line,
            'from': lines[int(line)][0],
            'to': lines[int(line)][1],
            'pos': position,
             't': time}

faults = list()
for line, positions in line_positions.items():
    faults += map(lambda pos: get_fault(line, pos, 0), positions)


'''
Dictionary that is an input for whose multicore computational stuff.
'''
INPUT = {
    'fault_per_core': 22,  # how many faults should be computed by on single core, 15 is default
    'files': {
        'pre': 'dyPre.p',
        'post': 'dyCost.p',
        # 'inrun': 'none',
    },
    'cases': {
        'new39opf110dem': {
            'id': 'new39opf',
            'time': 1800,
            'faults': faults,
            'load' : 1,
            'load_increase': 0,
            'gen': list(),
            'outages': list(),
        }
    }
}
